//
//  AddMeterViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface AddMeterViewController : UIViewController<NSFetchedResultsControllerDelegate,CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    AppDelegate *appDelegate;
    NSEntityDescription *entityDateReading;
    NSManagedObjectContext *contextDateReading;
    NSFetchRequest *requestDateReading;
    NSSortDescriptor *sortDescriptorDateReading;
    NSArray *sortDescriptorsDateReading;
    NSManagedObject *matchesDateReading;
    NSArray *arrAllObjDateReading;

}
@property (strong, nonatomic) IBOutlet UITextField *txt_SerialNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_MeterNickName;

@property (strong, nonatomic) IBOutlet UITableView *tblViewHistory;
- (IBAction)action_Submit:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *ThankYouView;
- (IBAction)action_DoneThankYou:(id)sender;
- (IBAction)action_Back:(id)sender;
- (IBAction)action_Help:(id)sender;
- (IBAction)hideKeyboard:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UILabel *lbl_MeterReading;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PermitNumber;
@property (strong, nonatomic) IBOutlet UILabel *lbl_WellPermitNumber;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ScrollHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_Submit_Top;
@property (strong, nonatomic) IBOutlet UILabel *lblDoneMsg;
- (IBAction)action_AddMeter:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAddReading;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_TblView_H;
@property (strong, nonatomic) IBOutlet UILabel *lbl_HeaderMain;
@property (strong, nonatomic) NSDictionary  *dictDataManageMeter;
- (IBAction)action_SerialnoHelp:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerDateReading;
@property (strong, nonatomic) IBOutlet UILabel *lblIsMeterAlreadyRegisteredTitle;
@property (strong, nonatomic) IBOutlet UIButton *btn_YesMeterRegistered;
@property (strong, nonatomic) IBOutlet UIButton *Btn_NoMeterRegistered;
@property (strong, nonatomic) IBOutlet UILabel *lbl_YesMeterRegistered;
@property (strong, nonatomic) IBOutlet UILabel *lbl_NoMeterRegistered;
- (IBAction)action_YesMeterRegistered:(id)sender;
- (IBAction)action_NoMeterRegistered:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_POUNo_TopLeading;
@property (weak, nonatomic) IBOutlet UIButton *buttonHelp;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightForViewAddByAdmin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topForViewAddByAdmin;








@end
