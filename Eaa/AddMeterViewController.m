//
//  AddMeterViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "AddMeterViewController.h"
#import "HistoryTableViewCell.h"
#import "DashBoardViewController.h"
#import "ReportMeterReadingViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "ManageMeterViewController.h"
#import "TechDashBoardViewController.h"
#import "HelpViewGlobalViewController.h"
#import "HelpViewController.h"
#import "RegisterANewMeterViewController.h"
#import <UserNotifications/UserNotifications.h>

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AddMeterViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    Global *global;
    NSArray *arrOfHistory;
    BOOL yesFromManageMeter;
    NSString *latitude,*longtitude,*strInitialMeterReading,*strPermitNumber,*strWellPermitNumber;
    BOOL isEdited, isYesMeterRegistered;
}

@end

@implementation AddMeterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isEdited=NO;
    isYesMeterRegistered=YES;
    [_btn_YesMeterRegistered setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_Btn_NoMeterRegistered setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

    self.txt_SerialNo.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
   // self.txt_WellPermitNo.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
   // self.txtPermitNo.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];

//    _scrollView.frame=CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, 15000);
//    [_scrollView setContentSize:CGSizeMake( _scrollView.frame.size.width,15000)];
    
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

    _const_ScrollHeight.constant=500;
    _const_TblView_H.constant=500;

    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL isFromManageMeter=[defs boolForKey:@"FromManageMeter"];

    if (!isFromManageMeter)
    {
        yesFromManageMeter=NO;
        _lbl_HeaderMain.text=@"Add a Meter";
        [defs setBool:NO forKey:@"FromManageMeter"];
        [defs synchronize];
//        _const_Submit_Top.constant=-250;
//        [_lbl_MeterReading setHidden:YES];
//        [_lbl_PermitNumber setHidden:YES];
//        [_txt_MeterReading setHidden:YES];
//        [_txtPermitNo setHidden:YES];
//        [_lbl_WellPermitNumber setHidden:YES];
//        [_txt_WellPermitNo setHidden:YES];
//        [_tblViewHistory setHidden:YES];
       [_btnAddReading setHidden:YES];
        _heightForViewAddByAdmin.constant = 0.0;
    }
    else
    {
        _heightForViewAddByAdmin.constant = 150.0;
        _topForViewAddByAdmin.constant = 15.0;
        
        _buttonHelp.hidden = YES;
//        [_btn_YesMeterRegistered setHidden:YES];
//        [_Btn_NoMeterRegistered setHidden:YES];
//        [_lblIsMeterAlreadyRegisteredTitle setHidden:YES];
//        [_lbl_NoMeterRegistered setHidden:YES];
//        [_lbl_YesMeterRegistered setHidden:YES];
//        _const_POUNo_TopLeading.constant=20;

        yesFromManageMeter=YES;
        _lbl_HeaderMain.text=@"Edit Meter";
        _txt_SerialNo.text=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterNumber"]];
        _txt_MeterNickName.text=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterName"]];
      
        
//        _txt_MeterReading.text=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"InitialMeterReading"]];
//        _txtPermitNo.text=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"PermitNumber"]];
//        _txt_WellPermitNo.text=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"WellPermitNumber"]];
        
              strInitialMeterReading = [NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"InitialMeterReading"]];
               strPermitNumber =[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"PermitNumber"]];
        
                strWellPermitNumber = [NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"WellPermitNumber"]];
        
        
        
        if ([_txt_SerialNo.text isEqualToString:@"(null)"]) {
            _txt_SerialNo.text=@"";
        }
        if ([_txt_MeterNickName.text isEqualToString:@"(null)"]) {
            _txt_MeterNickName.text=@"";
        }
        _lbl_PermitNumber.text=[NSString stringWithFormat:@"EAA POU Number - NA"];
        _lbl_WellPermitNumber.text=[NSString stringWithFormat:@"EAA Well Number - NA"];
        _lbl_MeterReading.text= [NSString stringWithFormat:@"Start Of Year Meter Reading - NA"];
     
        
        if (![strPermitNumber isEqualToString:@"(null)"] && ![strPermitNumber isEqualToString:@""]) {
             _lbl_PermitNumber.text=[NSString stringWithFormat:@"EAA POU Number - %@",strPermitNumber];
        }
        if (![strWellPermitNumber isEqualToString:@"(null)"] && ![strWellPermitNumber isEqualToString:@""]) {
            _lbl_WellPermitNumber.text=[NSString stringWithFormat:@"EAA Well Number - %@",strWellPermitNumber];
        }
        if (![strInitialMeterReading isEqualToString:@"(null)"] && ![strInitialMeterReading isEqualToString:@""]) {
            _lbl_MeterReading.text= [NSString stringWithFormat:@"Start Of Year Meter Reading - %@",strInitialMeterReading];
        }
       
       
        
        _lblDoneMsg.text=@"Meter Updated successfully";
        
        arrOfHistory=[_dictDataManageMeter valueForKey:@"MeterReadings"];
        if ([arrOfHistory isKindOfClass:[NSArray class]])
        {
            _const_ScrollHeight.constant=736+250*arrOfHistory.count;
            _const_TblView_H.constant=100*arrOfHistory.count;

            [_tblViewHistory reloadData];
            
        }
        else
        {
            
        }
        NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
        NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
        
        if ([strUserType isEqualToString:@"Customer"]) {

        if (_txt_SerialNo.text.length>0) {
            [_txt_SerialNo setEnabled:NO];
        }
//        if (_txt_MeterReading.text.length>0) {
//            [_txt_MeterReading setEnabled:NO];
//        }
//        if (_txtPermitNo.text.length>0) {
//            [_txtPermitNo setEnabled:NO];
//        }
//        if (_txt_WellPermitNo.text.length>0) {
//            [_txt_WellPermitNo setEnabled:NO];
//        }
            
        }
    }

    _tblViewHistory.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    self.tblViewHistory.rowHeight = UITableViewAutomaticDimension;
    self.tblViewHistory.estimatedRowHeight = 150.0;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//============================================================================
#pragma mark- Button Action METHODS
//============================================================================

- (IBAction)action_Submit:(id)sender {
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorInternetMsg;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
    }
    else
    {
        
        if (isYesMeterRegistered)
        {
            
            NSString *errorMessage=[self validationCheck];
            if (errorMessage) {
                
                NSString *strTitle = Alert;
                [global AlertMethod:strTitle :errorMessage];
                [DejalBezelActivityView removeView];
                
            } else {
                
                [self submitMeter];

            }
            
        } else {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            RegisterANewMeterViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RegisterANewMeterViewController"];
            objByProductVC.strMeterSerialNo=_txt_SerialNo.text;
            objByProductVC.strMeterNickName=_txt_MeterNickName.text;
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }

    }
    
}
- (IBAction)action_DoneThankYou:(id)sender {
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {

//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                             bundle: nil];
//    DashBoardViewController
//    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
//    [self.navigationController pushViewController:objByProductVC animated:NO];
//
//
        
    
        
        
        //Redirect On Add meter reading screen

        if (yesFromManageMeter) // From Edit Controller
        {
            BOOL isAlreadySent=[self fetchFromCoreDataDateReadingToCheck:[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterId"]]];
            
            NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
            NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
            
            if ([strUserType isEqualToString:@"Customer"]) {
                
            }else{
                
                isAlreadySent=NO;
                
            }
            
            isAlreadySent=NO;
            
            if (isAlreadySent) {
                
                NSString *strTitle = @"Alert";
                NSString *strMsg = @"Reading can be submitted only once in 24 hrs.";
                [global AlertMethod:strTitle :strMsg];
            }else{
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                ReportMeterReadingViewController
                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
                
                NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterNumber"]];
                NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterName"]];
                
                NSString *strMeterNamenNo;
                
                if (strMeterNumber.length==0) {
                    
                    if (strMeterNamee.length==0) {
                        
                        strMeterNamenNo=@"";
                        
                    } else {
                        
                        strMeterNamenNo=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterName"]];
                        
                    }
                    
                }else{
                    
                    
                    if (strMeterNamee.length==0) {
                        
                        strMeterNamenNo=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterNumber"]];
                        
                    } else {
                        
                        strMeterNamenNo=[NSString stringWithFormat:@"%@-%@",[_dictDataManageMeter valueForKey:@"MeterName"],[_dictDataManageMeter valueForKey:@"MeterNumber"]];
                        
                    }
                    
                }
                objByProductVC.strMeterNickNamenSerialNoDefault=strMeterNamenNo;
                objByProductVC.strMeteridDefault=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterId"]];
                objByProductVC.dictDataManageMeterList=_dictDataManageMeter;
                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
        }else{    // From
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            ReportMeterReadingViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
            objByProductVC.strComeFrom = @"AddMeter_New";
            objByProductVC.strMeterNickNamenSerialNoDefault= [NSString stringWithFormat:@"%@-%@",_txt_MeterNickName.text,_txt_SerialNo.text];

            [self.navigationController pushViewController:objByProductVC animated:NO];
            
            
            
        }
        
        
       
        
        
        
        
        
        
        
        
        
        
    }else{
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        TechDashBoardViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TechDashBoardViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
}

- (IBAction)action_Back:(id)sender {
    
    if (yesFromManageMeter) {
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k=0; k<arrstack.count; k++) {
            if ([[arrstack objectAtIndex:k] isKindOfClass:[ManageMeterViewController class]]) {
                index=k;
                // break;
            }
        }
        ManageMeterViewController *myController = (ManageMeterViewController *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];

//        for (UIViewController *controller in self.navigationController.viewControllers)
//        {
//            if ([controller isKindOfClass:[ManageMeterViewController class]])
//            {
//                [self.navigationController popToViewController:controller animated:NO];
//                //break;
//            }
//        }

    } else {
        
        NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
        NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
        
        if ([strUserType isEqualToString:@"Customer"]) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            DashBoardViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }else{
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            TechDashBoardViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TechDashBoardViewController"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }

    }
}

- (IBAction)action_Help:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HelpViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    objByProductVC.strHelpMessage = @"Allows you to add a meter you have registered with the EAA into your App menu.This feature also allows you to inform the EAA if you are adding a meter to your App menu that is not already registered with the EAA.\n\nIf you are adding a meter as a part of Replacing a Meter, please use Replace a Meter to report this action to the EAA.\n\n1.  Enter the Meter Serial Number, as registered with the EAA (or the serial number of your new meter).\n\n2.  Enter a Meter Nickname, to easily identify the meter in the future (Optional)\n\n 3.  Indicate (Yes/No) if this meter is already registered with the EAA.\n\n4.  Submit your entry.\n\nNote: If this a new meter that is not registered with the EAA, you will be taken to the Register a New Meter screen to complete the registration process.";
    [self.navigationController pushViewController:objByProductVC animated:NO];

}
- (IBAction)hideKeyboard:(id)sender{
    
    [self resignFirstResponder];
    
}
//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return arrOfHistory.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"HistoryTableViewCell" forIndexPath:indexPath];
        
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
        
}
- (void)configureCell:(HistoryTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    if (indexPath.row%2==0) {
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:161/255.0f green:172/255.0f blue:179/255.0f alpha:1.0];
    }else{
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:204/255.0f green:172/255.0f blue:84/255.0f alpha:1.0];
    }

    NSDictionary *dictData=arrOfHistory[indexPath.row];
    cell.lbl_LastMeterReading.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CurrentMeterReading"]];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SubmittedDate"]]];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];

    cell.lbl_DAte.text=finalTime;
    cell.lbl_Status.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Status"]];
    cell.lbl_PumpedAmountValue.text=[NSString stringWithFormat:@"%@ Acre-Feet",[dictData valueForKey:@"PumpedAmount"]];
    cell.lbl_ReasonValue.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DeclineReason"]];
    
    if (cell.lbl_LastMeterReading.text.length==0) {
        cell.lbl_LastMeterReading.text=@"NA";
    }

    if ([cell.lbl_Status.text caseInsensitiveCompare:@"New"] == NSOrderedSame || [cell.lbl_Status.text isEqualToString:@""]) {
        
        NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
        NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
        
        if ([strUserType isEqualToString:@"Customer"]) {
            if ([cell.lbl_Status.text isEqualToString:@""]) {
                cell.lbl_Status.text=@"NA";
            }else{
                cell.lbl_Status.text=@"Pending";
            }
        }else{
            if ([cell.lbl_Status.text isEqualToString:@""]) {
                cell.lbl_Status.text=@"NA";
            }
        }
        [cell.lbl_PumpedAmount setHidden:YES];
        [cell.lbl_PumpedAmountValue setHidden:YES];
        [cell.lbl_Reason setHidden:YES];
        [cell.lbl_ReasonValue setHidden:YES];
        [cell.const_PumpedAmount_H setConstant:0];
        [cell.lbl_Comment setHidden:YES];
        [cell.lbl_CommentValue setHidden:YES];
        
        cell.const_PumpedAmount_H.constant=0;
        cell.const_ResetReason_H.constant=0;
        
    } else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Approved"] == NSOrderedSame) {
        
        [cell.lbl_PumpedAmount setHidden:NO];
        [cell.lbl_PumpedAmountValue setHidden:NO];
        [cell.lbl_Reason setHidden:YES];
        [cell.lbl_ReasonValue setHidden:YES];
        [cell.lbl_Comment setHidden:YES];
        [cell.lbl_CommentValue setHidden:YES];
        cell.const_ResetReason_H.constant=0;
        cell.const_PumpedAmount_H.constant=21;
        
    }else if ([cell.lbl_Status.text caseInsensitiveCompare:@"In Progress"] == NSOrderedSame) {
        
        [cell.lbl_PumpedAmount setHidden:YES];
        [cell.lbl_PumpedAmountValue setHidden:YES];
        [cell.lbl_Reason setHidden:YES];
        [cell.lbl_ReasonValue setHidden:YES];
        [cell.const_PumpedAmount_H setConstant:0];
        cell.const_PumpedAmount_H.constant=0;
        cell.const_ResetReason_H.constant=0;
        
    }else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Approved with edit"] == NSOrderedSame) {
        
        [cell.lbl_PumpedAmount setHidden:NO];
        [cell.lbl_PumpedAmountValue setHidden:NO];
        [cell.lbl_Reason setHidden:NO];
        [cell.lbl_ReasonValue setHidden:NO];
        [cell.const_PumpedAmount_H setConstant:21];
        cell.const_ResetReason_H.constant=23;
    }
    else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Rejected"] == NSOrderedSame) {
        
        [cell.lbl_PumpedAmount setHidden:YES];
        [cell.lbl_PumpedAmountValue setHidden:YES];
        [cell.lbl_Reason setHidden:NO];
        [cell.lbl_ReasonValue setHidden:NO];
        [cell.const_PumpedAmount_H setConstant:0];
        cell.const_ResetReason_H.constant=23;
    }
    else{
        [cell.lbl_PumpedAmount setHidden:NO];
        [cell.lbl_PumpedAmountValue setHidden:NO];
        [cell.lbl_Reason setHidden:NO];
        [cell.lbl_ReasonValue setHidden:NO];
        cell.const_ResetReason_H.constant=23;
        cell.const_PumpedAmount_H.constant=21;
    }
    if ([cell.lbl_Status.text isEqualToString:@"Reject"]) {
        
        cell.lbl_Status.text=@"Rejected";
        
    }
    
    //http://pcrms.pestream.com/Documents/_20161109133414155.png
    //http://eaa.stagingsoftware.com/rimages/Meter1.jpg
    
    NSString *strUrlImage=[NSString stringWithFormat:@"%@%@%@",MainUrlImage,UrldownloadImages,[dictData valueForKey:@"Images"]];

    [cell.meterImgView setImageWithURL:[NSURL URLWithString:strUrlImage] placeholderImage:[UIImage imageNamed:@"NoImage.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
 //   [_txtPermitNo resignFirstResponder];
    [_txt_SerialNo resignFirstResponder];
//    [_txt_MeterReading resignFirstResponder];
//    [_txt_MeterNickName resignFirstResponder];
}

- (IBAction)action_AddMeter:(id)sender {
    
    
    BOOL isAlreadySent=[self fetchFromCoreDataDateReadingToCheck:[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterId"]]];
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
    }else{
        
        isAlreadySent=NO;
        
    }

    isAlreadySent=NO;
    
    if (isAlreadySent) {
        
        NSString *strTitle = @"Alert";
        NSString *strMsg = @"Reading can be submitted only once in 24 hrs.";
        [global AlertMethod:strTitle :strMsg];
    }else{

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ReportMeterReadingViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
    
    NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterNumber"]];
    NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterName"]];

    NSString *strMeterNamenNo;
    
    if (strMeterNumber.length==0) {
    
        if (strMeterNamee.length==0) {
            
            strMeterNamenNo=@"";
            
        } else {

           strMeterNamenNo=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterName"]];
            
        }
        
    }else{
        
        
        if (strMeterNamee.length==0) {
            
            strMeterNamenNo=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterNumber"]];
            
        } else {

        strMeterNamenNo=[NSString stringWithFormat:@"%@-%@",[_dictDataManageMeter valueForKey:@"MeterName"],[_dictDataManageMeter valueForKey:@"MeterNumber"]];
            
        }

    }
    objByProductVC.strMeterNickNamenSerialNoDefault=strMeterNamenNo;
    objByProductVC.strMeteridDefault=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterId"]];
    objByProductVC.dictDataManageMeterList=_dictDataManageMeter;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}

//============================================================================
#pragma mark- METHODS
//============================================================================

-(void)showThankYouView{
    
    CGRect frameForHiddenView=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    frameForHiddenView.origin.x=0;
    frameForHiddenView.origin.y=0;
    [_ThankYouView setFrame:frameForHiddenView];
    [self.view addSubview:_ThankYouView];
    
}
-(NSString *)validationCheck
{
    NSString *errorMessage;
//    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
//    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if (yesFromManageMeter) {
        if (_txt_SerialNo.text.length==0) {
            errorMessage = @"Please enter serial number";
        }
        else if (_txt_SerialNo.text.length<4)
        {
            errorMessage = @"Please enter atleast 4 digits in serial number";
        }

//        else if (_txt_MeterNickName.text.length==0)
//        {
//            errorMessage = @"Please enter nickname";
//        }
//        else if (_txt_MeterReading.text.length==0)
//        {
//            errorMessage = @"Please enter start of year meter reading";
//        }
//        else if (_txtPermitNo.text.length==0)
//        {
//            errorMessage = @"Please enter EAA permit number";
//        }
//        else if (_txt_WellPermitNo.text.length==0)
//        {
//            errorMessage = @"Please enter EAA well number";
//        }
 
    } else {
        if (_txt_SerialNo.text.length==0) {
            errorMessage = @"Please enter serial number";
        }
        else if (_txt_SerialNo.text.length<4)
        {
            errorMessage = @"Please enter atleast 4 digits in serial number";
        }

//        else if (_txt_MeterNickName.text.length==0)
//        {
//            errorMessage = @"Please enter nickname";
//        }
    }
    return errorMessage;
}

-(void)submitMeter
{
    [self.view endEditing:true];
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strCustomerIdd = [NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]];
    NSString *strAuthIdd = [NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"AuthId"]];
    
    NSString *strUserType=[dictLoginDetail valueForKey:@"LoginType"];
    if ([strUserType isEqualToString:@"Customer"])
    {
    }
    else{
        strCustomerIdd=@"0";
    }
    
    NSString *createdDate,*modifyDate,*strSerialNo,*strNickName,*meterReading,*permitNo,*wellPermitNo,*strStatus,*strInternalNotes,*strComments,*strMeterId;
  
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy HH:mm:ss";
    if (yesFromManageMeter)
    {
        //yaha par update karna
        strSerialNo=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterNumber"]];
        strNickName=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterName"]];
        meterReading=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"InitialMeterReading"]];
        permitNo=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"PermitNumber"]];
        strStatus=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterStatus"]];
        strInternalNotes=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"InternalNotes"]];
        strComments=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"Comments"]];
        
        modifyDate = [formatter stringFromDate:[NSDate date]];
        wellPermitNo=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"WellPermitNumber"]];
        strMeterId=[NSString stringWithFormat:@"%@",[_dictDataManageMeter valueForKey:@"MeterId"]];
        
    }
    else
    {
        //Yaha par add karna
       
        createdDate = [formatter stringFromDate:[NSDate date]];
        strNickName=[_txt_MeterNickName.text stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
        strSerialNo=[_txt_SerialNo.text stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
       // meterReading=[_txt_MeterReading.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
       // permitNo=[_txtPermitNo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
       // wellPermitNo=[_txt_WellPermitNo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        meterReading = [strInitialMeterReading stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        permitNo = [strPermitNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        wellPermitNo = [strWellPermitNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

        modifyDate = [formatter stringFromDate:[NSDate date]];

        strStatus=@"Active";
        strInternalNotes=@"";
        strComments=@"";
//        modifyDate=@"";
        strMeterId=@"0";
    
    }
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
        strNickName=[_txt_MeterNickName.text stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
        strSerialNo=[_txt_SerialNo.text stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];

        //        meterReading=[_txt_MeterReading.text stringByTrimmingCharactersInSet:
//                      [NSCharacterSet whitespaceCharacterSet]];
//        permitNo=[_txtPermitNo.text stringByTrimmingCharactersInSet:
//                  [NSCharacterSet whitespaceCharacterSet]];
//        wellPermitNo=[_txt_WellPermitNo.text stringByTrimmingCharactersInSet:
//                      [NSCharacterSet whitespaceCharacterSet]];
        meterReading = [strInitialMeterReading stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        permitNo = [strPermitNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        wellPermitNo = [strWellPermitNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
  
    }else{
        
        strNickName=[_txt_MeterNickName.text stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
        strSerialNo=[_txt_SerialNo.text stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];

        //        meterReading=[_txt_MeterReading.text stringByTrimmingCharactersInSet:
//                      [NSCharacterSet whitespaceCharacterSet]];
//        permitNo=[_txtPermitNo.text stringByTrimmingCharactersInSet:
//                  [NSCharacterSet whitespaceCharacterSet]];
//        wellPermitNo=[_txt_WellPermitNo.text stringByTrimmingCharactersInSet:
//                      [NSCharacterSet whitespaceCharacterSet]];
        meterReading = [strInitialMeterReading stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        permitNo = [strPermitNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        wellPermitNo = [strWellPermitNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
  
    }
    if (latitude.length==0) {
        latitude=@"0.00000";
    }
    if (longtitude.length==0) {
        longtitude=@"0.00000";
    }
    
    strSerialNo=[strSerialNo uppercaseString];
    permitNo=[permitNo uppercaseString];
    wellPermitNo=[wellPermitNo uppercaseString];
    
    if (strSerialNo.length==0) {
        strSerialNo=@"";
    }
    if (meterReading.length==0) {
        meterReading=@"";
    }
    if (permitNo.length==0) {
        permitNo=@"";
    }
    if (wellPermitNo.length==0) {
        wellPermitNo=@"";
    }
    if (strInternalNotes.length==0) {
        strInternalNotes=@"";
    }
    if (strComments.length==0) {
        strComments=@"";
    }
    if (createdDate.length==0) {
        createdDate=@"";
    }
    if (modifyDate.length==0) {
        modifyDate=@"";
    }
    if (wellPermitNo.length==0) {
        wellPermitNo=@"";
    }

    NSArray *objects = [NSArray arrayWithObjects:
                        strMeterId,
                        strCustomerIdd,
                        strSerialNo,
                        strNickName,
                        meterReading,
                        latitude,
                        longtitude,
                        permitNo,
                        @"",
                        strStatus,
                        strInternalNotes,
                        strComments,
                        createdDate,
                        modifyDate,
                        strAuthIdd,
                        strAuthIdd,
                        wellPermitNo,
                        @"",nil];
    
    NSArray *keys = [NSArray arrayWithObjects:
                     @"MeterId",
                     @"CustomerId",
                     @"MeterNumber",
                     @"MeterName",
                     @"InitialMeterReading",
                     @"MeterLat",
                     @"MeterLong",
                     @"PermitNumber",
                     @"MeterImages",
                     @"MeterStatus",
                     @"InternalNotes",
                     @"Comments",
                     @"CreatedDate",
                     @"ModifyDate",
                     @"CreatedBy",
                     @"ModifyBy",
                     @"WellPermitNumber",
                     @"CustomerName",nil];
    

    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Add Meter JSON: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
   
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlAddUpdateMeter];
    if(yesFromManageMeter){
      strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlUpdateMeter];
    }
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Submitting Request..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"AddUpdateMeter" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     [self afterServerResponseAddMeter:response];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}

-(void)afterServerResponseAddMeter :(NSDictionary*)dictResponse{
    
    if (dictResponse.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:Sorry
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        BOOL isSuccess=[[dictResponse valueForKey:@"IsSuccess"] boolValue];
        
        if (isSuccess) {
            
            [self showThankYouView];
            
        } else {
            
            NSArray *arrOfError=[dictResponse valueForKey:@"Error"];
            if ([arrOfError isKindOfClass:[NSArray class]]) {
                
                if (arrOfError.count==0) {
                                        
                    NSString *strTitle = Alert;
                    NSString *strMsg = Sorry;
                    [global AlertMethod:strTitle :strMsg];
                    
                } else {
                    
                    NSString *strErrorMsg=arrOfError[0];
                    
                    if ([strErrorMsg isEqualToString:@"Meter serial number already registered."]) {
                        
                        NSString *strTrueorFalse=arrOfError[2];
                        
                        if ([strTrueorFalse isEqualToString:@"True"]) {
                         
                            //yaha par vhi alert dikha kr is screen p rehna
                            UIAlertController *alert= [UIAlertController
                                                       alertControllerWithTitle:@"Alert!"
                                                       message:strErrorMsg
                                                       preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                        handler:^(UIAlertAction * action)
                                                  {
                                                      
                                                  }];
                            [alert addAction:yes];
                            [self presentViewController:alert animated:YES completion:nil];

                            
                        } else {
                            
                            
                            UIAlertController *alert= [UIAlertController
                                                       alertControllerWithTitle:@"Alert!"
                                                       message:@"Meter serial number already registered with another Account. Do you want to access this meter?"
                                                       preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                        handler:^(UIAlertAction * action)
                                                  {
                                                      
                                                      //Yaha par wapis se service call krni hai jo ki meter id send karegiiii
                                                      
                                                      Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
                                                      NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
                                                      if (netStatusWify1== NotReachable)
                                                      {
                                                          NSString *strTitle = Alert;
                                                          NSString *strMsg = ErrorInternetMsg;
                                                          [global AlertMethod:strTitle :strMsg];
                                                          [DejalBezelActivityView removeView];
                                                      }
                                                      else
                                                      {
                                                          
                                                          NSString *strMeterIdComingFromServer=arrOfError[1];
                                                          [self methodAccesDelegate:strMeterIdComingFromServer];
                                                          
                                                      }

                                                  }];
                            [alert addAction:yes];
                            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                        handler:^(UIAlertAction * action)
                                                  {
                                                      
                                                  }];
                            [alert addAction:no];
                            [self presentViewController:alert animated:YES completion:nil];

                            
                        }
                        
                    } else {
                        
                        NSString *joinedError = [arrOfError componentsJoinedByString:@","];
                        
                        UIAlertController *alert= [UIAlertController
                                                   alertControllerWithTitle:@"Alert!"
                                                   message:joinedError
                                                   preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action)
                                              {
                                                  
                                              }];
                        [alert addAction:yes];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }
                    
                }
                
                
            } else {
                
                NSString *strTitle = Alert;
                NSString *strMsg = Sorry;
                [global AlertMethod:strTitle :strMsg];
                
            }
            
        }
    }
}

-(void)methodAccesDelegate :(NSString*)strMeterIdd{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Submitting Request..."];
    
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginResponse=[userDefaults objectForKey:@"LoginData"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *currentDate = [dateFormat stringFromDate:[NSDate date]];

    NSDictionary *dictNewMeterInfo = @{
                                       @"MeterId":strMeterIdd,
                                       @"CustomerId":[dictLoginResponse valueForKey:@"UserId"],
                                       @"DelegateStatus":@"Active",
                                       @"CreatedDate":currentDate,
                                       @"CreatedBy":[dictLoginResponse valueForKey:@"AuthId"],
                                       };
    
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictNewMeterInfo])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictNewMeterInfo options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Access Delegate JSON: %@", jsonString);
        }
    }
    
    [self sendingFinalJson:jsonString];

}

-(void)sendingFinalJson :(NSString*)jsonString
{
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlAccessDelegate];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             [DejalBezelActivityView removeView];
             NSLog(@"%@",response);
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     
                     [self showThankYouView];
                     
                     NSString *strMsgToDisplay=[NSString stringWithFormat:@"Meter# %@ \n Added Successfully. Please check your Mange Meter list.",[_txt_SerialNo.text stringByTrimmingCharactersInSet:                                                                                                                                       [NSCharacterSet whitespaceCharacterSet]]];
                     
                     [self localNotification:@"AccessDelegate" :strMsgToDisplay];
                     
                 }
                 else
                 {
                     [DejalBezelActivityView removeView];
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}

-(void)localNotification :(NSString*)strType :(NSString*)strMsg
{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
    {
        UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
        objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"Success!" arguments:nil];
        objNotificationContent.body = [NSString stringWithFormat:@"%@ %@",strType,strMsg];
        objNotificationContent.sound = [UNNotificationSound defaultSound];
        objNotificationContent.userInfo = @{ @"Notification" : strType,@"Message" : strMsg };
        /// 4. update application icon badge number
        // objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
        
        // Deliver the notification in five seconds.
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
                                                      triggerWithTimeInterval:1.f repeats:NO];
        
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"Notification"
                                                                              content:objNotificationContent trigger:trigger];
        /// 3. schedule localNotification
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"Local Notification succeeded");
            }
            else {
                NSLog(@"Local Notification failed");
            }
        }];
        
    }else{
        
        UIApplication *app = [UIApplication sharedApplication];
        UILocalNotification* alarm = [[UILocalNotification alloc] init];
        if (alarm)
        {
            alarm.soundName = UILocalNotificationDefaultSoundName;
            alarm.fireDate = [NSDate dateWithTimeIntervalSinceNow:0.0f];
            alarm.timeZone = [NSTimeZone defaultTimeZone];
            alarm.repeatInterval = 0;
            alarm.userInfo = @{ @"Notification" : strType,@"Message" : strMsg };
            alarm.alertBody = [NSString stringWithFormat:@"%@ %@",strType,strMsg];
            [app scheduleLocalNotification:alarm];
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)alocations
{
    
    CLLocation *currentLocation;
    currentLocation =[alocations lastObject];
    int degrees = currentLocation.coordinate.latitude;
    double decimal = fabs(currentLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    
    latitude = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
                     degrees, minutes, seconds];
    
   // locationLatitude = currentLocation.coordinate.latitude;
    degrees = currentLocation.coordinate.longitude;
    decimal = fabs(currentLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    longtitude = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
                       degrees, minutes, seconds];
    
    [locationManager stopUpdatingLocation];
    
}

//============================================================================
//============================================================================
#pragma mark- Text Field Delegate Methods
//============================================================================
//============================================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    
    
    textField.leftView = leftView;
    
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    isEdited=YES;
    [textField resignFirstResponder];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *strToCheckIs;
    
    if (string.length==0) {
        strToCheckIs=textField.text;
    }else{
        strToCheckIs=string;
    }
    
    if (textField.tag==1) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"1234567890-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
        NSRange rangeNew = [strToCheckIs rangeOfCharacterFromSet:cset];
        if (rangeNew.location == NSNotFound) {
            // no ( or ) in the string
            return NO;
        } else {
            
            int noToCheck;
            noToCheck=13;
            //
            NSCharacterSet *csetNew = [NSCharacterSet characterSetWithCharactersInString:@"-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
            NSRange rangeNew2 = [textField.text rangeOfCharacterFromSet:csetNew];
            if (rangeNew2.location == NSNotFound) {
                // no ( or ) in the string
               noToCheck=13;
            } else {
                noToCheck=17;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return (newLength > noToCheck) ? NO : YES;

        }
        
    }
    else if ((textField.tag==3) || (textField.tag==4)) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"1234567890-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
        NSRange rangeNew = [strToCheckIs rangeOfCharacterFromSet:cset];
        if (rangeNew.location == NSNotFound) {
            // no ( or ) in the string
            return NO;
        } else {
            
            int noToCheck;
            noToCheck=5000;
            //
            NSCharacterSet *csetNew = [NSCharacterSet characterSetWithCharactersInString:@"-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
            NSRange rangeNew2 = [textField.text rangeOfCharacterFromSet:csetNew];
            if (rangeNew2.location == NSNotFound) {
                // no ( or ) in the string
                noToCheck=5000;
            } else {
                noToCheck=5000;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return (newLength > noToCheck) ? NO : YES;
            
        }
        
    }
    
    else
    {
        
        return YES;
        
    }
}

- (IBAction)action_SerialnoHelp:(id)sender {
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Help!"
                               message:@"1.If the serial number contains only numbers, it can be maximum 13 characters long.\n 2.If the serial number is a combination of characters, numbers and hyphen (-), it's maximum length is 17 characters."
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];

}

-(BOOL)fetchFromCoreDataDateReadingToCheck :(NSString*)meterIdd {
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDateReading = [appDelegate managedObjectContext];
    entityDateReading=[NSEntityDescription entityForName:@"DateMeterReading" inManagedObjectContext:contextDateReading];
    requestDateReading = [[NSFetchRequest alloc] init];
    [requestDateReading setEntity:entityDateReading];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@ AND meterId = %@",strUsername,meterIdd];
    [requestDateReading setPredicate:predicate];
    
    sortDescriptorDateReading = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsDateReading = [NSArray arrayWithObject:sortDescriptorDateReading];
    
    [requestDateReading setSortDescriptors:sortDescriptorsDateReading];
    
    self.fetchedResultsControllerDateReading = [[NSFetchedResultsController alloc] initWithFetchRequest:requestDateReading managedObjectContext:contextDateReading sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerDateReading setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerDateReading performFetch:&error];
    arrAllObjDateReading = [self.fetchedResultsControllerDateReading fetchedObjects];
    if ([arrAllObjDateReading count] == 0)
    {
        return NO;
    }
    else
    {
        
        matchesDateReading = arrAllObjDateReading[0];
        //dateReadingBeingSent
        
        NSString *strReadingDate=[matchesDateReading valueForKey:@"dateReadingBeingSent"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSDate* ReadingDate = [dateFormatter dateFromString:strReadingDate];
        NSString *strCurrentDate = [dateFormatter stringFromDate:[NSDate date]];
        NSDate* CurrentDate = [dateFormatter dateFromString:strCurrentDate];
        
        NSComparisonResult result = [ReadingDate compare:CurrentDate];
        
        if(result == NSOrderedDescending)
        {
            NSLog(@"ReadingDate is Greater");//
            return NO;
        }
        else if(result == NSOrderedAscending)
        {
            NSLog(@"CurrentDate is greater");//
            return NO;
        }
        else
        {
            NSLog(@"Equal date");//
            return YES;
        }
    }
}

- (IBAction)action_YesMeterRegistered:(id)sender {
    
    isYesMeterRegistered=YES;
    [_btn_YesMeterRegistered setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_Btn_NoMeterRegistered setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
}

- (IBAction)action_NoMeterRegistered:(id)sender {
    
    isYesMeterRegistered=NO;
    [_Btn_NoMeterRegistered setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btn_YesMeterRegistered setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

}
@end
