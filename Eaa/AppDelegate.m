//
//  AppDelegate.m
//  Eaa
//
//  Created by Saavan Patidar on 27/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "AppDelegate.h"
#import "OutboxViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <UserNotifications/UserNotifications.h>
#import "Global.h"
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate ()
{
    Global *global;
    OutboxViewController *outBoxObj;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //============================================================================
    
    NSDate *lastAlertDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"lastAlertDate"];
    
    if(lastAlertDate==nil) {
        [self checkAppVersion];
    } else {
        if(![[NSCalendar currentCalendar] isDateInToday:lastAlertDate]) {
            [self checkAppVersion];
            
            NSDate *today= [NSDate date];
            [[NSUserDefaults standardUserDefaults] setObject:today forKey:@"lastAlertDate"];
        }
    }
    
    global = [[Global alloc] init];

    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            // Will get here on both iOS 7 & 8 even though camera permissions weren't required
            // until iOS 8. So for iOS 7 permission will always be granted.
            if (granted) {
                // Permission has been granted. Use dispatch_async for any UI updating
                // code because this block may be executed in a thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Permission has been Granted.
                });
            } else {
                // Permission has been denied.
            }
        }];
    } else {
        
        // Permission has been Granted.
        
    }

    [self performSelector:@selector(callOutboxSendMethod) withObject:nil afterDelay:0.0];
    
    sleep(3.0);
    
    //============================================================================
    //============================================================================
    //Local Notification
   
   [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert)
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  if (!error) {
                                       NSLog(@"request succeeded!");
                                  }
                              }];
    }else  if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
 
    if (locationNotification)
    {
        
    }

    [FIRApp configure];
    [self connectToFCM];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
   /* NSString *fireBaseToken = [[FIRInstanceID instanceID] token];
   // NSLog(@"InstanceID token: %@", fireBaseToken);
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
    [userDefaults synchronize];*/
    [FIRMessaging messaging].delegate = self;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:FIRMessagingRegistrationTokenRefreshedNotification object:nil];

    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                         NSError * _Nullable error) {
       if (error != nil) {
         NSLog(@"Error fetching remote instance ID: %@", error);
       } else {
         NSLog(@"Remote instance ID token: %@", result.token);
         NSString* fireBaseToken =
           [NSString stringWithFormat:@"%@", result.token];
           NSLog(@"InstanceID token: %@", fireBaseToken);
           NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
           [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
           [userDefaults synchronize];
       }
     }];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        NSLog(@"didRegisterUser");
        [application registerForRemoteNotifications];
    }
}

//-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
//{
////    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
//  
//    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
//}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    
        [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                         NSError * _Nullable error) {
       if (error != nil) {
         NSLog(@"Error fetching remote instance ID: %@", error);
       } else {
         NSLog(@"Remote instance ID token: %@", result.token);
         NSString* fireBaseToken =
           [NSString stringWithFormat:@"%@", result.token];
           NSLog(@"InstanceID token: %@", fireBaseToken);
           NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
           [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
           [userDefaults synchronize];
       }
     }];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
    
}
-(void)connectToFCM
{
    
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                         NSError * _Nullable error) {
       if (error != nil) {
         NSLog(@"Error fetching remote instance ID: %@", error);
       } else {
         NSLog(@"Remote instance ID token: %@", result.token);
         NSString* fireBaseToken =
           [NSString stringWithFormat:@"%@", result.token];
           NSLog(@"InstanceID token: %@", fireBaseToken);
           NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
           [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
           [userDefaults synchronize];
       }
     }];

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[UIApplication sharedApplication].applicationIconBadgeNumber+1];
    NSLog(@"%@", userInfo);
    NSString *strMessage = [userInfo valueForKey:@"message"];
    NSString *strTitle = [userInfo valueForKey:@"title"];
    NSLog(@"Message---%@", strMessage);
    NSLog(@"Title---%@", strTitle);
    [[[UIAlertView alloc] initWithTitle:@"Message" message:[userInfo valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"%@", userInfo);
    [[[UIAlertView alloc] initWithTitle:@"Message" message:[userInfo valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    completionHandler(UIBackgroundFetchResultNewData);
}

//- (void)tokenRefreshNotification:(NSNotification *)notification {
//    
//    NSString *fireBaseToken = [[FIRInstanceID instanceID] token];
//   // NSLog(@"InstanceID token: %@", fireBaseToken);
//    if(fireBaseToken != nil)
//    {
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
//        [userDefaults synchronize];
//        [self connectToFCM];
//    }
//}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center  willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", notification.request.content.userInfo);
    [[[UIAlertView alloc] initWithTitle:@"Message" message:[notification.request.content.userInfo valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler
{
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
}

-(void)callOutboxSendMethod{
    
    outBoxObj=[[OutboxViewController alloc]init];
    [outBoxObj fetchFromCoreDataMeterReading];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self connectToFCM];

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Eaa"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

//==================================================
#pragma mark - ----Core Data stack-------
//==================================================

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Eaa" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Eaa.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
        NSFileManager *fm = [NSFileManager defaultManager];
        
        // Move Incompatible Store
        if ([fm fileExistsAtPath:[storeURL path]]) {
            NSURL *corruptURL = [[self applicationIncompatibleStoresDirectory] URLByAppendingPathComponent:[self nameForIncompatibleStore]];
            
            // Move Corrupt Store
            NSError *errorMoveStore = nil;
            [fm moveItemAtURL:storeURL toURL:corruptURL error:&errorMoveStore];
            
            if (errorMoveStore) {
                NSLog(@"Unable to move corrupt store.");
            }
        }
    }
    return _persistentStoreCoordinator;
}
- (NSURL *)applicationStoresDirectory {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *applicationApplicationSupportDirectory = [[fm URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *URL = [applicationApplicationSupportDirectory URLByAppendingPathComponent:@"Stores"];
    
    if (![fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm createDirectoryAtURL:URL withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error) {
            NSLog(@"Unable to create directory for data stores.");
            
            return nil;
        }
    }
    return URL;
}

- (NSURL *)applicationIncompatibleStoresDirectory {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *URL = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Incompatible"];
    
    if (![fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm createDirectoryAtURL:URL withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error) {
            NSLog(@"Unable to create directory for corrupt data stores.");
            
            return nil;
        }
    }
    
    return URL;
}
- (NSString *)nameForIncompatibleStore {
    // Initialize Date Formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Configure Date Formatter
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    
    return [NSString stringWithFormat:@"%@.sqlite", [dateFormatter stringFromDate:[NSDate date]]];
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    NSString *strNotification = notification.userInfo[@"Notification"];
    if ([strNotification isEqualToString:@"AccessDelegate"]) {
        
        [global AlertMethod:@"Info." :notification.userInfo[@"Message"]];
        
    }
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        
    }
    else{
        
    }
   // application.applicationIconBadgeNumber=0;
}

-(void)checkAppVersion
{
    UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    topWindow.rootViewController = [UIViewController new];
    topWindow.windowLevel = UIWindowLevelAlert + 1;
    
//    NSString *strAppStoreId=@"1181490468";
    NSString *strBundleIdentifier=@"com.quacito.EAAMetersMatter";
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", strBundleIdentifier]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request
     
                                       queue:[NSOperationQueue mainQueue]
     
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if (!error) {
                                   
                                   NSError* parseError;
                                   
                                   NSDictionary *appMetadataDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseError];
                                   
                                   NSArray *resultsArray = (appMetadataDictionary)?[appMetadataDictionary objectForKey:@"results"]:nil;
                                   
                                   NSDictionary *resultsDic = [resultsArray firstObject];
                                   
                                   if (resultsDic) {
                                       
                                       // compare version with your apps local version
                                       
                                       NSString *iTunesVersion = [resultsDic objectForKey:@"version"];
                                       
                                       NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)@"CFBundleShortVersionString"];
                                       
                                       if (iTunesVersion && [appVersion compare:iTunesVersion] != NSOrderedDescending&&[appVersion compare:iTunesVersion] != NSOrderedSame) {
                                           
                                           UIAlertController * alert=   [UIAlertController
                                                                         
                                                                         alertControllerWithTitle:@"Update"
                                                                         
                                                                         message:@"A new version of this app is available."
                                                                         
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                           
                                           
                                           UIAlertAction* updateBtn = [UIAlertAction
                                                                       
                                                                       actionWithTitle:@"Update"
                                                                       
                                                                       style:UIAlertActionStyleDefault
                                                                       
                                                                       handler:^(UIAlertAction * action)
                                                                       
                                                                       {
                                                                           
//                                                                                                                                                      NSString *iTunesLink = [NSString stringWithFormat:@"itms://itunes.apple.com/in/app/apple-store/id%@?mt=8",strAppStoreId];
                                                                           
                                                                           NSString *iTunesLink = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/in/app/id1181490468?mt=8"];
                                                                           
                                                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                                                       }];
                                           
                                           UIAlertAction *Cancelbtn = [UIAlertAction
                                                                       
                                                                       actionWithTitle:@"Cancel"
                                                                       
                                                                       style:UIAlertActionStyleDefault
                                                                       
                                                                       handler:^(UIAlertAction * action)
                                                                       
                                                                       {
                                                                           
                                                                           [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
                                                                           
                                                                       }];
                                           
                                           [alert addAction:Cancelbtn];
                                           
                                           [alert addAction:updateBtn];

                                           [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
                                       }
                                   }
                               } else {
                                   // error occurred with http(s) request
                                   NSLog(@"error occurred communicating with iTunes");
                               }
                           }];
    
}
@end
