//
//  CommonTBLCellVC.h
//  Eaa
//
//  Created by Navin Patidar on 12/5/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonTBLCellVC : UITableViewCell

//New MENU

@property (weak, nonatomic) IBOutlet UILabel *Dash_lbl_MeterNumber;
@property (weak, nonatomic) IBOutlet UILabel *Dash_lbl_MeterReading;
@property (weak, nonatomic) IBOutlet UILabel *Dash_lbl_MeterWell;
@property (weak, nonatomic) IBOutlet UILabel *Dash_lbl_MeterNickName;
@property (weak, nonatomic) IBOutlet UILabel *Dash_lbl_Date;
@property (weak, nonatomic) IBOutlet UIButton *Dash_Btn_Status;

//Nilind

@property (weak, nonatomic) IBOutlet UILabel *lblBattery_Voltage;
@property (weak, nonatomic) IBOutlet UILabel *lblGPMAverage;


//Nilind For Existing  and New User Table
@property (weak, nonatomic) IBOutlet UILabel *lblMeterSerialNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;

@end

NS_ASSUME_NONNULL_END
