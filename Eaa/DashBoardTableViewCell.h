//
//  DashBoardTableViewCell.h
//  Eaa
//
//  Created by Saavan Patidar on 07/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashBoardTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblColorLine;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Status;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Comment;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CommentValue;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Reason;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ReasonValue;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PumpedAmount;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PumpedAmountValue;
@property (strong, nonatomic) IBOutlet UIButton *btnAdd;
@property (strong, nonatomic) IBOutlet UIButton *btnHistory;
@property (strong, nonatomic) IBOutlet UIButton *btnNewReading;

@property (strong, nonatomic) IBOutlet UILabel *lblNickName;
@property (strong, nonatomic) IBOutlet UILabel *lblMeterReading;
@property (strong, nonatomic) IBOutlet UILabel *lblLastMeterreading;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblMeterSerialNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblMeterType;

@property (strong, nonatomic) IBOutlet UILabel *lblStatusLabell;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_PumpedAmount_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ResetReason_H;

@end
