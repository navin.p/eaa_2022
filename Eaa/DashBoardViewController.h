//
//  DashBoardViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 07/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface DashBoardViewController : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityDashBoardList;
    NSManagedObjectContext *contextDashBoardList;
    NSFetchRequest *requestDashBoardList;
    NSSortDescriptor *sortDescriptorDashBoardList;
    NSArray *sortDescriptorsDashBoardList;
    NSManagedObject *matchesDashBoardList;
    NSArray *arrAllObjDashBoardList;
    
    NSEntityDescription *entityDateReading;
    NSManagedObjectContext *contextDateReading;
    NSFetchRequest *requestDateReading;
    NSSortDescriptor *sortDescriptorDateReading;
    NSArray *sortDescriptorsDateReading;
    NSManagedObject *matchesDateReading;
    NSArray *arrAllObjDateReading;

}

- (IBAction)action_MenuBar:(id)sender;
- (IBAction)action_AddMeterReading:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewHistory;
@property (strong, nonatomic) IBOutlet UITableView *tblViewSideMenu;
@property (strong, nonatomic) IBOutlet UIView *viewwSideMenu;
@property (strong, nonatomic) IBOutlet UIView *hiddenView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImgView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_Img_W;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_MenuUserName_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_lblMenuUserName_Bottom;
@property (strong, nonatomic) IBOutlet UILabel *lbl_UserName;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerDashBoardList;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerDateReading;

@end
