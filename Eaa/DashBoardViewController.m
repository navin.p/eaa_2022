//
//  DashBoardViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 07/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "DashBoardViewController.h"
#import "DashBoardTableViewCell.h"
#import "SlideMenuTableViewCell.h"
#import "ReportMeterReadingViewController.h"
#import "AddMeterViewController.h"
#import "ManageMeterViewController.h"
#import "OutboxViewController.h"
#import "DisclaimerViewController.h"
#import "HelpViewController.h"
#import "LoginViewController.h"
#import "TermsConditionViewController.h"
#import "HistoryViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "DashBoardListt+CoreDataProperties.h"
#import "NotificationHubViewController.h"
#import "MBProgressHUD.h"
#import "Eaa-Swift.h"

@interface DashBoardViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *arrOfImgSlideMenu,*arrNameSlideMenu,*arrOfStatus;
    Global *global;
    NSArray *arrDashBoardResponse;
    OutboxViewController *outBoxObj;
    NSString *strAccuracyTest;
}


@end

@implementation DashBoardViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //============================================================================
    //============================================================================
    
       global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

   // arrOfImgSlideMenu=[NSArray arrayWithObjects:@"home_icon.png",@"add meter.png",@"add meter reading.png",@"add meter.png",@"history.png",@"outbox.png",@"help.png",@"disclaimer.png",@"logout.png",@"offline mode.png", nil];
    
   // arrOfImgSlideMenu=[NSArray arrayWithObjects:@"home_icon.png",@"add meter.png",@"add meter reading.png",@"meter_registration",@"remove_meter",@"your meter list.png",@"your meter list.png",@"history.png",@"outbox.png",@"help.png",@"disclaimer.png",@"logout.png", nil];
    
    // arrOfImgSlideMenu=[NSArray arrayWithObjects:@"home_icon.png",@"add meter.png",@"add meter reading.png",@"meter_registration",@"remove_meter",@"your meter list.png",@"history.png",@"outbox.png",@"history.png",@"help.png",@"disclaimer.png",@"logout.png", nil];
    
    //arrNameSlideMenu=[NSArray arrayWithObjects:@"Home",@"Add Meter",@"Report a Meter Reading",@"Manage Meters",@"History",@"Outbox",@"Help",@"Terms & Conditions",@"Logout", nil];
    
    // arrNameSlideMenu=[NSArray arrayWithObjects:@"Home",@"Add a Meter",@"Report a Meter Reading",@"Report an Accuracy Test",@"Replace a Meter",@"Manage Meters",@"Manage Accuracy Test",@"History",@"Outbox",@"Help",@"Terms & Conditions",@"Logout", nil];
    
     // arrNameSlideMenu=[NSArray arrayWithObjects:@"Home",@"Add a Meter",@"Report a Meter Reading",@"Report an Accuracy Test",@"Replace a Meter",@"Manage Meters",@"History",@"Outbox",@"Notification Hub",@"Help",@"Terms & Conditions",@"Logout", nil];
    
    //Report a Meter Reading remove by Navin on 08NOV2019 referance by Priyashi android developer
    
    NSUserDefaults *mobileCofigData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse1=[mobileCofigData objectForKey:@"MobileCofigData"];
    strAccuracyTest = [dictResponse1 valueForKey:@"Accuracytest"];
    
    if ([strAccuracyTest boolValue] == YES) {
        arrNameSlideMenu=[NSArray arrayWithObjects:@"Home",@"Add a Meter",@"Report an Accuracy Test",@"Replace a Meter",@"Manage Meters",@"History",@"Outbox",@"Notification Hub",@"Help",@"Terms & Conditions",@"Logout", nil];
        arrOfImgSlideMenu=[NSArray arrayWithObjects:@"home_icon.png",@"add meter.png",@"meter_registration",@"remove_meter",@"your meter list.png",@"history.png",@"outbox.png",@"history.png",@"help.png",@"disclaimer.png",@"logout.png", nil];
    }else {
        arrNameSlideMenu=[NSArray arrayWithObjects:@"Home",@"Add a Meter",@"Replace a Meter",@"Manage Meters",@"History",@"Outbox",@"Notification Hub",@"Help",@"Terms & Conditions",@"Logout", nil];
        arrOfImgSlideMenu=[NSArray arrayWithObjects:@"home_icon.png",@"add meter.png",@"remove_meter",@"your meter list.png",@"history.png",@"outbox.png",@"history.png",@"help.png",@"disclaimer.png",@"logout.png", nil];
    }
    
    
    
   
    arrOfStatus=[NSArray arrayWithObjects:@"New",@"Approved",@"Rejected",@"In Progress",@"Approved with edit",@"Rejected",@"New",@"Approved",@"Rejected", nil];

    if ([UIScreen mainScreen].bounds.size.height==480) {
        _const_MenuUserName_H.constant=40;
        _const_lblMenuUserName_Bottom.constant=0;
    }else if ([UIScreen mainScreen].bounds.size.height==568){
        _const_MenuUserName_H.constant=70;
        _const_lblMenuUserName_Bottom.constant=15;
    }
    
   
    _tblViewHistory.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    _tblViewSideMenu.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    self.tblViewHistory.rowHeight = UITableViewAutomaticDimension;
    self.tblViewHistory.estimatedRowHeight = 150.0;
    // Do any additional setup after loading the view.
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    if (dictLoginDetail.count==0) {
        
    } else {
        
        _lbl_UserName.text=[dictLoginDetail valueForKey:@"UserName"];
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            [self fetchFromCoreDataDashBoardList];
            
            //            NSString *strTitle = Alert;
            //            NSString *strMsg = ErrorInternetMsg;
            //            [global AlertMethod:strTitle :strMsg];
            //            [DejalBezelActivityView removeView];
        }
        else
        {
            
            [self getDashBoardDetails:[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]]];
            
            // [self performSelector:@selector(callOutboxSendMethod) withObject:nil afterDelay:5.0];
            
        }
    }
    
    NSLog(@"User Details =====  %@",dictLoginDetail);
    
}



-(void)callOutboxSendMethod{
    
    outBoxObj=[[OutboxViewController alloc]init];
    [outBoxObj fetchFromCoreDataMeterReading];
    
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)action_MenuBar:(id)sender {
    
    CGRect frameForHiddenView=CGRectMake(0, 0, self.view.frame.size.width-50, _hiddenView.frame.size.height+200);
    frameForHiddenView.origin.x=200;
    frameForHiddenView.origin.y=20;
    [_hiddenView setFrame:frameForHiddenView];
    [self.view addSubview:_hiddenView];
    
    CGRect frameForCheckView=CGRectMake(0, 20, self.view.frame.size.width-50, [UIScreen mainScreen].bounds.size.height-10);
    frameForCheckView.origin.x=0;
    frameForCheckView.origin.y=20;
    [_viewwSideMenu setFrame:frameForCheckView];
    [self.view addSubview:_viewwSideMenu];
    
    _profileImgView.layer.cornerRadius = _const_Img_W.constant/2;
    _profileImgView.clipsToBounds = YES;
    
    [self performSelector:@selector(profileImageViewSetting) withObject:nil afterDelay:2.0];
}

-(void)profileImageViewSetting{
    
    _profileImgView.layer.cornerRadius = _const_Img_W.constant/2;
    _profileImgView.clipsToBounds = YES;
    
}

- (IBAction)action_AddMeterReading:(id)sender {
}

//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        return arrDashBoardResponse.count;
    }
    else
    {
        return arrNameSlideMenu.count;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==1) {

        return tableView.rowHeight;
//        NSDictionary *dictdata=arrDashBoardResponse[indexPath.row];
//        NSString *strStatuss=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"Status"]];
//        if ([strStatuss isEqualToString:@""]) {
//            return 90;
//        }else{
//        return tableView.rowHeight;
//        }
        
    }else{
            return tableView.rowHeight;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==1)
    {
        
        DashBoardTableViewCell *cell = (DashBoardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DashBoardTableViewCell" forIndexPath:indexPath];
        
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
        
    } else {
        
        SlideMenuTableViewCell *cell = (SlideMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SlideMenuTableViewCell" forIndexPath:indexPath];
        
        // Configure Table View Cell
        [self configureCellSlideMenuTableViewCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}
- (void)configureCell:(DashBoardTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    
    
    if (indexPath.row%2==0) {
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:161/255.0f green:172/255.0f blue:179/255.0f alpha:1.0];
    }else{
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:204/255.0f green:172/255.0f blue:84/255.0f alpha:1.0];
    }
    
    NSDictionary *dictdata=arrDashBoardResponse[indexPath.row];
    
    if([[dictdata valueForKey:@"MeterStatus"] isEqualToString:@"Active"])
    {
        cell.btnAdd.hidden = NO;
    }
    else
    {
        cell.btnAdd.hidden = YES;
    }
    
    NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
    [inputDateFormatter setTimeZone:inputTimeZone];
    [inputDateFormatter setDateFormat:dateFormat];
    
//    NSString *inputString = [dictdata valueForKey:@"SubmittedDate"];
  //  NSDate *date = [inputDateFormatter dateFromString:inputString];
    
    
    NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
    [outputDateFormatter setTimeZone:outputTimeZone];
     NSString *dateFormat1 = @"MM/dd/yyyy";
    [outputDateFormatter setDateFormat:dateFormat1];
    NSString *outputString = [outputDateFormatter stringFromDate:[dictdata valueForKey:@"SubmittedDate"]];
    
    cell.lbl_Reason.text = @"Reason:";
    cell.lbl_PumpedAmount.text = @"Pumped YTD :";
     cell.lblStatusLabell.text = @"Status :";
    cell.lbl_Status.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"Status"]];
    cell.lbl_CommentValue.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"DeclineReason"]];
    cell.lbl_PumpedAmountValue.text=[NSString stringWithFormat:@"%@ Acre-Feet",[dictdata valueForKey:@"PumpedAmount"]];
    cell.lbl_ReasonValue.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"DeclineReason"]];
    cell.lblNickName.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNickName"]];
    cell.lblMeterType.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterType"]];

    
    
    if(cell.lblNickName.text.length == 0){
        cell.lblNickName.text = @"NA";
    }
    if(cell.lblMeterType.text.length == 0){
        cell.lblMeterType.text = @"NA";
    }
    cell.lblMeterSerialNumber.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterSerialNumber"]];
    if(cell.lblMeterSerialNumber.text.length == 0){
        cell.lblMeterSerialNumber.text = @"NA";
    }
    cell.lblLastMeterreading.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"LastMeterReading"]];
    if (cell.lblLastMeterreading.text.length==0)
    {
        cell.lblLastMeterreading.text=@"NA";
    }
    cell.lblDate.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"SubmittedDate"]];
  
    
    cell.lblDate.text = outputString;
   
    if ([cell.lbl_Status.text caseInsensitiveCompare:@"New"] == NSOrderedSame || [cell.lbl_Status.text isEqualToString:@""]) {
        
        NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
        NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
        
        if ([strUserType isEqualToString:@"Customer"])
        {
        if ([cell.lbl_Status.text isEqualToString:@""])
        {
            cell.lbl_Status.text=@"NA";
        }
        else
        {
            cell.lbl_Status.text=@"Pending";
        }
        }
        else
        {
            if ([cell.lbl_Status.text isEqualToString:@""])
            {
                cell.lbl_Status.text=@"NA";
            }
        }
        
        cell.lbl_PumpedAmount.text = @"";
        cell.lbl_PumpedAmountValue.text = @"";
        cell.lbl_Reason.text = @"";
        cell.lbl_ReasonValue.text = @"";
        cell.lbl_Comment.text = @"";
        cell.lbl_CommentValue.text = @"";
        
        
//        [cell.lbl_PumpedAmount setHidden:YES];
//        [cell.lbl_PumpedAmountValue setHidden:YES];
//        [cell.lbl_Reason setHidden:YES];
//        [cell.lbl_ReasonValue setHidden:YES];
//        [cell.const_PumpedAmount_H setConstant:0];
//        [cell.lbl_Comment setHidden:YES];
//        [cell.lbl_CommentValue setHidden:YES];
//
//        cell.const_PumpedAmount_H.constant=0;
//        cell.const_ResetReason_H.constant=0;

    } else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Approved"] == NSOrderedSame) {
//
//        [cell.lbl_PumpedAmount setHidden:NO];
//        [cell.lbl_PumpedAmountValue setHidden:NO];
//        [cell.lbl_Reason setHidden:YES];
//        [cell.lbl_ReasonValue setHidden:YES];
//        [cell.lbl_Comment setHidden:YES];
//        [cell.lbl_CommentValue setHidden:YES];
//        cell.const_ResetReason_H.constant=0;
//        cell.const_PumpedAmount_H.constant=21;
        
        
   
        cell.lbl_Reason.text = @"";
        cell.lbl_ReasonValue.text = @"";
        cell.lbl_Comment.text = @"";
        cell.lbl_CommentValue.text = @"";
        
        
        
    }else if ([cell.lbl_Status.text caseInsensitiveCompare:@"In Progress"] == NSOrderedSame) {
        
//        [cell.lbl_PumpedAmount setHidden:YES];
//        [cell.lbl_PumpedAmountValue setHidden:YES];
//        [cell.lbl_Reason setHidden:YES];
//        [cell.lbl_ReasonValue setHidden:YES];
//        [cell.const_PumpedAmount_H setConstant:0];
//        cell.const_PumpedAmount_H.constant=0;
//        cell.const_ResetReason_H.constant=0;
        
        
        cell.lbl_Reason.text = @"";
        cell.lbl_ReasonValue.text = @"";
        cell.lbl_PumpedAmount.text = @"";
        cell.lbl_PumpedAmountValue.text = @"";
        
        
    }else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Approved with edit"] == NSOrderedSame) {
        
//        [cell.lbl_PumpedAmount setHidden:NO];
//        [cell.lbl_PumpedAmountValue setHidden:NO];
//        [cell.lbl_Reason setHidden:NO];
//        [cell.lbl_ReasonValue setHidden:NO];
//        [cell.const_PumpedAmount_H setConstant:21];
//        cell.const_ResetReason_H.constant=23;
    }
    else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Rejected"] == NSOrderedSame) {
        
//        [cell.lbl_PumpedAmount setHidden:YES];
//        [cell.lbl_PumpedAmountValue setHidden:YES];
//        [cell.lbl_Reason setHidden:NO];
//        [cell.lbl_ReasonValue setHidden:NO];
//        [cell.const_PumpedAmount_H setConstant:0];
//        cell.const_ResetReason_H.constant=23;
        
        cell.lbl_PumpedAmount.text = @"";
        cell.lbl_PumpedAmountValue.text = @"";
    }
    else{
//        [cell.lbl_PumpedAmount setHidden:NO];
//        [cell.lbl_PumpedAmountValue setHidden:NO];
//        [cell.lbl_Reason setHidden:NO];
//        [cell.lbl_ReasonValue setHidden:NO];
//        cell.const_ResetReason_H.constant=23;
//        cell.const_PumpedAmount_H.constant=21;
    }
    if ([cell.lbl_Status.text isEqualToString:@"Reject"]) {
        cell.lbl_Status.text=@"Rejected";
    }

    cell.btnAdd.tag=indexPath.row;
    [cell.btnAdd addTarget:self action:@selector(btnAddClick:) forControlEvents:UIControlEventTouchDown];
    
    cell.btnHistory.layer.borderColor = [[UIColor colorWithRed:161/255.0f green:172/255.0f blue:179/255.0f alpha:1.0] CGColor];
    cell.btnHistory.layer.cornerRadius = 4.0;
    cell.btnHistory.tag=indexPath.row;
    cell.btnHistory.layer.borderWidth = 1.0;
    [cell.btnHistory addTarget:self action:@selector(btnHistoryClick:) forControlEvents:UIControlEventTouchDown];
 
    cell.btnNewReading.layer.borderColor = [[UIColor colorWithRed:161/255.0f green:172/255.0f blue:179/255.0f alpha:1.0] CGColor];
    cell.btnNewReading.layer.cornerRadius = 4.0;
    cell.btnNewReading.tag=indexPath.row;
    cell.btnNewReading.layer.borderWidth = 1.0;
    
    [cell.btnNewReading addTarget:self action:@selector(btnAddReadingClick:) forControlEvents:UIControlEventTouchDown];
    

}

- (void)configureCellSlideMenuTableViewCell:(SlideMenuTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    cell.imgView.image=[UIImage imageNamed:arrOfImgSlideMenu[indexPath.row]];
    cell.lblName.text=arrNameSlideMenu[indexPath.row];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView.tag==1) {
        
        NSUserDefaults *sefss=[NSUserDefaults standardUserDefaults];
        [sefss setBool:NO forKey:@"fromDashBoard"];
        [sefss synchronize];
        NSDictionary *dictData=arrDashBoardResponse[indexPath.row];
//
//        if([[dictData valueForKey:@"MeterStatus"] isEqualToString:@"Active"])
//        {
        
            BOOL isAlreadySent=[self fetchFromCoreDataDateReadingToCheck:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterID"]]];
            isAlreadySent=NO;
            if (isAlreadySent) {
                
                NSString *strTitle = @"Alert";
                NSString *strMsg = @"Reading can be submitted only once in 24 hrs.";
                [global AlertMethod:strTitle :strMsg];
            }else{
                
                //Updated by Navin on 30 OCT 2019
                //recommmend by Sameep
                //check MeterType
                
                
                if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterStatus"]]  isEqual: @"Inactive"] || [[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterStatus"]]  isEqual: @"inactive"]) {
                    NSString *strTitle = @"Alert";
                    NSString *strMsg = @"Meter is Inactive";
                    [global AlertMethod:strTitle :strMsg];
                }else{
                    if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterType"]]  isEqual: @"Permitted"] || [[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterType"]]  isEqual: @"LPW"]  ){
                        
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                 bundle: nil];
                        Permitted_LPW_VC
                        *obj = [mainStoryboard instantiateViewControllerWithIdentifier:@"Permitted_LPW_VC"];
                        NSDictionary *dictdata=arrDashBoardResponse[indexPath.row];
                        obj.aryCollection = [arrDashBoardResponse mutableCopy];
                        obj.dictMeterData = [dictdata mutableCopy];
                        obj.currentIndex = indexPath.row;
                        [self.navigationController pushViewController:obj animated:true];
                        
                    }
                
                }
                
                
         
                
//                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                         bundle: nil];
//                ReportMeterReadingViewController
//                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
//                NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];
//                NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];
//                NSString *strName;
//                if (strMeterNumber.length==0) {
//                    if (strMeterNamee.length==0) {
//                        strName=@"";
//                    } else {
//                        strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];
//                    }
//                }else{
//                    if (strMeterNamee.length==0) {
//                        strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];
//                    } else {
//                        strName=[NSString stringWithFormat:@"%@-%@",[dictData valueForKey:@"MeterNickName"],[dictData valueForKey:@"MeterSerialNumber"]];
//                    }
//                }
//                objByProductVC.strMeteridDefault=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterID"]];
//                objByProductVC.strMeterNickNamenSerialNoDefault=strName;
//                objByProductVC.dictDataManageMeterList=dictData;
//                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
       // }
    }
    else
    {
        NSInteger i;
        i=indexPath.row;
        if (([strAccuracyTest boolValue] == YES)) {
            switch (i)
            {
                case 0:
                {
                    //Home
                    [_viewwSideMenu removeFromSuperview];
                    [_hiddenView removeFromSuperview];
                    break;
                }
                case 1:
                {
                    //Ad Meter
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setBool:NO forKey:@"FromManageMeter"];
                    [defs synchronize];
                    
                    [self goToAddMeter];
                    break;
                }
    //            case 2:
    //            {
    //                //Report Meter Reading
    //                NSUserDefaults *sefss=[NSUserDefaults standardUserDefaults];
    //                [sefss setBool:YES forKey:@"fromDashBoard"];
    //                [sefss synchronize];
    //
    //                [self goToReportMeterReading];
    //                break;
    //            }
                case 2:
                {
                    [self performSegueWithIdentifier:@"ReportAnAccuracyTestViewController" sender:nil];
                    [_viewwSideMenu removeFromSuperview];
                    [_hiddenView removeFromSuperview];
                    break;
                }
    //             case 4:
    //            {
    //                [self performSegueWithIdentifier:@"RegisterANewMeterViewController" sender:nil];
    //                [_viewwSideMenu removeFromSuperview];
    //                [_hiddenView removeFromSuperview];
    //                break;
    //            }
                case 3:
                {
                    //replace a meter
                    [self performSegueWithIdentifier:@"ReplaceAMeterViewController" sender:nil];
                    [_viewwSideMenu removeFromSuperview];
                    [_hiddenView removeFromSuperview];
                    break;
                }
                case 4:
                {
                    [self goToManageMeter];
                    break;
                }
    //            case 6:
    //            {
    //                // manage accuracy test
    //                [self performSegueWithIdentifier:@"ManageAccuracyTestViewController" sender:nil];
    //                [_viewwSideMenu removeFromSuperview];
    //                [_hiddenView removeFromSuperview];
    //                break;
    //            }
                case 5:
                {
                    [self goToHistoryView];
                    break;
                }
                case 6:
                {
                    [self goToOutbox];
                    break;
                }
                case 7:
                {
                    [self goToNotificationHub];
                    break;
                }
                case 8:
                {
                    //Help
                    [self goToHelp];
                    break;
                }
                case 9:
                {
                    [self goTermsConditions];
                    break;
                }
                case 10:
                {
                    //logout
                    [self logOut];
                    break;
                }
                default:
                    break;
            }
        }else{
            switch (i)
            {
                case 0:
                {
                    //Home
                    [_viewwSideMenu removeFromSuperview];
                    [_hiddenView removeFromSuperview];
                    break;
                }
                case 1:
                {
                    //Ad Meter
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setBool:NO forKey:@"FromManageMeter"];
                    [defs synchronize];
                    
                    [self goToAddMeter];
                    break;
                }
    
                case 2:
                {
                    //replace a meter
                    [self performSegueWithIdentifier:@"ReplaceAMeterViewController" sender:nil];
                    [_viewwSideMenu removeFromSuperview];
                    [_hiddenView removeFromSuperview];
                    break;
                }
                case 3:
                {
                    [self goToManageMeter];
                    break;
                }
                case 4:
                {
                    [self goToHistoryView];
                    break;
                }
                case 5:
                {
                    [self goToOutbox];
                    break;
                }
                case 6:
                {
                    [self goToNotificationHub];
                    break;
                }
                case 7:
                {
                    //Help
                    [self goToHelp];
                    break;
                }
                case 8:
                {
                    [self goTermsConditions];
                    break;
                }
                case 9:
                {
                    //logout
                    [self logOut];
                    break;
                }
                default:
                    break;
            }
        }
    }
    
}
-(void)btnHistoryClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    NSDictionary *dictData=arrDashBoardResponse[btn.tag];

    if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterType"]]  isEqual: @"Permitted"] || [[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterType"]]  isEqual: @"LPW"]  ){
        MeterHistory
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MeterHistory"];
        objByProductVC.dictData = [dictData mutableCopy];
        [self.navigationController pushViewController:objByProductVC animated:true];
    }
    
    
   
   
}
-(void)btnAddReadingClick:(id)sender{
    NSUserDefaults *sefss=[NSUserDefaults standardUserDefaults];
    [sefss setBool:NO forKey:@"fromDashBoard"];
    [sefss synchronize];
    long tag = [sender tag];
    NSDictionary *dictData= arrDashBoardResponse[tag];
//
//        if([[dictData valueForKey:@"MeterStatus"] isEqualToString:@"Active"])
//        {
    
        BOOL isAlreadySent=[self fetchFromCoreDataDateReadingToCheck:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterID"]]];
        isAlreadySent=NO;
        if (isAlreadySent) {
            
            NSString *strTitle = @"Alert";
            NSString *strMsg = @"Reading can be submitted only once in 24 hrs.";
            [global AlertMethod:strTitle :strMsg];
        }else{
            
            //Updated by Navin on 30 OCT 2019
            //recommmend by Sameep
            //check MeterType
            
            
            if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterStatus"]]  isEqual: @"Inactive"] || [[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterStatus"]]  isEqual: @"inactive"]) {
                NSString *strTitle = @"Alert";
                NSString *strMsg = @"Meter is Inactive";
                [global AlertMethod:strTitle :strMsg];
            }else{
                if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterType"]]  isEqual: @"Permitted"] || [[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterType"]]  isEqual: @"LPW"]  ){
                    
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                             bundle: nil];
                    Permitted_LPW_VC
                    *obj = [mainStoryboard instantiateViewControllerWithIdentifier:@"Permitted_LPW_VC"];
                    NSDictionary *dictdata=arrDashBoardResponse[tag];
                    obj.aryCollection = [arrDashBoardResponse mutableCopy];
                    obj.dictMeterData = [dictdata mutableCopy];
                    obj.currentIndex = tag;
                    [self.navigationController pushViewController:obj animated:true];
                    
                }
            
            }
            
            
     
            
//                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                         bundle: nil];
//                ReportMeterReadingViewController
//                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
//                NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];
//                NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];
//                NSString *strName;
//                if (strMeterNumber.length==0) {
//                    if (strMeterNamee.length==0) {
//                        strName=@"";
//                    } else {
//                        strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];
//                    }
//                }else{
//                    if (strMeterNamee.length==0) {
//                        strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];
//                    } else {
//                        strName=[NSString stringWithFormat:@"%@-%@",[dictData valueForKey:@"MeterNickName"],[dictData valueForKey:@"MeterSerialNumber"]];
//                    }
//                }
//                objByProductVC.strMeteridDefault=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterID"]];
//                objByProductVC.strMeterNickNamenSerialNoDefault=strName;
//                objByProductVC.dictDataManageMeterList=dictData;
//                [self.navigationController pushViewController:objByProductVC animated:NO];
        }
   // }

}
//============================================================================
#pragma mark- -------------------------METHODS--------------------------------
//============================================================================
-(void)btnAddClick:(id)sender
{
   
    
    UIButton *btn = (UIButton *)sender;

    NSUserDefaults *sefss=[NSUserDefaults standardUserDefaults];
    [sefss setBool:NO forKey:@"fromDashBoard"];
    [sefss synchronize];
    NSDictionary *dictData=arrDashBoardResponse[btn.tag];

    BOOL isAlreadySent=[self fetchFromCoreDataDateReadingToCheck:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterID"]]];

    isAlreadySent=NO;

    if (isAlreadySent) {

        NSString *strTitle = @"Alert";
        NSString *strMsg = @"Reading can be submitted only once in 24 hrs.";
        [global AlertMethod:strTitle :strMsg];
    }else{

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ReportMeterReadingViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];

    NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];
    NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];

    NSString *strName;

    if (strMeterNumber.length==0) {
        if (strMeterNamee.length==0) {

            strName=@"";

        } else {

            if (strMeterNamee.length==0) {

                strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];

            } else {

                strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];

            }
        }
    }else{
        strName=[NSString stringWithFormat:@"%@-%@",[dictData valueForKey:@"MeterNickName"],[dictData valueForKey:@"MeterSerialNumber"]];

    }
    objByProductVC.strMeteridDefault=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterID"]];
    objByProductVC.strMeterNickNamenSerialNoDefault=strName;
    objByProductVC.dictDataManageMeterList=dictData;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
-(void)goToReportMeterReading{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ReportMeterReadingViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToAddMeter{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    AddMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMeterViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToManageMeter{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ManageMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ManageMeterViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToDisclaimer{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    DisclaimerViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DisclaimerViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToHistoryView{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)goToHelp{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HelpViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goTermsConditions{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    TermsConditionViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TermsConditionViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)goToOutbox{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    OutboxViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"OutboxViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToNotificationHub{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    NotificationHubViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"NotificationHubViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)logOut{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm!"
                               message:@"Do you want to logout ?"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Logout" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                           {
    
                               NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                               [defs setBool:NO forKey:@"RememberMe"];
                               [defs synchronize];
                               
                               UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                        bundle: nil];
                               LoginViewController
                               *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                               [self.navigationController pushViewController:objByProductVC animated:NO];

                           }];
            [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];

    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_viewwSideMenu removeFromSuperview];
    [_hiddenView removeFromSuperview];
    
}
-(void)getDashBoardDetails :(NSString*)customerId{
    UIAlertController *loaderAlert = [UIAlertController new];
    if(arrDashBoardResponse.count == 0){
        loaderAlert = [global loader_ShowObjectiveC:self :@"Fetching Details..."];
    }
    
   // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Dashboard Details..."];
    
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlGetDashBoardDetails,customerId];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"DashBoardDetails" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
           //  [loaderAlert dismissViewControllerAnimated:NO completion:^{  }];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 [loaderAlert dismissViewControllerAnimated:NO completion:^{  }];
                 if (success)
                 {
                     [self afterServerResponseDashBoard:response];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
             
         }];
    });
}

-(void)afterServerResponseDashBoard :(NSDictionary*)dictResponse{
    
    
    arrDashBoardResponse=(NSArray*)dictResponse;
    
    if (arrDashBoardResponse.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailable
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];

    } else {
        
        NSMutableArray *tempArray=[NSMutableArray new];
        for(NSInteger i=0;i<arrDashBoardResponse.count ;i++)
        {
            NSMutableDictionary *dic =[[NSMutableDictionary alloc] initWithDictionary:[arrDashBoardResponse objectAtIndex:i]];
           
            NSString *dateFormat = @"MM/dd/yyyy";
            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
            [inputDateFormatter setTimeZone:inputTimeZone];
            [inputDateFormatter setDateFormat:dateFormat];
            
            NSString *inputString = [dic valueForKey:@"SubmittedDate"];
            NSDate *date = [inputDateFormatter dateFromString:inputString];
            
            
            NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
            [outputDateFormatter setTimeZone:outputTimeZone];
            [outputDateFormatter setDateFormat:dateFormat];
            NSString *outputString = [outputDateFormatter stringFromDate:date];
            
             NSDate *dateConverted = [inputDateFormatter dateFromString:inputString];
          
            
            [dic setValue:dateConverted forKey:@"SubmittedDate"];
            [tempArray addObject:dic];
        }
        
        
        NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:@"SubmittedDate" ascending:NO];
        NSArray *descriptors=[NSArray arrayWithObject: descriptor];
        NSArray *sortedArray=[tempArray sortedArrayUsingDescriptors:descriptors];
        NSLog(@"%@",sortedArray);
        
      //  [self saveToCoreDataDashBoardData:sortedArray];
     
       [self saveToCoreDataDashBoardData:arrDashBoardResponse];
        
       // [_tblViewHistory reloadData];
        
    }
}
-(void)saveToCoreDataDashBoardData :(NSArray*)arrOfDashBoardList{
    
    //==================================================================================
    //==================================================================================
    
    [self deleteDashBoardListFromDB];
    
    //==================================================================================
    //==================================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDashBoardList = [appDelegate managedObjectContext];
    entityDashBoardList=[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList];
    
    //==================================================================================
    //==  ================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    DashBoardListt *objDashBoardList = [[DashBoardListt alloc]initWithEntity:entityDashBoardList insertIntoManagedObjectContext:contextDashBoardList];
    objDashBoardList.userName=[dictLoginDetail valueForKey:@"UserName"];
    objDashBoardList.userType=@"";
    objDashBoardList.arrOfDashBoardList=arrOfDashBoardList;
    NSError *error1;
    [contextDashBoardList save:&error1];
    
    [self fetchFromCoreDataDashBoardList];
    //==================================================================================
    //==================================================================================
}

-(void)deleteDashBoardListFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDashBoardList = [appDelegate managedObjectContext];
    entityDashBoardList=[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [contextDashBoardList executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [contextDashBoardList deleteObject:data];
        }
        NSError *saveError = nil;
        [contextDashBoardList save:&saveError];
    }
    
}

-(void)fetchFromCoreDataDashBoardList{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDashBoardList = [appDelegate managedObjectContext];
    entityDashBoardList=[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList];
    requestDashBoardList = [[NSFetchRequest alloc] init];
    [requestDashBoardList setEntity:entityDashBoardList];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestDashBoardList setPredicate:predicate];
    
    sortDescriptorDashBoardList = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsDashBoardList = [NSArray arrayWithObject:sortDescriptorDashBoardList];
    
    [requestDashBoardList setSortDescriptors:sortDescriptorsDashBoardList];
    
    self.fetchedResultsControllerDashBoardList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestDashBoardList managedObjectContext:contextDashBoardList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerDashBoardList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerDashBoardList performFetch:&error];
    arrAllObjDashBoardList = [self.fetchedResultsControllerDashBoardList fetchedObjects];
    if ([arrAllObjDashBoardList count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailable;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        matchesDashBoardList = arrAllObjDashBoardList[0];
        arrDashBoardResponse=[matchesDashBoardList valueForKey:@"arrOfDashBoardList"];
        [_tblViewHistory reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}

-(BOOL)fetchFromCoreDataDateReadingToCheck :(NSString*)meterIdd {
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDateReading = [appDelegate managedObjectContext];
    entityDateReading=[NSEntityDescription entityForName:@"DateMeterReading" inManagedObjectContext:contextDateReading];
    requestDateReading = [[NSFetchRequest alloc] init];
    [requestDateReading setEntity:entityDateReading];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@ AND meterId = %@",strUsername,meterIdd];
    [requestDateReading setPredicate:predicate];
    
    sortDescriptorDateReading = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsDateReading = [NSArray arrayWithObject:sortDescriptorDateReading];
    
    [requestDateReading setSortDescriptors:sortDescriptorsDateReading];
    
    self.fetchedResultsControllerDateReading = [[NSFetchedResultsController alloc] initWithFetchRequest:requestDateReading managedObjectContext:contextDateReading sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerDateReading setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerDateReading performFetch:&error];
    arrAllObjDateReading = [self.fetchedResultsControllerDateReading fetchedObjects];
    if ([arrAllObjDateReading count] == 0)
    {
        return NO;
    }
    else
    {
        
        matchesDateReading = arrAllObjDateReading[0];
        //dateReadingBeingSent
        
        NSString *strReadingDate=[matchesDateReading valueForKey:@"dateReadingBeingSent"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSDate* ReadingDate = [dateFormatter dateFromString:strReadingDate];
        NSString *strCurrentDate = [dateFormatter stringFromDate:[NSDate date]];
        NSDate* CurrentDate = [dateFormatter dateFromString:strCurrentDate];
        
        NSComparisonResult result = [ReadingDate compare:CurrentDate];
        
        if(result == NSOrderedDescending)
        {
            NSLog(@"ReadingDate is Greater");//
            return NO;
        }
        else if(result == NSOrderedAscending)
        {
            NSLog(@"CurrentDate is greater");//
            return NO;
        }
        else
        {
            NSLog(@"Equal date");//
            return YES;
        }
    }
}

@end
