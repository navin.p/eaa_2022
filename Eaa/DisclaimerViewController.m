//
//  DisclaimerViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 05/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "DisclaimerViewController.h"
#import "DashBoardViewController.h"
#import "TechDashBoardViewController.h"

@interface DisclaimerViewController ()

@end

@implementation DisclaimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_Back:(id)sender {
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {

    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[DashBoardViewController class]])
        {
            [self.navigationController popToViewController:controller animated:NO];
            //break;
        }
    }
    }else{
        
        for (UIViewController *controller in self.navigationController.viewControllers)
        {
            if ([controller isKindOfClass:[TechDashBoardViewController class]])
            {
                [self.navigationController popToViewController:controller animated:NO];
                //break;
            }
        }
    }
}
@end
