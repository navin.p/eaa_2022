//
//  ForgotPassViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassViewController : UIViewController
- (IBAction)action_Back:(id)sender;
- (IBAction)action_SendMail:(id)sender;
- (IBAction)hideKeyBoard:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtEmailId;

@end
