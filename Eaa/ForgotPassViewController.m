//
//  ForgotPassViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ForgotPassViewController.h"
#import "LoginViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"

@interface ForgotPassViewController ()
{
    Global *global;
}

@end

@implementation ForgotPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_Back:(id)sender {
    
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[LoginViewController class]])
        {
            [self.navigationController popToViewController:controller animated:NO];
            //break;
        }
    }

}

- (IBAction)action_SendMail:(id)sender {
    
    [self sendEmailMethod];
    
}
- (IBAction)hideKeyBoard:(id)sender{
    [self resignFirstResponder];
}
//============================================================================
//============================================================================
#pragma mark--- Methods
//============================================================================
//============================================================================
-(NSString *)validationCheck
{
    NSString *errorMessage;
    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
  
    if (_txtEmailId.text.length==0)
    {
        errorMessage = @"Please enter email address";
    }
    else if (![emailPredicate evaluateWithObject:_txtEmailId.text])
    {
        errorMessage = @"Please enter valid email id only";
    }
    return errorMessage;
}
-(void)sendEmailMethod{
    
    NSString *errorMessage=[self validationCheck];
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
        [DejalBezelActivityView removeView];
    } else {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending Email..."];
        
        NSString *strUrl=[NSString stringWithFormat:@"%@%@%@%@%@",MainUrl,UrlForgotPassword,[_txtEmailId.text stringByReplacingOccurrencesOfString:@" " withString:@""],UrlForgotPasswordType,@""];
        //============================================================================
        //============================================================================
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"ForgotPassword" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         [self afterServerResponseForgotPass:response];
                         
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });

        
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_txtEmailId resignFirstResponder];
    
}
-(void)afterServerResponseForgotPass :(NSDictionary*)dictResponse{
    
    if (dictResponse.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:Sorry
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        BOOL isSuccess=[[dictResponse valueForKey:@"IsSuccess"] boolValue];
        
        if (isSuccess) {
            
            [global AlertMethod:Info :SuccessMailSend];
            _txtEmailId.text=@"";
            
        } else {
            
            NSArray *arrOfError=[dictResponse valueForKey:@"Error"];
            if ([arrOfError isKindOfClass:[NSArray class]]) {
                
                if (arrOfError.count==0) {
                    
                    NSString *strTitle = Alert;
                    NSString *strMsg = Sorry;
                    [global AlertMethod:strTitle :strMsg];
                    
                } else {
                    
                    NSString *joinedError = [arrOfError componentsJoinedByString:@","];
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert!"
                                               message:joinedError
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              
                                          }];
                    [alert addAction:yes];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }
                
            } else {
                
                NSString *strTitle = Alert;
                NSString *strMsg = Sorry;
                [global AlertMethod:strTitle :strMsg];
                
            }
            
        }
    }
}

@end
