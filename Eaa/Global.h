//
//  Global.h
//  CaptureTheirFlag
//
//  Created by Rakesh Jain on 30/09/15.
//  Copyright © 2015 Sankalp. All rights reserved.
//  Y wali main hai

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "UITextView+Placeholder.h"


@interface Global : NSObject<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityAddLead;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityOutBox,*entitySalesModifyDate;
    
    // Modify Date Change
    NSEntityDescription *entityModifyDate;
    NSFetchRequest *requestModifyDate;
    NSSortDescriptor *sortDescriptorModifyDate;
    NSArray *sortDescriptorsModifyDate;
    NSManagedObject *matchesModifyDate;
    NSArray *arrAllObjModifyDate;
}
-(void)AlertMethod :(NSString *)strMsgTitle :(NSString *)strMsg;

//============================================================================
//============================================================================

typedef void (^WebServiceCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForUrl:(NSString *)url :(NSString *)Type withCallback:(WebServiceCompletionBlock)callback;

//============================================================================
//============================================================================

typedef void (^WebServicePostCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForUrlPostMethod:(NSString *)url :(NSString *)type :(NSData *)requestDataa withCallback:(WebServicePostCompletionBlock)callback;


//============================================================================
//============================================================================

typedef void (^WebServiceCompletionBlockGetAsynchronus)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForUrlGetAynchronusMethod:(NSString *)url :(NSString *)type withCallback:(WebServiceCompletionBlockGetAsynchronus)callback;


//============================================================================
//============================================================================

typedef void (^CoreDataAddleadsSaveBlock)(BOOL success, NSError *error);
- (void)saveToCoreDataLeadInfo:(NSString *)url :(NSString *)type :(NSDictionary *)dictData withCallback:(CoreDataAddleadsSaveBlock)callback;

//============================================================================
//============================================================================

typedef void (^CoreDataOutBoxSaveBlock)(BOOL success, NSError *error);
- (void)saveToCoreDataOutBox:(NSString *)url :(NSString *)type :(NSDictionary *)dictData withCallback:(CoreDataOutBoxSaveBlock)callback;

//============================================================================
//============================================================================

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerAddLead;
-(void)deleteDatabaseBeforeAdding;

//============================================================================
//============================================================================

typedef void (^SendLeadToServerPostCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForSendLeadToServer:(NSString *)url :(NSData *)requestDataa :(NSDictionary *)dictData :(NSString*)indexToDletee :(NSString*)Type withCallback:(SendLeadToServerPostCompletionBlock)callback;

//============================================================================
//============================================================================

typedef void (^SalesDynamicJsonPostCompletionBlock)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForSalesDynamicJson:(NSString *)url :(NSData *)requestDataa withCallback:(SalesDynamicJsonPostCompletionBlock)callback;


//============================================================================
//============================================================================
#pragma mark ---------------- Date Change Method--------------------------
//============================================================================
//============================================================================

-(NSString *)ChangeDateToLocalDate :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateOther :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateOtherNew :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateFull :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateOtherSep :(NSString*)strDateToConvert;
-(NSString *)modifyDate;
-(NSString *)ChangeDateToLocalDateOtherTimeAlso :(NSString*)strDateToConvert;
-(NSString *)ChangeDateToLocalDateOtherServiceAppointments :(NSString*)strDateToConvert;
-(BOOL)isCameraPermissionAvailable;
-(BOOL)isAudioPermissionAvailable;
-(BOOL)isGalleryPermission;

-(NSString *)modifyDateService;

typedef void (^SendLeadToServerPostCompletionBlockOffline)(BOOL success, NSDictionary *response, NSError *error);
- (void)getServerResponseForSendLeadToServerOffline:(NSString *)url :(NSData *)requestDataa :(NSDictionary *)dictData :(NSString*)indexToDletee :(NSDictionary *)dictDataAllValues withCallback:(SendLeadToServerPostCompletionBlockOffline)callback;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerModifyDate;
//Nilind 5 Oct
-(void)updateSalesModifydate:(NSString *)strLeadId;

+(void)addPaddingView:(UITextField*)textField;

// Akshay
+(void)postData:(NSString *)urlStr parameters: (NSDictionary *)parameters success: (void(^)(NSDictionary * response))success failure: (void(^)(NSError * error))failure ;
-(NSString *)ChangeDateToLocalDateOtherNilind :(NSString*)strDateToConvert;
-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
;

-(UIAlertController *)loader_ShowObjectiveC:(UIViewController*)controller :(NSString *)strMessage;
@end
