
//
//  Global.m
//  CaptureTheirFlag
//
//  Created by Rakesh Jain on 30/09/15.
//  Copyright © 2015 Sankalp. All rights reserved.
//
#import "Header.h"
#import "Global.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@implementation Global
{
    NSMutableData *responseData;
    NSDictionary *ResponseDict;
}

//============================================================================
//============================================================================
#pragma mark--- Alert View Method
//============================================================================
//============================================================================

-(void)AlertMethod :(NSString *)strMsgTitle :(NSString *)strMsg
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:strMsgTitle message:strMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

//============================================================================
//============================================================================
#pragma mark--- Get Method Call Back
//============================================================================
//============================================================================

void(^getServerResponseForUrlCallback)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForUrl:(NSString *)url :(NSString *)Type withCallback:(WebServiceCompletionBlock)callback
{
    getServerResponseForUrlCallback = callback;
    [self onBackendResponse:nil withSuccess:YES error:nil withUrl:url withType:Type];
}

- (void)onBackendResponse:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withType:(NSString*)strType
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    if ([strType isEqualToString:@"TotalLead"] || [strType isEqualToString:@"LeadCount"] || [strType isEqualToString:@"History"]) {
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strHrmsCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.HrmsCompanyId"]];
    NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
    NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeId"]];
    NSString *strCreatedBy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"CreatedBy"]];
    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeName"]];
    NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
    NSString *strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
    NSString *strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
        
    [request addValue:strHrmsCompanyId forHTTPHeaderField:@"HrmsCompanyId"];
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"IpAddress"];
    [request addValue:strEmployeeNumber forHTTPHeaderField:@"EmployeeNumber"];
    [request addValue:strEmployeeId forHTTPHeaderField:@"EmployeeId"];
    [request addValue:strCreatedBy forHTTPHeaderField:@"CreatedBy"];
    [request addValue:strEmployeeName forHTTPHeaderField:@"EmployeeName"];
    [request addValue:strCoreCompanyId forHTTPHeaderField:@"CoreCompanyId"];
    [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
    [request addValue:strCompanyKey forHTTPHeaderField:@"CompanyKey"];
        
    }
    
    @try {
        //        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
        //                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
        //         {
        //             NSData* jsonData = [NSData dataWithData:data];
        //             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        //         }];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        if ([strType isEqualToString:@"getActivity"] || [strType isEqualToString:@"getTask"] || [strType isEqualToString:@"salesInspection"] || [strType isEqualToString:@"serviceInspection"] || [strType isEqualToString:@"getMasterProductChemicalsServiceAutomation"] || [strType isEqualToString:@"getDocumentService"]) {
            if (ResponseDict.count==0) {
                
            } else {
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                ResponseDict =dictData;
            }
        }else{
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
        }
    }
    @catch (NSException *exception) {
        
        [self AlertMethod:Alert :Sorry];
        
    }
    @finally {
        
    }
    getServerResponseForUrlCallback(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Post Method Call Back
//============================================================================
//============================================================================

void(^getServerResponseForUrlCallbackPostMethod)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForUrlPostMethod:(NSString *)url :(NSString *)type :(NSData *)requestDataa withCallback:(WebServicePostCompletionBlock)callback
{
    getServerResponseForUrlCallbackPostMethod = callback;
    [self onBackendResponsePostMethod:nil withSuccess:YES error:nil withUrl:url withType:type withData:requestDataa];
}

- (void)onBackendResponsePostMethod:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withType:(NSString *)type withData:(NSData*)requestData
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        //        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
        //                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
        //         {
        //             NSData* jsonData = [NSData dataWithData:data];
        //             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        //         }];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        NSString *strException;
//        @try {
//            NSArray *arr=[ResponseDict valueForKey:@"Errors"];
//            if (!(arr.count==0)) {
//                strException=arr[0];
//            }
//        } @catch (NSException *exception) {
//            
//        } @finally {
//            
//        }
        
        if (strException.length==0)
        {
            if ([type isEqualToString:@"ManageMeterAPI"]) {
                
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                ResponseDict=dictData;

            }else if ([type containsString:@"MeterImgg"]){
                
                ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
                
                [self deleteFromOutBoxCoreData:type];
                
                //AddUpdateMeterOutBox
                
            }
            else if ([type isEqualToString:@"AddUpdateMeter"]){
                
                NSString *strIsuccess=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"IsSuccess"]];
                if ([strIsuccess isEqualToString:@"0"]) {
                    
                    
                }
                else
                {
                    ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];                    
                }
                
            }
            else if ([type isEqualToString:@"SignUpAPI"]){
                
                NSString *strIsuccess=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"IsSuccess"]];
                if ([strIsuccess isEqualToString:@"0"]) {
                    
                    
                } else {
                
                    ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
                    
                }
            }
            else if([type isEqualToString:@"ReplaceMeter"])
            {
               // ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            }
            else {
                
               // ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];

            }

        } else {
            if ([strException isEqualToString:@"Incorrect password."])
            {
                
            }else if ([strException isEqualToString:@"Passwords must have at least one non letter or digit character. Passwords must have at least one digit ('0'-'9'). Passwords must have at least one uppercase ('A'-'Z')."]){
                
            }
            else{
                
                ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];

            }
        }
    }
    @catch (NSException *exception) {
       // [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackPostMethod(success, ResponseDict, error);
}



//============================================================================
//============================================================================
#pragma mark--- Asynchronus Get Method Call Back
//============================================================================
//============================================================================

void(^getServerResponseForUrlCallbackAsynchronus)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForUrlGetAynchronusMethod:(NSString *)url :(NSString *)type withCallback:(WebServiceCompletionBlockGetAsynchronus)callback
{
    getServerResponseForUrlCallbackAsynchronus = callback;
    if ([type isEqualToString:@"getEmployeeList"])
    {
        [self onBackendResponseAsynchronusgetEmployeeList:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getLeadDetailMaster"])
    {
        [self onBackendResponseAsynchronusgetLeadDetailMaster:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getLeadCount"])
    {
        [self onBackendResponseAsynchronusgetgetLeadCount:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getMasterServiceAutomation"])
    {
        [self onBackendResponseAsynchronusgetMasterServiceAutomation:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getMasterSalesAutomation"])
    {
        [self onBackendResponseAsynchronusgetMasterSalesAutomation:nil withSuccess:YES error:nil withUrl:url :type];
    }

    else if ([type isEqualToString:@"getMasterProductChemicalsServiceAutomation"])
    {
        [self onBackendResponseAsynchronusgetProductChemicalsServiceAutomation:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"ForgotPassword"])
    {
        [self onBackendResponseAsynchronusgSendEmailServiceAutomation:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"DashBoardDetails"])
    {
        [self onBackendResponseAsynchronusgGetCustomMessage:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"DeleteMeter"])
    {
        [self onBackendResponseDeleteMeter:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"MobileCofigData"])
    {
        [self onBackendResponseAsynchronusgGetCustomMessage:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else{
        [self onBackendResponseAsynchronus:nil withSuccess:YES error:nil withUrl:url :type];
    }
}

- (void)onBackendResponseAsynchronus:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        
        
        //NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

           [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                  completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
            {
                NSData* jsonData = [NSData dataWithData:data];
                ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                ResponseDict =dictData;
                /*NSMutableDictionary *dictResponse;
                dictResponse=[[NSMutableDictionary alloc]init];
                [dictResponse setObject:ResponseDict forKey:@"response"];
                ResponseDict=dictResponse;
                NSLog(@"Response on Asynchronus = = = =  = %@",ResponseDict);
                ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];*/
            }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- getLeadDetailMaster
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetLeadDetailMaster:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }

             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setValue:ResponseDict forKey:@"LeadDetailMaster"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
             
             NSLog(@"Response on getLeadDeatilMaster = = = =  = %@",ResponseDict);
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- getEmployeeList
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetEmployeeList:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if (ResponseDict.count==0) {
                 
             } else {
                 
             NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
             [dictTempResponse setObject:ResponseDict forKey:@"response"];
             NSDictionary *dict=[[NSDictionary alloc]init];
             dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
             NSMutableDictionary *dictData=[dict valueForKey:@"response"];
             
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                 
                 if (strException.length==0) {
                     
                     NSUserDefaults *defsResponse=[NSUserDefaults standardUserDefaults];
                     [defsResponse setObject:dictData forKey:@"EmployeeList"];
                     [defsResponse synchronize];
                     
                 } else {
                     ResponseDict=nil;
                 }
    
             NSLog(@"Response on getEmployeeList = = = =  = %@",dictData);
                 
             }
         }];
        
        //        NSError *error = nil;
        //        NSURLResponse * response = nil;
        //        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        //        NSData* jsonData = [NSData dataWithData:data];
        //        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- GetLeadCount
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetgetLeadCount:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             NSArray *arrOfLeadStatusMasters =[ResponseDict valueForKey:@"LeadStatusMasters"];
//             arrOfLeadStatusMasters =[NSArray arrayWithObjects:@"test._-&/",
//                                      @"Open",
//                                      @"New",
//                                      @"Sold",
//                                      @"Deferred",
//                                      @"Complete",
//                                      nil];
             
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }

             if (strException.length==0) {
                 
                NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"OrderBy" ascending:YES];
                 NSArray *sortDescriptorsNew = [NSArray arrayWithObject:brandDescriptor];
                 arrOfLeadStatusMasters = [arrOfLeadStatusMasters sortedArrayUsingDescriptors:sortDescriptorsNew];
                 
                 NSMutableArray *arrDelete=[[NSMutableArray alloc]init];
                 
                 for (int k=0; k<arrOfLeadStatusMasters.count; k++) {
                     
                     NSDictionary *dictData=arrOfLeadStatusMasters[k];
                     
                     BOOL isActive=[[dictData valueForKey:@"IsActive"] boolValue];
                     
                     if (!isActive) {
                         
                         [arrDelete addObject:dictData];
                         
                     }
                     
                 }
                 NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                 
                 [arrTemp addObjectsFromArray:arrOfLeadStatusMasters];
                 
                 if (!(arrDelete.count==0)) {
                     [arrTemp removeObjectsInArray:arrDelete];
                 }
                 
                 arrOfLeadStatusMasters=arrTemp;
                 
                 NSUserDefaults *defLeadStatusMasters =[NSUserDefaults standardUserDefaults];
                 [defLeadStatusMasters setObject:arrOfLeadStatusMasters forKey:@"LeadStatusMasters"];
                 [defLeadStatusMasters setObject:ResponseDict forKey:@"TotalLeadCountResponse"];
                 [defLeadStatusMasters synchronize];
                 
             } else {
                 ResponseDict=nil;
             }

             
             NSLog(@"Response on lead count = = = =  = %@",ResponseDict);
         }];
        
        //        NSError *error = nil;
        //        NSURLResponse * response = nil;
        //        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        //        NSData* jsonData = [NSData dataWithData:data];
        //        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Service Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterServiceAutomation:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }

             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterServiceAutomation"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
             
             NSLog(@"Response on getMasterServiceAutomation = = = =  = %@",ResponseDict);
         }];
        }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Sales Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterSalesAutomation:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }

             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterSalesAutomation"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }

             
             NSLog(@"Response on MasterSalesAutomation = = = =  = %@",ResponseDict);
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Product Chemicals Service Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetProductChemicalsServiceAutomation:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }

             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"ProductChemicalsServiceAutomation"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }


             NSLog(@"Response on getProductChemicalsServiceAutomation = = = =  = %@",ResponseDict);
         }];
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}
//============================================================================
//============================================================================
#pragma mark--- Send Email Service Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgSendEmailServiceAutomation:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
//    [request setHTTPMethod:@"GET"];
//    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        
             NSError *error = nil;
             NSURLResponse * response = nil;
             NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:data options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
        NSString *strIsuccess=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"IsSuccess"]];
        if ([strIsuccess isEqualToString:@"0"]) {
            
            
        } else {
            
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
        }

//             NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//             if ([returnString isEqualToString:@"\"Your password has been successfully sent. Please check your email.\""]) {
//                 ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:returnString,@"ReturnMsg", nil];
//             } else {
//                 ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:returnString,@"ReturnMsg", nil];
//             }
        
             NSLog(@"Response on Send Email Forgot Password = = = =  = %@",ResponseDict);
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}
//============================================================================
//============================================================================
#pragma mark--- Send Email Service Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgSendSurveyStatus:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
  
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             
             NSString *returnString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
             if ([returnString isEqualToString:@"Success"]) {
                 ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"",@"ReturnMsg", nil];
             } else {
                 ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:returnString,@"ReturnMsg", nil];
             }
             
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }

    
    /*
    @try {
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"Success"]) {
            ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"",@"ReturnMsg", nil];
        } else {
            ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:returnString,@"ReturnMsg", nil];
        }
        NSLog(@"Response on Send Email Service Automation = = = =  = %@",ResponseDict);
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
     */
    
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Custom Message
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgGetCustomMessage:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict=dictData;
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }

        if (strException.length==0) {
            
            
        } else {
            ResponseDict=nil;
        }


        NSLog(@"Response on DashBoard Details = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

- (void)onBackendResponseDeleteMeter:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        
       // NSString *strResult= ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        

        
        NSString *strIsuccess=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"IsSuccess"]];
        
        if ([strIsuccess isEqualToString:@"0"]) {
            
            
        } else {
            
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
        }

    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Save to Core Data Add Lead Info
//============================================================================
//============================================================================

void(^saveToCoreDataAddLeadCallback)(BOOL success, NSError *error);

// --------------
- (void)saveToCoreDataLeadInfo:(NSString *)url :(NSString *)type :(NSDictionary *)dictData withCallback:(CoreDataAddleadsSaveBlock)callback
{
    saveToCoreDataAddLeadCallback = callback;
    [self onSavingDataAddLead:nil withSuccess:YES error:nil withUrl:url withtype:type withDictData:dictData];
}

- (void)onSavingDataAddLead:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withtype:(NSString*)type withDictData:(NSDictionary*)dictData
{
}

//============================================================================
//============================================================================
#pragma mark--- Delete Data AddLead
//============================================================================
//============================================================================

-(void)deleteDatabaseBeforeAdding
{
}


//============================================================================
//============================================================================
#pragma mark--- Save Data OutBox
//============================================================================
//============================================================================

//============================================================================
//============================================================================
#pragma mark--- Save to Core Data Add Lead Info
//============================================================================
//============================================================================

void(^saveToCoreDataOutBoxCallback)(BOOL success, NSError *error);

// --------------
- (void)saveToCoreDataOutBox:(NSString *)url :(NSString *)type :(NSDictionary *)dictData withCallback:(CoreDataAddleadsSaveBlock)callback
{
    saveToCoreDataOutBoxCallback = callback;
    [self onSavingDataOutBox:nil withSuccess:YES error:nil withUrl:url withtype:type withDictData:dictData];
}

- (void)onSavingDataOutBox:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withtype:(NSString*)type withDictData:(NSDictionary*)dictData
{
    saveToCoreDataOutBoxCallback(success, error);
}

//============================================================================
//============================================================================
#pragma mark--- Sent Lead To Server OutBox
//============================================================================
//============================================================================

void(^getServerResponseForSendLeadToServer)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForSendLeadToServer:(NSString *)url :(NSData *)requestDataa :(NSDictionary *)dictData :(NSString*)indexToDletee :(NSString*)Type withCallback:(SendLeadToServerPostCompletionBlock)callback
{
    getServerResponseForSendLeadToServer = callback;
    [self onBackendResponseSendLeadToServer:nil withSuccess:YES error:nil withUrl:url withData:requestDataa withIndex:indexToDletee withType:Type];
}

- (void)onBackendResponseSendLeadToServer:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withData:(NSData*)requestData withIndex:(NSString*)indexToDelete withType:(NSString*)Typeee
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strHrmsCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.HrmsCompanyId"]];
    NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
    NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeId"]];
    NSString *strCreatedBy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"CreatedBy"]];
    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeName"]];
    NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
    NSString *strSalesProcessCompanyId;
    if ([Typeee isEqualToString:@"serviceOrder"]) {
        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ServiceAutoCompanyId"]];

    }else{
        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];

    }

    NSString *strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];

    [request addValue:strHrmsCompanyId forHTTPHeaderField:@"HrmsCompanyId"];
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"IpAddress"];
    [request addValue:strEmployeeNumber forHTTPHeaderField:@"EmployeeNumber"];
    [request addValue:strEmployeeId forHTTPHeaderField:@"EmployeeId"];
    [request addValue:strCreatedBy forHTTPHeaderField:@"CreatedBy"];
    [request addValue:strEmployeeName forHTTPHeaderField:@"EmployeeName"];
    [request addValue:strCoreCompanyId forHTTPHeaderField:@"CoreCompanyId"];
    if ([Typeee isEqualToString:@"serviceOrder"]) {
        
        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"ServiceAutoCompanyId"];
        
    }else{
        
    [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
        
    }
    [request addValue:strCompanyKey forHTTPHeaderField:@"CompanyKey"];

    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        if ([Typeee isEqualToString:@"leadFromOutbox"]) {
            
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
             {
                 NSData* jsonData = [NSData dataWithData:data];
                 ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                 ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
                 NSLog(@"Response ===========  %@",ResponseDict);
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 
                 if (strException.length==0) {
                     
                     UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Your Lead has been sent to server." message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                     
                     [self deleteFromOutBoxCoreData:indexToDelete];
                     
                 } else {
                     
                     UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to send lead to server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                     
                 }

             }];

        } else {
            
            NSError *error = nil;
            NSURLResponse * response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
        }
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForSendLeadToServer(success, ResponseDict, error);
}


//============================================================================
//============================================================================
#pragma mark--- -----------------Null Conversion-------------------
//============================================================================
//============================================================================

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

//============================================================================
//============================================================================
#pragma mark ---------------- Date Change Method--------------------------
//============================================================================
//============================================================================

-(NSString *)ChangeDateToLocalDate :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];

    return finalTime;
}
-(NSString *)ChangeDateToLocalDateFull :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateOther :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}
-(NSString *)ChangeDateToLocalDateOtherServiceAppointments :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateOtherTimeAlso :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateOtherSep :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}
-(NSString *)ChangeDateToLocalDateOtherNilind :(NSString*)strDateToConvert{
    //2018-12-20T00:00:00
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}
-(NSString *)ChangeDateToLocalDateOtherNew :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

//============================================================================
//============================================================================
#pragma mark--- Sent Lead To Server OutBox Offline
//============================================================================
//============================================================================

void(^getServerResponseForSendLeadToServerOffline)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForSendLeadToServerOffline:(NSString *)url :(NSData *)requestDataa :(NSDictionary *)dictData :(NSString*)indexToDletee :(NSDictionary *)dictDataAllValues withCallback:(SendLeadToServerPostCompletionBlockOffline)callback
{
    getServerResponseForSendLeadToServerOffline = callback;
    [self onBackendResponseSendLeadToServerOffline:nil withSuccess:YES error:nil withUrl:url withData:requestDataa withIndex:indexToDletee withDataValues:dictDataAllValues];
}

- (void)onBackendResponseSendLeadToServerOffline:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withData:(NSData*)requestData withIndex:(NSString*)indexToDelete withDataValues:(NSDictionary*)dictLoginData
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *strHrmsCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"HrmsCompanyId"]];
    NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    NSString *strCreatedBy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"CreatedBy"]];
    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"CoreCompanyId"]];
    NSString *strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"SalesProcessCompanyId"]];
    NSString *strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"CompanyKey"]];
    
    [request addValue:strHrmsCompanyId forHTTPHeaderField:@"HrmsCompanyId"];
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"IpAddress"];
    [request addValue:strEmployeeNumber forHTTPHeaderField:@"EmployeeNumber"];
    [request addValue:strEmployeeId forHTTPHeaderField:@"EmployeeId"];
    [request addValue:strCreatedBy forHTTPHeaderField:@"CreatedBy"];
    [request addValue:strEmployeeName forHTTPHeaderField:@"EmployeeName"];
    [request addValue:strCoreCompanyId forHTTPHeaderField:@"CoreCompanyId"];
    [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
    [request addValue:strCompanyKey forHTTPHeaderField:@"CompanyKey"];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             NSLog(@"Response ===========  %@",ResponseDict);
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (strException.length==0) {
                 
//                 UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Your Lead has been sent to server." message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                 [alert show];
                 
                 [self deleteFromOutBoxCoreDataOffline:indexToDelete];
                 
             } else {
                 
//                 UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to send lead to server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                 [alert show];
                 
             }

         }];
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForSendLeadToServerOffline(success, ResponseDict, error);
}


//============================================================================
//============================================================================
#pragma mark--- Sales Dynamic Json Call Back
//============================================================================
//============================================================================

void(^getServerResponseForDynamicJsonUrlCallbackPostMethod)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForSalesDynamicJson:(NSString *)url :(NSData *)requestDataa withCallback:(SalesDynamicJsonPostCompletionBlock)callback
{
    getServerResponseForDynamicJsonUrlCallbackPostMethod = callback;
    [self onBackendResponseDynamicJsonPostMethod:nil withSuccess:YES error:nil withUrl:url withData:requestDataa];
}

- (void)onBackendResponseDynamicJsonPostMethod:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withData:(NSData*)requestData
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        //        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
        //                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
        //         {
        //             NSData* jsonData = [NSData dataWithData:data];
        //             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        //         }];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
       // NSData* jsonData = [NSData dataWithData:data];
        NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"Successful"] || [returnString isEqualToString:@"Success"]) {
            ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"",@"ReturnMsg", nil];
        } else {
            ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:returnString,@"ReturnMsg", nil];
        }
        NSLog(@"Response on Send Sale Automation Dynamic Json = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForDynamicJsonUrlCallbackPostMethod(success, ResponseDict, error);
}
//============================================================================
#pragma mark--- CHANGE BY NILIND
//============================================================================

//Nilind 24 Sept
//============================================================================
#pragma mark- MODIFY DATE
//============================================================================
-(NSString *)modifyDate
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString* finalTime = [dateFormatter stringFromDate:[NSDate date]];
    return finalTime;
}
-(NSString *)modifyDateService
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString* finalTime = [dateFormatter stringFromDate:[NSDate date]];
    return finalTime;
}

//..........Nilind 5 Oct.....................................


-(void)updateSalesModifydate:(NSString *)strLeadId
{
    
}

//...........................................................
//===========================================================

-(BOOL)isCameraPermissionAvailable{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        // do your logic
        
        return true;
        
    } else if(authStatus == AVAuthorizationStatusDenied){
        // denied
        
        return false;
        
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
        
        return false;
        
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeVideo);
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeVideo);
            }
        }];
        
        return false;
        
    } else {
        // impossible, unknown authorization status
        
        return false;
        
    }
}
-(BOOL)isGalleryPermission{
    
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    if (status != ALAuthorizationStatusAuthorized) {
        //show alert for asking the user to give permission
        return false;
    }else
    return true;
}
-(BOOL)isAudioPermissionAvailable{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        // do your logic
        
        return true;
        
    } else if(authStatus == AVAuthorizationStatusDenied){
        // denied
        
        return false;
        
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
        
        return false;
        
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeAudio);
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeAudio);
            }
        }];
        
        return false;
        
    } else {
        // impossible, unknown authorization status
        
        return false;
        
    }
}
//============================================================================
//============================================================================
#pragma mark--- -----------------deleting From OutBox Core Data-------------------
//============================================================================
//============================================================================

-(void)deleteFromOutBoxCoreData :(NSString *)strImageName{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"AddMeterReading" inManagedObjectContext:context]];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"meterImageName=%@",strImageName];
    [allData setPredicate:p];
    NSError * error = nil;
    NSArray *Data = [context executeFetchRequest:allData error:&error];
    for (NSManagedObject * car in Data) {
        [context  deleteObject:car];
        NSError *saveError = nil;
        [context save:&saveError];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OutBoxRefresh" object:self];
    
}
-(void)deleteFromOutBoxCoreDataOffline :(NSString *)strImageName{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"AddMeterReading" inManagedObjectContext:context]];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"meterImageName=%@",strImageName];
    [allData setPredicate:p];
    NSError * error = nil;
    NSArray *Data = [context executeFetchRequest:allData error:&error];
    for (NSManagedObject * car in Data) {
        [context  deleteObject:car];
        NSError *saveError = nil;
        [context save:&saveError];
    }
    
}
+(void)addPaddingView:(UITextField*)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

// Akshay

+(void)postData:(NSString *)urlStr parameters: (NSDictionary *)parameters success: (void(^)(NSDictionary * response))success failure: (void(^)(NSError * error))failure
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer =  [AFHTTPResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    
    serializer.timeoutInterval = 600.0;
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    serializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithArray:@[@"POST"]];
    manager.requestSerializer = serializer;
    
    [manager POST:urlStr parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
     }
         progress:nil success:^(NSURLSessionTask *operation, id responseObject)
     {
         NSString* newStr = [NSString stringWithUTF8String:[responseObject bytes]];
         NSLog(@"String Response%@",newStr);
         
         responseObject = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
         success(responseObject);
         
     }
          failure:^(NSURLSessionTask *operation, NSError *error)
     {
         failure(error);
     }];
}



-(UIAlertController *)loader_ShowObjectiveC:(UIViewController*)controller :(NSString *)strMessage
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:strMessage message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
     [loadingIndicator setFrame:CGRectMake(10, 5, 50,50)];
    loadingIndicator.hidesWhenStopped  = YES;
    [loadingIndicator startAnimating];
    [alert.view addSubview:loadingIndicator];
    [controller presentViewController:alert animated:NO completion:^{
        
    }];
    return alert;
}




@end
