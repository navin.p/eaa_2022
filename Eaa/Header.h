//
//  Header.h
//  DPS
//
//  Created by Saavan Patidar
//  Copyright © 2016 Saavan. All rights reserved.
//

#ifndef Header_h
#define Header_h

#define Upload date = "15/APR/2024"
//Production Urlssss // Y wali purani hai ab nyi lagayi hai https wali

//#define MainUrl @"http://eaaservice.stagingsoftware.com"
//#define MainUrlImage @"http://eaa.stagingsoftware.com"
//#define MainUrlImageTesting @"http://eaa.stagingsoftware.com"// recommmend by prateek

//
#define MainUrl @"https://metermobileapi.edwardsaquifer.org"
#define MainUrlImage @"https://eomr.edwardsaquifer.org"
#define MainUrlImageTesting @"https://eomr.edwardsaquifer.org"



// //Testing Urlssss

//
//#define MainUrl @"http://eaatestservice.stagingsoftware.com"
//#define MainUrlImage @"http://eaatest.stagingsoftware.com"
//#define MainUrlImageTesting @"http://eaatest.stagingsoftware.com"// recommmend by prateek
//


//====================================================================================================================
//====================================================================================================================

#define UrlLogin @"/api/mobile/login"

//====================================================================================================================

#define UrlForgotPassword @"/api/mobile/ForGotPassword?Email="
#define UrlForgotPasswordType @"&Type"

//====================================================================================================================

#define UrlSignUp @"/api/mobile/SignUp"

//====================================================================================================================

#define UrlGetDashBoardDetails @"/api/mobile/GetMobileDashboardMeterList?CustomerId="

//====================================================================================================================

#define UrlGetManageMeterList @"/api/mobile/GetMeters"

//====================================================================================================================

//api/LeadNowAppToSalesProcess/GetLeadCountReportAsync?employeeId=

#define UrlGetHistoryList @"/api/mobile/GetMeterReadingHistory"

//====================================================================================================================

#define UrldownloadImages @"/rimages/"

//====================================================================================================================
//====================================================================================================================

#define UrlAddUpdateMeter @"/api/mobile/InsertUpdateMeter"
#define UrlUpdateMeter @"/api/Mobile/UpdateMeter"

//====================================================================================================================
//====================================================================================================================

#define UrlImageUpload @"/api/File/UploadAsync"

//====================================================================================================================
//====================================================================================================================

#define UrlDeleteMeter @"/api/mobile/DeleteMeter?MeterId="
#define UrlDeleteMeterAuthId @"&AuthId="

//====================================================================================================================
//====================================================================================================================

#define UrlAddMeterReading @"/api/mobile/InsertMeterReading"

#define UrlReplaceMeter @"/api/mobile/ReplaceMeter"
#define UrlInsertUpdateAccuracyTest  @"/api/mobile/InsertUpdateAccuracyTest"
#define UrlGetAccuracyTests  @"/api/mobile/GetAccuracyTests"


#define UrlAccessDelegate @"/api/mobile/SaveAccessDelegate"

#define UrlInsertUpdateNotifDevice @"/api/mobile/InsertUpdateNotifDevice"


//====================================================================================================================
//====================================================================================================================

#define UrlRegisterNewMeter @"/api/mobile/InsertUpdateMeter"

//====================================================================================================================
//====================================================================================================================

#define UrlDeleteTaskGlobal @"/api/LeadNowAppToSalesProcess/DeleteTaskById?leadTaskId="

//====================================================================================================================
//====================================================================================================================

#define UrlGetActivityListGlobal @"/api/LeadNowAppToSalesProcess/GetActivitiesByRefIdAndRefTypeAsync?refId="
#define UrlGetActivityListGlobalrefType @"&refType=Employee"
#define UrlGetActivityListGlobalcompanykey @"&companyKey="

//====================================================================================================================
//====================================================================================================================

#define UrlAddUpdateActivityGlobal @"/api/LeadNowAppToSalesProcess/AddUpdateActivity"

//====================================================================================================================
//====================================================================================================================

#define UrlDeleteActivityGlobal @"/api/LeadNowAppToSalesProcess/DeleteActivityById?activityId="

//====================================================================================================================
//====================================================================================================================

//----------------------------------------------***************--------------------------------------------------------

#define UrlGetTaskList @"/api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsync?refId="
#define UrlGetTaskListrefType @"&refType=Lead"
#define UrlGetTaskListcompanyKey @"&companyKey="

//====================================================================================================================
//====================================================================================================================

#define UrlGetActivityList @"/api/LeadNowAppToSalesProcess/GetActivitiesByRefIdAndRefTypeAsync?refId="
#define UrlGetActivityListrefType @"&refType=Lead"
#define UrlGetActivityListcompanyKey @"&companyKey="

//====================================================================================================================
//====================================================================================================================

#define UrlAddUpdateActivity @"/api/LeadNowAppToSalesProcess/AddUpdateActivity"

//====================================================================================================================
//====================================================================================================================

#define UrlDeleteActivity @"api/LeadNowAppToSalesProcess/AddUpdateLead"

//====================================================================================================================
//====================================================================================================================

#define UrlGetTotalLead @"api/LeadNowAppToSalesProcess/GetAllLeadsForEmployeeAsync?lastModifiedDate="

//====================================================================================================================
//====================================================================================================================

#define UrlToUpdateLeadInfo @"/api/LeadNowAppToSalesProcess/UpdateLead"

//====================================================================================================================
//====================================================================================================================

#define UrlPostComment @"/api/LeadNowAppToSalesProcess/AddCommentToActivity"

//====================================================================================================================
//====================================================================================================================


#pragma marks   ---------------------------------Service Automation--------------------------------

//--------------------**************-----------------*********************-----------------******************----------
//--------------------**************-----------------*********************-----------------******************----------
//--------------------**************-----------------*********************-----------------******************----------

//Service Automations

//====================================================================================================================
//====================================================================================================================

#define UrlAudioUploadService @"/api/File/UploadAsync"

//====================================================================================================================
//====================================================================================================================

#define UrlGetMasterServiceAutomation @"/api/MobileToServiceAuto/GetMastersByCompanyKey?CompanyKey="

//====================================================================================================================
//====================================================================================================================

#define UrlGetMasterProductChemicalsServiceAutomation @"/api/MobileToServiceAuto/GetAllProductByCompany?CompanyId="

//====================================================================================================================
//====================================================================================================================

#define UrlGetTotalWorkOrdersServiceAutomation @"/api/MobileToServiceAuto/GetWorkOrderByEmployee?CompanyKey="

#define UrlGetTotalWorkOrdersServiceAutomationEmployeeNo @"&EmployeeNo="

//====================================================================================================================
//====================================================================================================================

#define UrlUpdateWorkorderDetail @"/api/MobileToServiceAuto/UpdateWorkorderDetail"

//====================================================================================================================
//====================================================================================================================

#define UrlUploadBeforeAfter @"/api/File/UploadImageAsync"

//====================================================================================================================
//====================================================================================================================

#define UrlUploadTechCustomerSign @"/api/File/UploadSignatureAsync"

//====================================================================================================================
//====================================================================================================================

#define UrlMainGGQ @"http://ggq.stagingsoftware.com/GoHandler.ashx?"

#define UrlSendEmailServiceAutomation @"http://ggq.stagingsoftware.com/GoHandler.ashx?key=remindersms&aId="

//====================================================================================================================
//====================================================================================================================
//CompanyKey   ComKey
#define UrlGetCustomMessage @"http://ggq.stagingsoftware.com/GoHandler.ashx?key=getSms&ComKey="

//====================================================================================================================
//====================================================================================================================


//====================================================================================================================
//====================================================================================================================

#define UrlServiceDynamicForm @"api/Mobiletoserviceauto/GetJsonFormsByCompanyId?CompanyKey="
#define UrlServiceDynamicFormWorkOrderID @"&WorkorderId="
#define UrlServiceDynamicFormDepartmentId @"&DepartmentId="

//====================================================================================================================
//====================================================================================================================

///Api/MobileToServiceAuto/GetCustomerDocumentsByAccountNo?CompanyKey
#define UrlServiceDocuments @"api/MobileToServiceAuto/GetCustomerDocumentsByAccountNo?CompanyKey="
#define UrlServiceDocumentsAccountNo @"&AccountNo="

//====================================================================================================================
//====================================================================================================================

#define UrlServiceDynamicFormSubmission @"api/Mobiletoserviceauto/UpdateJsonFormsByCompanyId"

//====================================================================================================================
//====================================================================================================================

#define UrlSalesImageUpload @"/api/File/UploadImageAsync"//UploadImageAsync
#define UrlSalesSignatureImageUpload @"/api/File/UploadSignatureAsync"

//====================================================================================================================
//====================================================================================================================

#pragma marks   ---------------------------------Sales Automation--------------------------------

//--------------------**************-----------------*********************-----------------******************----------
//--------------------**************-----------------*********************-----------------******************----------
//--------------------**************-----------------*********************-----------------******************----------


//====================================================================================================================
//====================================================================================================================

#define UrlGetMasterSalesAutomation @"/api/MobileToSaleAuto/GetMastersByCompanyKey?CompanyKey="

//====================================================================================================================
//====================================================================================================================

#define UrlSalesInfoGeneral @"/api/MobileToSaleAuto/GetLeadByCompanyKey?CompanyKey="

//====================================================================================================================
//====================================================================================================================

#define UrlSalesInspection @"/api/MobileToSaleAuto/GetJsonFormList"

//====================================================================================================================
//====================================================================================================================

//api/MobileToSaleAuto/UpdateJsonFormsByCompanyId
//====================================================================================================================
//====================================================================================================================

#define UrlSalesInspectionDynamicFormSubmission @"api/MobileToSaleAuto/UpdateJsonFormsByCompanyId"

//====================================================================================================================
//====================================================================================================================


//====================================================================================================================
//====================================================================================================================

#define UrlGetSalesInspectionDynamicForm @"/api/MobileToSaleAuto/GetJsonFormsByCompanyId?CompanyKey="
#define UrlGetSalesInspectionDynamicFormleadID @"&leadID="
#define UrlGetSalesInspectionDynamicFormInspectionId @"&InspectionId="

//====================================================================================================================

#define UrlMobileCofigData @"/api/mobile/GetMobileCofigData"

//====================================================================================================================


//====================================================================================================================
//====================================================================================================================


//====================================================================================================================
//====================================================================================================================

#define UrlSalesEmail @"http://www.gogetquality.com/survey/GoHandler.ashx?key="

#define UrlUpdateSalesSurveyStatus @"api/MobileToSaleAuto/UpdateLeadSurveyStatus?"

#define UrlUpdateServiceSurveyStatus @"api/Mobiletoserviceauto/UpdateWorkorderSurveyStatus"

//====================================================================================================================
//====================================================================================================================

#define UrlSalesFinalUpdate @"/api/MobileToSaleAuto/UpdateLeadDetail"

// akshay //
 #define UrlInsertMeterRequestComment @"/api/mobile/InsertMeterRequestComment"

#define UrlGetDashBoardDetailsNew @"/api/mobile/GetIsAMRMeterListByCustomerId?CustomerId="

#define UrlGetARMMeterReadingHistoryNew @"/api/mobile/GetIsAMRMeterReadingHistoryByMeterNo?AMRSerialNumber="

//Nilind
#define UrlGetMeterListDetails @"/api/mobile/GetMeterlistByCustomerId?CustomerId="
#define UrlForSubmitUser @"/api/mobile/AssociateCreateAMRUser"
//api/Mobiletoserviceauto/GetJsonFormsByCompanyId



#define UrlGetNotificationsHistoryByAuthId @"/api/mobile/GetNotificationsHistoryByAuthId?AuthId="


//====================================================================================================================
//====================================================================================================================

#define Alert @"Alert!"
#define Info @"Information."
#define ErrorInternetMsg @"Please check your internet connection and relaunch the app."
#define Sorry @"Sorry,something went wrong. Please try again later."
#define InvalidLogin @"Invalid login attempt."
#define failMailSend @"Unable to send mail.Please try again later."
#define SuccessMailSend @"Your password has been successfully sent. Please check your email."
#define failSMSSend @"Unable to send SMS. Please try again later."
#define SuccessSMSSend @"SMS sent Successfully."
#define AlertSelectCustomMsg @"Before sending SMS.Please select Custom Message from Setting Menu."
#define NoBeforeImg @"No images available to show."
#define NoPreview @"No Preview available to show."
#define imageThree @"You have already attached max limit of Photo."
#define NoStatus @"No Status Available.\n Please check either Status are set on Web or your internet connection and relaunch the App"
#define NoCustomMsg @"No Custom Message Available.\n Please check either Custom Message are set on Web or your internet connection and relaunch the App"
#define NoService @"No Services Available.\n Please check either Services are set on Web or your internet connection and relaunch the App"
#define NoUrgency @"No Urgency Available.\n Please check either Urgency are set on Web or your internet connection and relaunch the App"
#define NoDataAvailable @"No Data Available."
#define NoDataAvailableMeterName @"No Data Available.\n Please check either Meter are available or your internet connection and relaunch the App"

#endif /* Header_h */
