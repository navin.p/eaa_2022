//
//  HelpViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 05/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
- (IBAction)action_Back:(id)sender;
@property (strong,nonatomic) NSString *strHelpMessage;
@property (strong,nonatomic) NSMutableAttributedString *strHelpBoldMessage;
@property (weak, nonatomic) IBOutlet UITextView *textViewHelpMsg;
@end
