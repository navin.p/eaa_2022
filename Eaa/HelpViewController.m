//
//  HelpViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 05/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "HelpViewController.h"
#import "DashBoardViewController.h"
#import "TechDashBoardViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(_strHelpMessage.length>0)
    {
        _textViewHelpMsg.text = _strHelpMessage;
    }
    if(_strHelpBoldMessage.length>0)
    {
        _textViewHelpMsg.attributedText = _strHelpBoldMessage;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_Back:(id)sender {
    /*
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
        for (UIViewController *controller in self.navigationController.viewControllers)
        {
            if ([controller isKindOfClass:[DashBoardViewController class]])
            {
                [self.navigationController popToViewController:controller animated:NO];
                //break;
            }
        }
    }else{
        
        for (UIViewController *controller in self.navigationController.viewControllers)
        {
            if ([controller isKindOfClass:[TechDashBoardViewController class]])
            {
                [self.navigationController popToViewController:controller animated:NO];
                //break;
            }
        }
    }
     */
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
        [self.navigationController popViewControllerAnimated:NO];
//        int index = 0;
//        NSArray *arrstack=self.navigationController.viewControllers;
//        for (int k=0; k<arrstack.count; k++) {
//            if ([[arrstack objectAtIndex:k] isKindOfClass:[DashBoardViewController class]]) {
//                index=k;
//                // break;
//            }
//        }
//        DashBoardViewController *myController = (DashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
//        [self.navigationController popToViewController:myController animated:NO];
        
    }else{
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k=0; k<arrstack.count; k++) {
            if ([[arrstack objectAtIndex:k] isKindOfClass:[TechDashBoardViewController class]]) {
                index=k;
                // break;
            }
        }
        TechDashBoardViewController *myController = (TechDashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        
    }

}
@end
