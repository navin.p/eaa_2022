//
//  HelpViewGlobalViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 15/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewGlobalViewController : UIViewController
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtVieww;
@property (strong,nonatomic) NSString *strHelpMessage;
@end
