//
//  ConstantClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreLocation

import CoreTelephony
import MessageUI
import CoreData
import SystemConfiguration
import AVKit

//MARK: Primary Uses
var nsud = UserDefaults.standard
var mainStoryboard : UIStoryboard = UIStoryboard()
var aryPeriodDataGloble = NSMutableArray()



//MARK: Globle Variable


var DeviceID = ""
var PushToken = String()
var PlatForm = "iPhone"



//MARK: Alert Images


//MARK: Primary Color
var primaryTheamColor = "EC2421"
var primaryDark = "354750"


//MARK: Version Information
var app_Version : String = "6.5.6"
var app_VersionDate : String = "24/OCT/2019"
var app_VersionSupport : String = "Requires iOS 10.0 or later.Compatible with iPhone."

var app_URL : String = "https://itunes.apple.com/us/app/citizen-cop/id631959732"
var app_ShareText : String = "The Personal Safety Tool for Citizens #CitizenCOP"

//MARK: Common Alert
var AppName = "Eaa"
var alertMessage = "Alert!"
var alertInfo = "Information!"
var alertInternet = "No Internet Connection, try later!"
var alertDataNotFound = "Data is not Available!"
var alertSomeError = "Somthing went wrong please try again!"
var alertCalling = "Your device doesn't support this feature."
var alertLogout = "Are you sure want to logout ?"
var alertRemove = "Are you sure want to remove?"
var alertDelete = "Are you sure want to delete?"
var Alert_Gallery = "Gallery"
var Alert_Camera = "Camera"
var Alert_Preview = "Preview"
var alert_Email = "Email address required!"
var alert_InvalidEmail = "Email address invalid!"
var alert_Mobile_limit = "Mobile number invalid!"
var alert_Mobile = "Mobile number required!"
var alertCountry = "Country name required!"
var alertState = "State name required!"
var alertCity = "City name required!"


//MARK:
//MARK:API
//NAVIN

 //Testing Urlssss


//var  MainUrl = "http://eaatestservice.stagingsoftware.com/api/Mobile/"

//Live Urlssss
var  MainUrl = "https://metermobileapi.edwardsaquifer.org/api/Mobile/"
var UrlGetGetMeterYearReading = "\(MainUrl)GetMeterYearReading?MeterID="
var UrlGetReportingPeriodData = "\(MainUrl)GetReportingPeriodData"







//MARK:
//MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}
struct DeviceType{
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
    static let IS_IPHONE_XR_XS_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH >= 1024.0
}
func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}

 func loader_Show(controller: UIViewController , strMessage : String) -> UIAlertController {
    let alert = UIAlertController(title: nil, message: strMessage, preferredStyle: .alert)
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
    loadingIndicator.hidesWhenStopped = true
    loadingIndicator.style = UIActivityIndicatorView.Style.gray
    loadingIndicator.startAnimating();
    alert.view.addSubview(loadingIndicator)
    controller.present(alert, animated: false, completion: nil)
    return alert
}
func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}
