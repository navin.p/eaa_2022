//
//  WebServiceClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

import Alamofire





 @objc class WebServiceClass: NSObject , NSURLConnectionDelegate,XMLParserDelegate {
    
     //MARK: REST API Calling With Json
     
     
     class func callAPIBYGET(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
         let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
         AF.request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
             switch(response.result) {
             case .success(_):
                 if let data = response.value
                 {
                     if(data is NSArray){
                         let dictData = NSMutableDictionary()
                         dictData.setValue(data as! NSArray, forKey: "data")
                         OnResultBlock(dictData ,true)

                     }else{
                         OnResultBlock((data as! NSDictionary) ,true)
                     }
                     
                 }
                 break
             case .failure(_):
                 print(response.error.debugDescription ?? 0)
                 let dic = NSMutableDictionary.init()
                 dic .setValue("\(alertSomeError) ", forKey: "message")
                 OnResultBlock(dic,false)
                 break
             }
         }
     }

  
    // MARK: - Convert String To Dictionary Function
    class func convertJsonToDictionary(text: String) -> [String: Any]? {
        if let data = text.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String:AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}

