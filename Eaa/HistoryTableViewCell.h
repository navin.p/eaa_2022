//
//  HistoryTableViewCell.h
//  Eaa
//
//  Created by Saavan Patidar on 27/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_CustName_Value;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CustomerName;
@property (strong, nonatomic) IBOutlet UILabel *lblColorLine;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Status;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Comment;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CommentValue;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Reason;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ReasonValue;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PumpedAmount;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PumpedAmountValue;
@property (strong, nonatomic) IBOutlet UILabel *lbl_NickName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SerialNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_LastMeterReading;
@property (strong, nonatomic) IBOutlet UILabel *lbl_DAte;
@property (strong, nonatomic) IBOutlet UIImageView *meterImgView;
@property (strong, nonatomic) IBOutlet UILabel *lblStatusLabell;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_PumpedAmount_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ResetReason_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_CustomerName_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_CustomerName_Top;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_MeterNickName_Top;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_MeterNickNameValue_Top;
@property (weak, nonatomic) IBOutlet UILabel *labelCommentTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelComment;
@property (weak, nonatomic) IBOutlet UILabel *labelType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceLabelType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_h_date;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_h_StatusTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_h_status;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_h_reasonTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_h_reason;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_h_customerName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_h_commentTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblLastMeterReadingTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalConstDateAndMeterSerialNo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalConstCustomerNameAndDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_h_meterReading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_h_pumpedAmount;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceLabelTypeAndDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceType;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceComment;


// Notification Hub
@property (strong, nonatomic) IBOutlet UILabel *lblNotificationTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblNotificationSentDate;

@end
