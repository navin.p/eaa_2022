//
//  HistoryViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 27/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface HistoryViewController : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityHistoryList;
    NSManagedObjectContext *contextHistoryList;
    NSFetchRequest *requestHistoryList;
    NSSortDescriptor *sortDescriptorHistoryList;
    NSArray *sortDescriptorsHistoryList;
    NSManagedObject *matchesHistoryList;
    NSArray *arrAllObjHistoryList;
}

- (IBAction)action_MenuBar:(id)sender;
- (IBAction)action_AddMeterReading:(id)sender;
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewHistory;
@property (strong, nonatomic) IBOutlet UITableView *tblViewSideMenu;
@property (strong, nonatomic) IBOutlet UIView *viewwSideMenu;
@property (strong, nonatomic) IBOutlet UIView *hiddenView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImgView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_Img_W;
- (IBAction)action_Help:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerHistoryList;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tblViewHeight;

@end
