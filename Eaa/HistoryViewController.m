//
//  HistoryViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 27/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryTableViewCell.h"
#import "SlideMenuTableViewCell.h"
#import "ReportMeterReadingViewController.h"
#import "AddMeterViewController.h"
#import "ManageMeterViewController.h"
#import "OutboxViewController.h"
#import "DisclaimerViewController.h"
#import "HelpViewController.h"
#import "LoginViewController.h"
#import "TermsConditionViewController.h"
#import "DashBoardViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "HelpViewGlobalViewController.h"
#import "TechDashBoardViewController.h"
#import "HistoryListt+CoreDataProperties.h"


@interface HistoryViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *arrOfImgSlideMenu,*arrNameSlideMenu,*arrOfStatus;
    Global *global;
    NSArray *arrOfHistory;
    BOOL yesFiltered;
    NSMutableArray *filteredArray;
    __weak IBOutlet NSLayoutConstraint *verticalSpaceLabelType;
}

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    yesFiltered=NO;
    _tblViewHistory.estimatedRowHeight = 217;
    _tblViewHistory.rowHeight = UITableViewAutomaticDimension;
    arrOfImgSlideMenu=[NSArray arrayWithObjects:@"add meter.png",@"add meter.png",@"add meter reading.png",@"add meter.png",@"history.png",@"disclaimer.png",@"help.png",@"help.png",@"offline mode.png", nil];
    
    arrNameSlideMenu=[NSArray arrayWithObjects:@"Home",@"Add Meter",@"Add Meter Reading",@"Manage Meters",@"History",@"Disclaimer",@"Terms & Conditions",@"Help",@"Outbox", nil];
    
    arrOfStatus=[NSArray arrayWithObjects:@"New",@"Approved",@"Rejected",@"New",@"Approved",@"Rejected",@"New",@"Approved",@"Rejected", nil];
    
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    if (dictLoginDetail.count==0) {
        
    } else {
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            
            [self fetchFromCoreDataHistoryList];
            
            //            NSString *strTitle = Alert;
            //            NSString *strMsg = ErrorInternetMsg;
            //            [global AlertMethod:strTitle :strMsg];
            //            [DejalBezelActivityView removeView];
        }
        else
        {
            NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
            NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
            
            if ([strUserType isEqualToString:@"Customer"]) {
                
                [self getHistoryDetails:[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"AuthId"]] :[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]]];
                
            }else{
                
                [self getHistoryDetails:[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"AuthId"]] :[NSString stringWithFormat:@"%@",@"0"]];
            }
        }
    }
    
    // [self advanceSearchButton];
    
    _tblViewHistory.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    self.tblViewHistory.rowHeight = UITableViewAutomaticDimension;
    self.tblViewHistory.estimatedRowHeight = 200.0;
    
    // Do any additional setup after loading the view.
}


- (IBAction)action_MenuBar:(id)sender {
    
    CGRect frameForHiddenView=CGRectMake(0, 0, self.view.frame.size.width-50, _hiddenView.frame.size.height+200);
    frameForHiddenView.origin.x=200;
    frameForHiddenView.origin.y=20;
    [_hiddenView setFrame:frameForHiddenView];
    [self.view addSubview:_hiddenView];
    
    CGRect frameForCheckView=CGRectMake(0, 20, self.view.frame.size.width-50, _viewwSideMenu.frame.size.height+200);
    frameForCheckView.origin.x=0;
    frameForCheckView.origin.y=20;
    [_viewwSideMenu setFrame:frameForCheckView];
    [self.view addSubview:_viewwSideMenu];
    
    _profileImgView.layer.cornerRadius = _const_Img_W.constant/2;
    _profileImgView.clipsToBounds = YES;
    
    [self performSelector:@selector(profileImageViewSetting) withObject:nil afterDelay:2.0];
}

-(void)profileImageViewSetting{
    
    _profileImgView.layer.cornerRadius = _const_Img_W.constant/2;
    _profileImgView.clipsToBounds = YES;
    
}

- (IBAction)action_AddMeterReading:(id)sender {
}

- (IBAction)action_Back:(id)sender {
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k=0; k<arrstack.count; k++) {
            if ([[arrstack objectAtIndex:k] isKindOfClass:[DashBoardViewController class]]) {
                index=k;
                // break;
            }
        }
        DashBoardViewController *myController = (DashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        
    }else{
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k=0; k<arrstack.count; k++) {
            if ([[arrstack objectAtIndex:k] isKindOfClass:[TechDashBoardViewController class]]) {
                index=k;
                // break;
            }
        }
        TechDashBoardViewController *myController = (TechDashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        
    }
}
- (IBAction)action_Help:(id)sender {
    
    [self goToHelpView];
    
}

//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (yesFiltered)
    {
        return filteredArray.count;
    }
    else
    {
        return arrOfHistory.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)setUpCell:(HistoryTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    if (indexPath.row%2==0)
    {
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:161/255.0f green:172/255.0f blue:179/255.0f alpha:1.0];
    }
    else
    {
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:204/255.0f green:172/255.0f blue:84/255.0f alpha:1.0];
    }
    
    NSDictionary *dictData;
    
    if (yesFiltered)
    {
        dictData=filteredArray[indexPath.row];
    }
    else
    {
        dictData=arrOfHistory[indexPath.row];
    }
    
    cell.lbl_NickName.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterName"]];
    cell.lbl_SerialNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNumber"]];
    cell.lbl_LastMeterReading.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CurrentMeterReading"]];
    cell.lbl_PumpedAmountValue.text=[NSString stringWithFormat:@"%@ Acre-Feet",[dictData valueForKey:@"PumpedAmount"]];
    cell.lbl_DAte.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SubmittedDate"]];
    cell.lbl_Status.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Status"]];
    cell.lbl_ReasonValue.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DeclineReason"]];
    cell.lbl_CustName_Value.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CombinedName"]];
    cell.labelType.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ReadingType"]];
  
    
    cell.lbl_PumpedAmount.hidden = NO;
    cell.lbl_PumpedAmountValue.hidden = NO;
    cell.verticalConstDateAndMeterSerialNo.constant = 45;
    
    cell.const_PumpedAmount_H.constant = 21;
    if (cell.lbl_LastMeterReading.text.length==0)
    {
        cell.lbl_LastMeterReading.text=@"NA";
    }
    
    int const_custName;
    const_custName=20;
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"])
    {
        cell.lbl_CustomerName.hidden = YES;
        cell.lbl_CustName_Value.hidden = YES;
        cell.const_CustomerName_H.constant = 0;
    }
    else
    {
        cell.lbl_CustomerName.hidden = NO;
        cell.lbl_CustName_Value.hidden = NO;
        cell.const_CustomerName_H.constant = 21;
    }
    if ([cell.lbl_Status.text isEqualToString:@"Approved"])
    {
        if([cell.labelType.text isEqualToString:@"Meter Reading"])
        {
            cell.lbl_LastMeterReading.hidden = NO;
            cell.lblLastMeterReadingTitle.hidden = NO;
            cell.lbl_PumpedAmount.hidden = NO;
            cell.lbl_PumpedAmountValue.hidden = NO;
            cell.verticalConstDateAndMeterSerialNo.constant = 45;
            
            cell.const_PumpedAmount_H.constant = 21;
        }
        else
        {
            cell.lbl_LastMeterReading.hidden = YES;
            cell.lblLastMeterReadingTitle.hidden = YES;
            cell.lbl_PumpedAmount.hidden = YES;
            cell.lbl_PumpedAmountValue.hidden = YES;
            cell.verticalConstDateAndMeterSerialNo.constant = 1;
            
            cell.const_PumpedAmount_H.constant = 0;
        }
        
        cell.lbl_ReasonValue.hidden = YES;
        cell.lbl_Reason.hidden = YES;
        cell.const_h_reasonTitle.constant = 0;
        cell.labelComment.hidden = YES;
        cell.labelCommentTitle.hidden = YES;
        cell.const_h_commentTitle.constant = 0;
        cell.verticalSpaceLabelType.constant = 1;
    }
    else if([cell.lbl_Status.text isEqualToString:@"Approved With Edit"])
    {
        if([cell.labelType.text isEqualToString:@"Meter Reading"])
        {
            cell.lbl_LastMeterReading.hidden = NO;
            cell.lblLastMeterReadingTitle.hidden = NO;
            
            cell.lbl_PumpedAmount.hidden = NO;
            cell.const_PumpedAmount_H.constant = 21;
            
        }
        else
        {
            cell.lbl_LastMeterReading.hidden = YES;
            cell.lblLastMeterReadingTitle.hidden = YES;
            cell.lbl_PumpedAmount.hidden = YES;
            cell.const_PumpedAmount_H.constant = 0;
        }
        
        cell.lbl_ReasonValue.hidden = NO;
        cell.lbl_Reason.hidden = NO;
        cell.const_h_reasonTitle.constant = 21;
        cell.labelComment.hidden = NO;
        cell.labelCommentTitle.hidden = NO;
        cell.const_h_commentTitle.constant = 21;
        cell.verticalSpaceLabelType.constant = 67;
        cell.verticalConstDateAndMeterSerialNo.constant = 45;
        
    }
    
    else if ([cell.lbl_Status.text caseInsensitiveCompare:@"New"] == NSOrderedSame || [cell.lbl_Status.text isEqualToString:@""])
    {
        //        if([cell.labelType.text isEqualToString:@"Meter Reading"])
        //        {
        //            cell.lbl_LastMeterReading.hidden = NO;
        //            cell.lblLastMeterReadingTitle.hidden = NO;
        //            cell.lbl_PumpedAmount.hidden = NO;
        //            cell.lbl_PumpedAmountValue.hidden = NO;
        //            //cell.verticalConstDateAndMeterSerialNo.constant = 45;
        //            cell.const_PumpedAmount_H.constant = 21;
        //        }
        //        else
        //        {
        //            cell.lbl_LastMeterReading.hidden = YES;
        //            cell.lblLastMeterReadingTitle.hidden = YES;
        //            cell.lbl_PumpedAmount.hidden = YES;
        //            cell.lbl_PumpedAmountValue.hidden = YES;
        //            //cell.verticalConstDateAndMeterSerialNo.constant = 1;
        //            cell.const_PumpedAmount_H.constant = 0;
        //        }
        
        cell.lbl_PumpedAmount.hidden = YES;
        cell.lbl_PumpedAmountValue.hidden = YES;
        
        cell.lbl_ReasonValue.hidden = YES;
        cell.lbl_Reason.hidden = YES;
        cell.const_h_reasonTitle.constant = 0;
        cell.labelComment.hidden = YES;
        cell.labelCommentTitle.hidden = YES;
        cell.const_h_commentTitle.constant = 0;
        cell.verticalSpaceLabelType.constant = 1;
        cell.verticalConstDateAndMeterSerialNo.constant = 45-21;
        
        if ([strUserType isEqualToString:@"Customer"])
        {
            if ([cell.lbl_Status.text isEqualToString:@""])
            {
                cell.lbl_Status.text=@"NA";
            }
            else
            {
                cell.lbl_Status.text=@"Pending";
            }
        }
        else
        {
            if ([cell.lbl_Status.text isEqualToString:@""])
            {
                cell.lbl_Status.text=@"NA";
            }
        }
    }
    
    else if([cell.lbl_Status.text isEqualToString:@"Rejected"])
    {
        cell.lbl_PumpedAmount.hidden = YES;
        cell.lbl_PumpedAmountValue.hidden = YES;
        cell.const_PumpedAmount_H.constant = 0;
        
        cell.lbl_Reason.hidden = NO;
        cell.lbl_ReasonValue.hidden = NO;
        cell.const_h_reasonTitle.constant = 21;
        
        cell.labelCommentTitle.hidden = NO;
        cell.labelComment.hidden = NO;
        cell.const_h_commentTitle.constant = 21;
        cell.verticalSpaceLabelType.constant = 67;
        //cell.verticalConstDateAndMeterSerialNo.constant = 1;
        
    }
    
    //    else
    //    {
    //        cell.lbl_PumpedAmount.hidden = YES;
    //        cell.lbl_PumpedAmountValue.hidden = YES;
    //        cell.const_PumpedAmount_H.constant = 0;
    //
    //        cell.lbl_ReasonValue.hidden = YES;
    //        cell.lbl_Reason.hidden = YES;
    //        cell.const_h_reasonTitle.constant = 0;
    //
    //        cell.labelCommentTitle.hidden = YES;
    //        cell.labelComment.hidden = YES;
    //        cell.const_h_commentTitle.constant = 0;
    //      //  cell.verticalSpaceLabelType.constant = 67;
    //    }
    
    //http://pcrms.pestream.com/Documents/_20161109133414155.png
    
    NSString *strUrlImage=[NSString stringWithFormat:@"%@%@%@",MainUrlImage,UrldownloadImages,[dictData valueForKey:@"Images"]];
    
    [cell.meterImgView setImageWithURL:[NSURL URLWithString:strUrlImage] placeholderImage:[UIImage imageNamed:@"NoImage.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
}
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell
{
    [sizingCell layoutIfNeeded];
    
    NSArray *sizee=sizingCell.contentView.constraints;
    
    NSMutableArray *arrTEMP=[[NSMutableArray alloc]init];
    
    for (int k=0; k<sizee.count; k++)
    {
        NSLayoutConstraint *constr=sizee[k];
        
        int  asdf = constr.constant;
        
        NSNumber *num = [NSNumber numberWithInt:asdf];
        
        [arrTEMP addObject:num];
        
        NSLog(@"%f",constr.constant);
    }
    
    NSInteger sum = 0;
    for (NSNumber *num in arrTEMP)
    {
        sum += [num intValue];
    }
    
    NSLog(@"Sum======%ld",(long)sum);
    
    
    // CGFloat sizeH=sizingCell.contentView.frame.size.height;
    
    CGFloat sizeH=sizingCell.contentView.frame.size.height;
    
    //    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    return sizeH;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"HistoryTableViewCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    [cell updateConstraintsIfNeeded];
    return cell;
}
- (void)configureCell:(HistoryTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Fetch Record
    if (indexPath.row%2==0)
    {
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:161/255.0f green:172/255.0f blue:179/255.0f alpha:1.0];
    }
    else
    {
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:204/255.0f green:172/255.0f blue:84/255.0f alpha:1.0];
    }
    
    NSDictionary *dictData;
    
    if (yesFiltered)
    {
        dictData=filteredArray[indexPath.row];
    }
    else
    {
        dictData=arrOfHistory[indexPath.row];
    }
    
    cell.lbl_NickName.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterName"]];
    cell.lbl_SerialNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNumber"]];
    cell.lbl_LastMeterReading.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CurrentMeterReading"]];
    cell.lbl_PumpedAmountValue.text=[NSString stringWithFormat:@"%@ Acre-Feet",[dictData valueForKey:@"PumpedAmount"]];
    cell.lbl_DAte.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SubmittedDate"]];
    cell.lbl_Status.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Status"]];
    cell.lbl_ReasonValue.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DeclineReason"]];
    cell.lbl_CustName_Value.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CombinedName"]];
    cell.labelType.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ReadingType"]];
//    cell.labelComment.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Comment"]];
//    if(!(cell.labelComment.text.length>0))
//    {
//        cell.labelComment.text = @" ";
//    }
    cell.labelComment.text = @"";
    
    if (cell.lbl_LastMeterReading.text.length==0)
    {
        cell.lbl_LastMeterReading.text=@"NA";
    }
    BOOL isCustomerNameHidden;
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];

    
    if ([strUserType isEqualToString:@"Customer"])
    {
        cell.const_h_customerName.constant = 0.0;
        isCustomerNameHidden = YES;
    }
    else
    {
        cell.const_h_customerName.constant = 14.0;
        isCustomerNameHidden = NO;
    }
    
    if ([cell.lbl_Status.text isEqualToString:@"Approved"])
    {
        if([cell.labelType.text isEqualToString:@"Meter Reading"])
        {
            cell.const_h_meterReading.constant = 14.0;
            cell.const_h_pumpedAmount.constant = 14.0;
            cell.topSpaceDate.constant = 8.0;
        }
        else
        {
            cell.const_h_meterReading.constant = 0.0;
            cell.const_h_pumpedAmount.constant = 0.0;
            cell.topSpaceDate.constant = -8.0;
        }
        
      
        cell.const_h_reason.constant = 0.0;
      
        cell.const_h_commentTitle.constant = 0.0;
        
        if(isCustomerNameHidden)
        {
            cell.topSpaceType.constant = -8.0;
        }
        else
        {
            cell.topSpaceType.constant = 0.0;
        }

    }
    else if([cell.lbl_Status.text isEqualToString:@"Approved With Edit"])
    {
        if([cell.labelType.text isEqualToString:@"Meter Reading"])
        {
            cell.const_h_meterReading.constant = 14.0;
            cell.const_h_pumpedAmount.constant = 14.0;
        }
        else
        {
            cell.const_h_meterReading.constant = 0.0;
            cell.const_h_pumpedAmount.constant = 0.0;
        }

        cell.const_h_reason.constant = 14.0;

        cell.const_h_commentTitle.constant = 14.0;
        
        cell.labelComment.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Comment"]];
        if(!(cell.labelComment.text.length>0))
        {
          cell.labelComment.text = @" ";
        }
        if(isCustomerNameHidden)
        {
            cell.topSpaceComment.constant = 0.0;
        }
        else
        {
         cell.topSpaceComment.constant = 8.0;
        }
        cell.topSpaceDate.constant = 8.0;
        cell.topSpaceType.constant = 8.0;

    }
    
    else if ([cell.lbl_Status.text caseInsensitiveCompare:@"New"] == NSOrderedSame || [cell.lbl_Status.text isEqualToString:@""])
    {
        if([cell.labelType.text isEqualToString:@"Meter Reading"])
        {
            cell.const_h_meterReading.constant = 14.0;
            cell.const_h_pumpedAmount.constant = 14.0;
            cell.topSpaceDate.constant = 0.0;
        }
        else
        {
            cell.const_h_meterReading.constant = 0.0;
            cell.const_h_pumpedAmount.constant = 0.0;
            cell.topSpaceDate.constant = -8.0;
        }
    
        cell.const_h_pumpedAmount.constant = 0.0;
        
        cell.const_h_reason.constant = 0.0;

        cell.const_h_commentTitle.constant = 0.0;
       
        if(isCustomerNameHidden)
        {
            cell.topSpaceType.constant = -8.0;
        }
        else
        {
            cell.topSpaceType.constant = 0.0;
        }
        
        if ([strUserType isEqualToString:@"Customer"])
        {
            if ([cell.lbl_Status.text isEqualToString:@""])
            {
                cell.lbl_Status.text=@"NA";
            }
            else
            {
                cell.lbl_Status.text=@"Pending";
            }
        }
        else
        {
            if ([cell.lbl_Status.text isEqualToString:@""])
            {
                cell.lbl_Status.text=@"NA";
            }
        }

    }
    
    else if([cell.lbl_Status.text isEqualToString:@"Rejected"])
    {
        if([cell.labelType.text isEqualToString:@"Meter Reading"])
        {
            cell.const_h_meterReading.constant = 14.0;
           // cell.const_h_pumpedAmount.constant = 14.0;
            cell.topSpaceDate.constant = 8.0;
        }
        else
        {
            cell.const_h_meterReading.constant = 0.0;
           // cell.const_h_pumpedAmount.constant = 0.0;
            cell.topSpaceDate.constant = -8.0;
        }

        cell.const_h_pumpedAmount.constant = 0.0;
        cell.const_h_reason.constant = 14.0;
        
        cell.const_h_commentTitle.constant = 14.0;
        
        if(isCustomerNameHidden)
        {
            cell.topSpaceComment.constant = 0.0;
        }
        else
        {
            cell.topSpaceComment.constant = 8.0;
        }
        
        cell.labelComment.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Comment"]];
        if(! (cell.labelComment.text.length>0))
        {
            cell.labelComment.text = @" ";
        }
        
//        if(isCustomerNameHidden)
//        {
//            cell.topSpaceType.constant = -8.0;
//        }
//        else
//        {
            cell.topSpaceType.constant = 8.0;
        //}
    }
    
    else
    {

        cell.const_h_meterReading.constant = 14.0;
        
        cell.const_h_pumpedAmount.constant = 0.0;
        
        cell.const_h_reason.constant = 0.0;
        
        cell.const_h_commentTitle.constant = 0.0;
        cell.topSpaceDate.constant = 0.0;
       
        if(isCustomerNameHidden)
        {
            cell.topSpaceType.constant = -8.0;
        }
        else
        {
            cell.topSpaceType.constant = 0.0;
        }
    }
    
    //http://pcrms.pestream.com/Documents/_20161109133414155.png
    
    NSString *strUrlImage=[NSString stringWithFormat:@"%@%@%@",MainUrlImage,UrldownloadImages,[dictData valueForKey:@"Images"]];
    
    [cell.meterImgView setImageWithURL:[NSURL URLWithString:strUrlImage] placeholderImage:[UIImage imageNamed:@"NoImage.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
  
}


//{
//    // Fetch Record
//    if (indexPath.row%2==0)
//    {
//        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:161/255.0f green:172/255.0f blue:179/255.0f alpha:1.0];
//    }
//    else
//    {
//        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:204/255.0f green:172/255.0f blue:84/255.0f alpha:1.0];
//    }
//
//    NSDictionary *dictData;
//
//    if (yesFiltered)
//    {
//        dictData=filteredArray[indexPath.row];
//    }
//    else
//    {
//        dictData=arrOfHistory[indexPath.row];
//    }
//
//    cell.lbl_NickName.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterName"]];
//    cell.lbl_SerialNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNumber"]];
//    cell.lbl_LastMeterReading.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CurrentMeterReading"]];
//    cell.lbl_PumpedAmountValue.text=[NSString stringWithFormat:@"%@ Acre-Feet",[dictData valueForKey:@"PumpedAmount"]];
//    cell.lbl_DAte.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SubmittedDate"]];
//    cell.lbl_Status.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Status"]];
//    cell.lbl_ReasonValue.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DeclineReason"]];
//    cell.lbl_CustName_Value.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CombinedName"]];
//    cell.labelType.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ReadingType"]];
//
//    if([cell.labelType.text isEqualToString:@"Accuracy Tests"])
//    {
//        cell.lbl_LastMeterReading.hidden = YES;
//        cell.lblLastMeterReadingTitle.hidden = YES;
//        cell.lbl_PumpedAmount.hidden = YES;
//        cell.lbl_PumpedAmountValue.hidden = YES;
//        cell.verticalConstDateAndMeterSerialNo.constant = 1;
//    }
//    else
//    {
//        cell.lbl_LastMeterReading.hidden = NO;
//        cell.lblLastMeterReadingTitle.hidden = NO;
//        cell.lbl_PumpedAmount.hidden = NO;
//        cell.lbl_PumpedAmountValue.hidden = NO;
//        cell.verticalConstDateAndMeterSerialNo.constant = 45;
//    }
//    if (cell.lbl_LastMeterReading.text.length==0)
//    {
//        cell.lbl_LastMeterReading.text=@"NA";
//    }
//
//    int const_custName;
//    const_custName=20;
//
//    if ([cell.lbl_Status.text caseInsensitiveCompare:@"New"] == NSOrderedSame || [cell.lbl_Status.text isEqualToString:@""])
//    {
//        NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
//        NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
//        NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
//
//        if ([strUserType isEqualToString:@"Customer"])
//        {
//            if ([cell.lbl_Status.text isEqualToString:@""])
//            {
//                cell.lbl_Status.text=@"NA";
//            }
//            else
//            {
//                cell.lbl_Status.text=@"Pending";
//            }
//        }
//        else
//        {
//            if ([cell.lbl_Status.text isEqualToString:@""])
//            {
//                cell.lbl_Status.text=@"NA";
//            }
//        }
////        [cell.lbl_PumpedAmount setHidden:YES];
////        [cell.lbl_PumpedAmountValue setHidden:YES];
//        [cell.lbl_Reason setHidden:YES];
//        [cell.lbl_ReasonValue setHidden:YES];
//        [cell.const_PumpedAmount_H setConstant:0];
//        [cell.lbl_Comment setHidden:YES];
//        [cell.lbl_CommentValue setHidden:YES];
//
//        cell.const_PumpedAmount_H.constant=0;
//        cell.const_ResetReason_H.constant=0;
//        cell.const_MeterNickName_Top.constant=-15;
//        cell.const_MeterNickNameValue_Top.constant=-15;
//        cell.verticalSpaceLabelType.constant = 1;
//        cell.const_h_status.constant = 0;
//        cell.const_h_reason.constant = 0;
//       //  cell.verticalConstDateAndMeterSerialNo.constant = 1;
//        const_custName=0;
//
//    }
//    else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Approved"] == NSOrderedSame)
//    {
//        [cell.lbl_PumpedAmount setHidden:NO];
//        [cell.lbl_PumpedAmountValue setHidden:NO];
//        [cell.lbl_Reason setHidden:YES];
//        [cell.lbl_ReasonValue setHidden:YES];
//        [cell.lbl_Comment setHidden:YES];
//        [cell.lbl_CommentValue setHidden:YES];
//        cell.const_ResetReason_H.constant=0;
//        cell.const_PumpedAmount_H.constant=21;
//        const_custName=0;
//        cell.const_MeterNickName_Top.constant=6;
//        cell.const_MeterNickNameValue_Top.constant=6;
//        cell.verticalSpaceLabelType.constant = 1;
//        cell.verticalConstDateAndMeterSerialNo.constant = 45;
//    }
//    else if ([cell.lbl_Status.text caseInsensitiveCompare:@"In Progress"] == NSOrderedSame)
//    {
//
//        [cell.lbl_PumpedAmount setHidden:YES];
//        [cell.lbl_PumpedAmountValue setHidden:YES];
//        [cell.lbl_Reason setHidden:YES];
//        [cell.lbl_ReasonValue setHidden:YES];
//        [cell.const_PumpedAmount_H setConstant:0];
//        cell.const_PumpedAmount_H.constant=0;
//        cell.const_ResetReason_H.constant=0;
//        const_custName=0;
//        cell.const_MeterNickName_Top.constant=-15;
//        cell.const_MeterNickNameValue_Top.constant=-15;
//        cell.verticalSpaceLabelType.constant = 1;
//
//    }
////    else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Approved with edit"] == NSOrderedSame)
////    {
////        //[cell.lbl_Status.text caseInsensitiveCompare:@"New"] == NSOrderedSame
////        [cell.lbl_PumpedAmount setHidden:NO];
////        [cell.lbl_PumpedAmountValue setHidden:NO];
////        [cell.lbl_Reason setHidden:NO];
////        [cell.lbl_ReasonValue setHidden:NO];
////        [cell.const_PumpedAmount_H setConstant:21];
////        cell.const_ResetReason_H.constant=23;
////        const_custName=20;
////        cell.const_MeterNickName_Top.constant=6;
////        cell.const_MeterNickNameValue_Top.constant=6;
////        cell.verticalSpaceLabelType.constant = 67;
////
////    }
////    else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Rejected"] == NSOrderedSame)
////    {
////        // akshay commented coz cell were overlaping I just commented and things go well,17 Aug 2017 Thursday
//////        [cell.lbl_PumpedAmount setHidden:YES];
//////        [cell.lbl_PumpedAmountValue setHidden:YES];
//////        [cell.lbl_Reason setHidden:NO];
//////        [cell.lbl_ReasonValue setHidden:NO];
//////        [cell.const_PumpedAmount_H setConstant:0];
//////        cell.const_ResetReason_H.constant=23;
//////        const_custName=20;
//////        cell.const_MeterNickName_Top.constant=-15;
//////        cell.const_MeterNickNameValue_Top.constant=-15;
//////        cell.verticalSpaceLabelType.constant = 87;
////    }
////    else
////    {
////        [cell.lbl_PumpedAmount setHidden:NO];
////        [cell.lbl_PumpedAmountValue setHidden:NO];
////        [cell.lbl_Reason setHidden:YES];
////        [cell.lbl_ReasonValue setHidden:YES];
////        cell.const_ResetReason_H.constant=23;
////        cell.const_PumpedAmount_H.constant=21;
////        const_custName=20;
////        cell.const_MeterNickName_Top.constant=6;
////        cell.const_MeterNickNameValue_Top.constant=6;
////        cell.verticalSpaceLabelType.constant = 1;
////
////    }
//
//    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
//    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
//    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
//
//    if ([strUserType isEqualToString:@"Customer"])
//    {
//        [cell.lbl_CustomerName setHidden:YES];
//        [cell.lbl_CustName_Value setHidden:YES];
//         cell.const_CustomerName_H.constant=0;
//    }
//    else
//    {
//        [cell.lbl_CustomerName setHidden:NO];
//        [cell.lbl_CustName_Value setHidden:NO];
//         cell.const_CustomerName_Top.constant=const_custName;
//         cell.const_CustomerName_H.constant=21;
//    }
//
//    if ([cell.lbl_Status.text isEqualToString:@"Reject"])
//    {
//        cell.labelCommentTitle.hidden = NO;
//        cell.labelComment.hidden = NO;
//        cell.lbl_Status.text=@"Rejected";
//        cell.labelComment.text = [[arrOfHistory objectAtIndex:indexPath.row] valueForKey:@"Comment"];
//        cell.verticalSpaceLabelType.constant = 67;
//        cell.const_h_status.constant = 21;
//        cell.const_h_reason.constant = 21;
//
//    }
//    else if ([cell.lbl_Status.text isEqualToString:@"Approved With Edit"])
//    {
//        cell.labelCommentTitle.hidden = NO;
//        cell.labelComment.hidden = NO;
//        cell.lbl_Status.text=@"Approved With Edit";
//        cell.labelComment.text = [[arrOfHistory objectAtIndex:indexPath.row] valueForKey:@"Comment"];
//        cell.labelComment.textAlignment = NSTextAlignmentJustified;
//        cell.verticalSpaceLabelType.constant = 67;
//        cell.const_h_status.constant = 21;
//        cell.const_h_reason.constant = 21;
//    }
//    else
//    {
//        cell.labelCommentTitle.hidden = YES;
//        cell.labelComment.hidden = YES;
//        cell.verticalSpaceLabelType.constant = 1;
//    }
//
//
//    //http://pcrms.pestream.com/Documents/_20161109133414155.png
//
//    NSString *strUrlImage=[NSString stringWithFormat:@"%@%@%@",MainUrlImage,UrldownloadImages,[dictData valueForKey:@"Images"]];
//
//    [cell.meterImgView setImageWithURL:[NSURL URLWithString:strUrlImage] placeholderImage:[UIImage imageNamed:@"NoImage.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//
//}
- (void)configureCellSlideMenuTableViewCell:(SlideMenuTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    cell.imgView.image=[UIImage imageNamed:arrOfImgSlideMenu[indexPath.row]];
    cell.lblName.text=arrNameSlideMenu[indexPath.row];
    
}

//============================================================================
#pragma mark- -------------------------METHODS--------------------------------
//============================================================================

-(void)goToReportMeterReading{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ReportMeterReadingViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToAddMeter{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    AddMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMeterViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToManageMeter{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ManageMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ManageMeterViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToDisclaimer{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    DisclaimerViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DisclaimerViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToHelp{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HelpViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goTermsConditions{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    TermsConditionViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TermsConditionViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)goToOutbox{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    OutboxViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"OutboxViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)logOut{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    LoginViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToHelpView{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HelpViewGlobalViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewGlobalViewController"];
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_viewwSideMenu removeFromSuperview];
    [_hiddenView removeFromSuperview];
    
}

-(void)getHistoryDetails :(NSString*)authId :(NSString*)customerId{
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    NSString *currentDate = [formatter stringFromDate:[NSDate date]];
    
    NSArray *objects = [NSArray arrayWithObjects:
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"01/01/1990",
                        currentDate,
                        customerId,
                        authId,
                        @"0",
                        @"",
                        @"",nil];
    
    NSArray *keys = [NSArray arrayWithObjects:
                     @"FirstName",
                     @"LastName",
                     @"EmailID",
                     @"PhoneNo",
                     @"SubmittedBy",
                     @"SubmittedDateFrom",
                     @"SubmittedDateTo",
                     @"CustomerId",
                     @"AuthId",
                     @"MeterId",
                     @"ReadindId",
                     @"Status",nil];
    
    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Get History List JSON: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlGetHistoryList];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading History..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"ManageMeterAPI" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     [self afterServerResponseHistory:response];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
}

-(void)afterServerResponseHistory :(NSDictionary*)dictResponse{
    
    
    arrOfHistory=(NSArray*)dictResponse;
    
    if (arrOfHistory.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailable
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        [self saveToCoreDataHistoryy:arrOfHistory];
        
        // [_tblViewHistory reloadData];
        
    }
}
//============================================================================
//============================================================================
#pragma mark- Advance Search Method
//============================================================================
//============================================================================

-(void)advanceSearchButton{
    
    UIView *view_AddTask=[[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-80,[UIScreen mainScreen].bounds.size.height-120,60,60)];
    view_AddTask.layer.cornerRadius = view_AddTask.frame.size.width/2;
    view_AddTask.clipsToBounds = YES;
    view_AddTask.backgroundColor=[UIColor colorWithRed:69/255.0f green:88/255.0f blue:103/255.0f alpha:1];
    [self.view addSubview:view_AddTask];
    
    UIButton *buttonAddTask=[[UIButton alloc]initWithFrame:CGRectMake(13, 12, 35, 35)];
    [buttonAddTask addTarget:self action:@selector(goToAddTask:) forControlEvents:UIControlEventTouchDown];
    [buttonAddTask setImage:[UIImage imageNamed:@"advanceFilter.png"] forState:UIControlStateNormal];
    [view_AddTask addSubview:buttonAddTask];
    
}
-(void)goToAddTask:(id)sender
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Enter Details To Search"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Meter Number";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Customer Name";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    
    //    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    //        textField.placeholder = @"Email Address";
    //        textField.textColor = [UIColor blueColor];
    //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    //        textField.borderStyle = UITextBorderStyleRoundedRect;
    //        textField.keyboardType=UIKeyboardTypeDefault;
    //    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Search" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtMeterNo = textfields[0];
        UITextField * txtNamee = textfields[1];
        // UITextField * txtEmail = textfields[2];
        NSLog(@"Meter No--%@------Name----%@",txtMeterNo.text,txtNamee.text);
        
        [self filterThrough:txtMeterNo.text :txtNamee.text :@""];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        yesFiltered=NO;
        [_tblViewHistory reloadData];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)filterThrough :(NSString*)strMeterNo :(NSString*)strName :(NSString*)strEmail{
    
    yesFiltered=YES;
    
    filteredArray=[[NSMutableArray alloc]init];
    
    for(int i=0;i<[arrOfHistory count];i++){
        
        NSString *str11;
        NSString *str22;
        // NSString *str33;
        
        NSDictionary *dictdata=arrOfHistory[i];
        str11=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNumber"]];
        str22=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterName"]];
        // str33=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"LastMeterReading"]];
        
        if (strMeterNo.length==0) {
            strMeterNo=@"#`%&*";
        }
        if (strName.length==0) {
            strName=@"#`%&*";
        }
        if (strEmail.length==0) {
            strEmail=@"#`%&*";
        }
        
        NSRange strRange1;
        if ([strMeterNo isEqualToString:@"#`%&*"]) {
            
            strRange1=[@"#`%&*" rangeOfString:strMeterNo options:NSCaseInsensitiveSearch];
            
        } else {
            
            strRange1=[str11 rangeOfString:strMeterNo options:NSCaseInsensitiveSearch];
            
        }
        
        NSRange strRange2;
        if ([strName isEqualToString:@"#`%&*"]) {
            
            strRange2=[@"#`%&*" rangeOfString:strName options:NSCaseInsensitiveSearch];
            
        } else {
            
            strRange2=[str22 rangeOfString:strName options:NSCaseInsensitiveSearch];
            
        }
        
        //        NSRange strRange3;
        //        if ([strEmail isEqualToString:@"#`%&*"]) {
        //
        //            strRange3=[@"#`%&*" rangeOfString:[NSString stringWithFormat:@"%@",strEmail] options:NSCaseInsensitiveSearch];
        //
        //        } else {
        //
        //            strRange3=[str33 rangeOfString:[NSString stringWithFormat:@"%@",strEmail] options:NSCaseInsensitiveSearch];
        //
        //        }
        
        if(strRange1.location!=NSNotFound&&strRange2.location!=NSNotFound)
        {
            [filteredArray addObject:dictdata];
        }
    }
    
    [_tblViewHistory setHidden:NO];
    [_tblViewHistory reloadData];
    
}

//============================================================================
//============================================================================
#pragma mark- SEARCH BAR DELEGATE METHODS
//============================================================================
//============================================================================

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length==0){
        yesFiltered=NO;
        [_tblViewHistory reloadData];
    }else
    {
        yesFiltered=YES;
        filteredArray=[[NSMutableArray alloc]init];
        
        for(int i=0;i<[arrOfHistory count];i++){
            
            
            NSDictionary *dict=[arrOfHistory objectAtIndex:i];
            NSString *str11=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterNumber"]];
            NSString *str111=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterName"]];
            NSString *str22=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CombinedName"]];
            NSString *str33=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterName"]];
            NSRange strRange1=[str11 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange strRange2=[str22 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange strRange3=[str33 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange strRange4=[str111 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(strRange1.location!=NSNotFound||strRange2.location!=NSNotFound||strRange3.location!=NSNotFound||strRange4.location!=NSNotFound)
            {
                [filteredArray addObject:dict];
            }
        }
    }
    [_tblViewHistory reloadData];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *) bar
{
    UITextField *searchBarTextField = nil;
    NSArray *views = ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f) ? bar.subviews : [[bar.subviews objectAtIndex:0] subviews];
    for (UIView *subview in views)
    {
        if ([subview isKindOfClass:[UITextField class]])
        {
            searchBarTextField = (UITextField *)subview;
            //break;
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
}
-(void)saveToCoreDataHistoryy :(NSArray*)arrOfHistoryList{
    
    //==================================================================================
    //==================================================================================
    
    [self deleteHistoryListFromDB];
    
    //==================================================================================
    //==================================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextHistoryList = [appDelegate managedObjectContext];
    entityHistoryList=[NSEntityDescription entityForName:@"HistoryListt" inManagedObjectContext:contextHistoryList];
    
    //==================================================================================
    //==  ================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    HistoryListt *objHistoryList = [[HistoryListt alloc]initWithEntity:entityHistoryList insertIntoManagedObjectContext:contextHistoryList];
    objHistoryList.userName=[dictLoginDetail valueForKey:@"UserName"];
    objHistoryList.userType=@"";
    objHistoryList.arrOfHistoryListSaved=arrOfHistoryList;
    NSError *error1;
    [contextHistoryList save:&error1];
    
    [self fetchFromCoreDataHistoryList];
    //==================================================================================
    //==================================================================================
}

-(void)deleteHistoryListFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextHistoryList = [appDelegate managedObjectContext];
    entityHistoryList=[NSEntityDescription entityForName:@"HistoryListt" inManagedObjectContext:contextHistoryList];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"HistoryListt" inManagedObjectContext:contextHistoryList]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [contextHistoryList executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [contextHistoryList deleteObject:data];
        }
        NSError *saveError = nil;
        [contextHistoryList save:&saveError];
    }
    
}

-(void)fetchFromCoreDataHistoryList{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextHistoryList = [appDelegate managedObjectContext];
    entityHistoryList=[NSEntityDescription entityForName:@"HistoryListt" inManagedObjectContext:contextHistoryList];
    requestHistoryList = [[NSFetchRequest alloc] init];
    [requestHistoryList setEntity:entityHistoryList];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestHistoryList setPredicate:predicate];
    
    sortDescriptorHistoryList = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsHistoryList = [NSArray arrayWithObject:sortDescriptorHistoryList];
    
    [requestHistoryList setSortDescriptors:sortDescriptorsHistoryList];
    
    self.fetchedResultsControllerHistoryList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestHistoryList managedObjectContext:contextHistoryList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerHistoryList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerHistoryList performFetch:&error];
    arrAllObjHistoryList = [self.fetchedResultsControllerHistoryList fetchedObjects];
    if ([arrAllObjHistoryList count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailable;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        matchesHistoryList = arrAllObjHistoryList[0];
        arrOfHistory=[matchesHistoryList valueForKey:@"arrOfHistoryListSaved"];
        [_tblViewHistory reloadData];
        self.tblViewHeight.constant = self.tblViewHistory.contentSize.height;
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

@end
