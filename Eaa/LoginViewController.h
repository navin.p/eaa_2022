//
//  LoginViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 27/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
- (IBAction)action_SignIn:(id)sender;
- (IBAction)action_SignUp:(id)sender;
- (IBAction)hideKeyBoard:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_UserName;
@property (strong, nonatomic) IBOutlet UITextField *txt_Password;
@property (strong, nonatomic) IBOutlet UIButton *btnRememberMe;
- (IBAction)action_RememberMe:(id)sender;
- (IBAction)action_ForgotPass:(id)sender;

@end
