//
//  LoginViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 27/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "LoginViewController.h"
#import "DashBoardViewController.h"
#import "SignUpViewController.h"
#import "ForgotPassViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "TechDashBoardViewController.h"
#import "New_DashBoardVC.h"
@interface LoginViewController ()
{
    Global *global;
    UIAlertController *loaderAlert;

}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        
    }
    [self.navigationController.navigationBar setHidden:YES];
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL isRemembered=[defs boolForKey:@"RememberMe"];
    
    if (isRemembered) {
        
        [_btnRememberMe setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            //            NSString *strTitle = Alert;
            //            NSString *strMsg = ErrorInternetMsg;
            //            [global AlertMethod:strTitle :strMsg];
            //            [DejalBezelActivityView removeView];
            
            NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictResponse=[defsLoginData valueForKey:@"LoginData"];
            
            if([[dictResponse valueForKey:@"IsAMR"] boolValue] == true || [[dictResponse valueForKey:@"IsAMR"] intValue]==1)// akshay 10 Jan 2019
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                New_DashBoardVC
                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"New_DashBoardVC"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
            else
            {
                
                NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
                
                if ([strUserType isEqualToString:@"Customer"]) {
                    
                    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                    [defsLoginData setBool:YES forKey:@"isCustomer"];
                    [defsLoginData synchronize];
                    
                    [self goToDashBoard];
                    
                } else {
                    
                    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                    [defsLoginData setBool:NO forKey:@"isCustomer"];
                    [defsLoginData synchronize];
                    
                    [self goToTechDashBoard];
                    
                }
            }
            
        }
        else
        {
            _txt_UserName.text=[defs valueForKey:@"EmailOrUserName"];
            _txt_Password.text=[defs valueForKey:@"Password"];
            
           // [self testConfigMethod];
        }
        
    } else {
        
        [_btnRememberMe setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
    }
    
    //============================================================================
    //============================================================================
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)action_SignIn:(id)sender {
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorInternetMsg;
        [global AlertMethod:strTitle :strMsg];
      //  [DejalBezelActivityView removeView];
    }
    else
    {
        [self testConfigMethod];
    }
}
- (IBAction)action_SignUp:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SignUpViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
- (IBAction)hideKeyBoard:(id)sender{
    
    [self resignFirstResponder];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_txt_Password resignFirstResponder];
    [_txt_UserName resignFirstResponder];
    
}
- (IBAction)action_RememberMe:(id)sender {
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"uncheck.png"];
    
    UIImage *img = [_btnRememberMe imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        [_btnRememberMe setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
    }
    else{
        
        [_btnRememberMe setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
    }
    
}

- (IBAction)action_ForgotPass:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ForgotPassViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ForgotPassViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

//============================================================================
//============================================================================
#pragma mark--- Methods
//============================================================================
//============================================================================
-(NSString *)validationCheck
{
    NSString *errorMessage;
    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if (_txt_UserName.text.length==0)
    {
        errorMessage = @"Please enter email address";
    }
    /*else if (![emailPredicate evaluateWithObject:_txt_UserName.text])
     {
     errorMessage = @"Please enter valid email id only";
     }*/
    else if (_txt_Password.text.length==0)
    {
        errorMessage = @"Please enter password";
    }
    return errorMessage;
}

-(void)LoginMethod
{
    NSString *errorMessage=[self validationCheck];
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
    } else {
        NSArray *objects = [NSArray arrayWithObjects:
                            [_txt_UserName.text stringByReplacingOccurrencesOfString:@" " withString:@""],
                            [_txt_Password.text stringByReplacingOccurrencesOfString:@" " withString:@""],nil];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"EmailOrUserName",
                         @"Password",nil];
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:[_txt_UserName.text stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:@"EmailOrUserName"];
        [defs setValue:[_txt_Password.text stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:@"Password"];
        [defs synchronize];
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Login JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlLogin];
        
       // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Logging In..."];
       // loaderAlert = [global loader_ShowObjectiveC:self :@"Logging In..."];

        //============================================================================
        //============================================================================
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrlPostMethod:strUrl :@"LoginAPI" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     [loaderAlert dismissViewControllerAnimated:NO completion:^{
                        // [DejalBezelActivityView removeView];
                         if (success)
                         {
                             [self afterServerResponseOnLogin:response];
                         }
                         else
                         {
                             NSString *strTitle = Alert;
                             NSString *strMsg = Sorry;
                             [global AlertMethod:strTitle :strMsg];
                         }
                     }];
                     
                  
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
    }
}

-(void)testConfigMethod
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlMobileCofigData];
    loaderAlert = [global loader_ShowObjectiveC:self :@"Loding..."];
    //============================================================================
    //============================================================================
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"MobileCofigData" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
//                 [loaderAlert dismissViewControllerAnimated:NO completion:^{    
//                if (success){
//                     [self afterServerResponseMobileCofigData:response];
//                 }else{
//                     NSString *strTitle = Alert;
//                     NSString *strMsg = Sorry;
//                     [global AlertMethod:strTitle :strMsg];
//                 } }];
                 if (success){
                      [self afterServerResponseMobileCofigData:response];
                  }else{
                      
                      [loaderAlert dismissViewControllerAnimated:NO completion:^{
                          NSString *strTitle = Alert;
                          NSString *strMsg = Sorry;
                          [global AlertMethod:strTitle :strMsg];
                        }];
                  }
             });
         }];
    });
}

-(void)afterServerResponseMobileCofigData :(NSDictionary*)dictResponse{
    NSLog(@"%@", dictResponse);
    [[NSUserDefaults standardUserDefaults] setObject:dictResponse forKey:@"MobileCofigData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self LoginMethod];
}

-(void)afterServerResponseOnLogin :(NSDictionary*)dictResponse{
    if (dictResponse.count==0) {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:Sorry
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        NSArray *error=[dictResponse valueForKey:@"Error"];
        
        if (![error isKindOfClass:[NSArray class]]) {
            
            
            //============================================================================
            //============================================================================
            
            UIImage *imageToCheckFor = [UIImage imageNamed:@"checked.png"];
            UIImage *img = [_btnRememberMe imageForState:UIControlStateNormal];
            NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
            NSData *imgData2 = UIImagePNGRepresentation(img);
            BOOL isCompare =  [imgData1 isEqual:imgData2];
            if(isCompare==true)
            {
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setBool:YES forKey:@"RememberMe"];
                [defs synchronize];
            }
            else{
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setBool:NO forKey:@"RememberMe"];
                [defs synchronize];
            }
            
            
            //============================================================================
            //============================================================================
            
            dictResponse = [self nestedDictionaryByReplacingNullsWithNil:dictResponse];
            
            NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
            [defsLoginData setObject:dictResponse forKey:@"LoginData"];
            [defsLoginData synchronize];
            
            // working for firebase token sending to server
            
            if([[dictResponse valueForKey:@"IsSuccess"] intValue] == 1)
            {
                // call api to send firebase token to server
                [self methodToSendFirebaseTokenOnServer:dictResponse];
            }
            
            if([[dictResponse valueForKey:@"IsAMR"] boolValue] == true || [[dictResponse valueForKey:@"IsAMR"] intValue]==1)// akshay 13 Dec 2018
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                New_DashBoardVC
                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"New_DashBoardVC"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
            else
            {
                
                NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
                
                if ([strUserType isEqualToString:@"Customer"]) {
                    
                    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                    [defsLoginData setBool:YES forKey:@"isCustomer"];
                    [defsLoginData synchronize];
                    
                    [self goToDashBoard];
                    
                } else {
                    
                    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                    [defsLoginData setBool:NO forKey:@"isCustomer"];
                    [defsLoginData synchronize];
                    
                    [self goToTechDashBoard];
                }
            }
            
            
        } else {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert!"
                                       message:@"Invalid login attempt.Please check email address or password."
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            //        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Delete" style:UIAlertActionStyleDefault
            //                                                   handler:^(UIAlertAction * action)
            //                             {
            //
            //                             }];
            //        [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }
}
-(void)sendFirebaseTokenOnServer
{
    
}
-(void)goToDashBoard{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    DashBoardViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToTechDashBoard{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    TechDashBoardViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TechDashBoardViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

-(void)methodToSendFirebaseTokenOnServer:(NSDictionary*)dictUserData
{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *strCurrentDate = [dateFormat stringFromDate:[NSDate date]];
    NSString *strAuthId = [dictUserData valueForKey:@"AuthId"];
    NSString *strFirebaseToken = [userDefaults valueForKey:@"FirebaseToken"];
    if(!(strFirebaseToken.length>0))
    {
        strFirebaseToken = @"";
    }
    NSDictionary *dictJson = @{
                               @"AuthId":strAuthId,
                               @"OSID":@"1",
                               @"CreatedBy":strAuthId,
                               @"CreatedDate":strCurrentDate,
                               @"ModifyBy":strAuthId,
                               @"ModifyDate":strCurrentDate,
                               @"IMEI":@"",
                               @"TokenID":strFirebaseToken
                               };
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            [self sendingJsonToServer:jsonString];
        }
    }
    
}
-(void)sendingJsonToServer:(NSString*)jsonString
{
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlInsertUpdateNotifDevice];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"" :requestData withCallback:^(BOOL success, id response, NSError *error)
         {
            // [DejalBezelActivityView removeView];
             NSLog(@"%@",response);
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 if (success)
                 {
                     NSLog(@"Firebase token sent successfully");
                 }
                 else
                 {
//[DejalBezelActivityView removeView];
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}
@end
