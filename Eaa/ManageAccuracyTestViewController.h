//
//  ManageAccuracyTestViewController.h
//  Eaa
//
//  Created by Akshay Hastekar on 8/10/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ManageAccuracyTest+CoreDataProperties.h"
@interface ManageAccuracyTestViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableViewManageAccuracyTest;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerManageMeterList;
@end
