//
//  ManageAccuracyTestViewController.m
//  Eaa
//
//  Created by Akshay Hastekar on 8/10/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "ManageAccuracyTestViewController.h"
#import "Header.h"
#import "Global.h"
#import "DejalActivityView.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface ManageAccuracyTestCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewManageAccuracy;
@property (weak, nonatomic) IBOutlet UILabel *labelNickname;

@property (weak, nonatomic) IBOutlet UILabel *labelMeterSerialNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;

@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UILabel *labelReason;
@property (weak, nonatomic) IBOutlet UILabel *lblReasonTitle;

@end

@implementation ManageAccuracyTestCell

@end

@interface ManageAccuracyTestViewController ()

@end

@implementation ManageAccuracyTestViewController
{
    NSString *strCurrentDate;
    Global *global;
    NSArray *arrayManageAccuracyTest;
    AppDelegate *appDelegate;
    NSEntityDescription *entityManageAccuracyTest;
    NSManagedObjectContext *contextManageAccuracyTest;
    NSFetchRequest *requestManageAccuracyTest;
    NSSortDescriptor *sortDescriptorManageAccuracyTest;
    NSArray *arraySortDescriptorsManageAccuracyTest;
    NSManagedObject *matchesManageAccuracyTest;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    global = [[Global alloc]init];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strCurrentDate = [dateFormat stringFromDate:[NSDate date]] ;
    
    // check internet connection
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
   
    // get manage accuracy test list
    
    if (netStatusWify1== NotReachable)
    {
        [self fetchFromCoreDataManageAccuracyTestList];
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please Wait..."];
        [self performSelector:@selector(getManageAccuracyTestList) withObject:self afterDelay:0.2];
    }

}
-(void)fetchFromCoreDataManageAccuracyTestList
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageAccuracyTest = [appDelegate managedObjectContext];
    entityManageAccuracyTest=[NSEntityDescription entityForName:@"ManageAccuracyTest" inManagedObjectContext:contextManageAccuracyTest];
    requestManageAccuracyTest = [[NSFetchRequest alloc] init];
    [requestManageAccuracyTest setEntity:entityManageAccuracyTest];
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestManageAccuracyTest setPredicate:predicate];
    
    sortDescriptorManageAccuracyTest = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    arraySortDescriptorsManageAccuracyTest = [NSArray arrayWithObject:sortDescriptorManageAccuracyTest];
    
    [requestManageAccuracyTest setSortDescriptors:arraySortDescriptorsManageAccuracyTest];
    
    self.fetchedResultsControllerManageMeterList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestManageAccuracyTest managedObjectContext:contextManageAccuracyTest sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerManageMeterList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerManageMeterList performFetch:&error];
   
     arrayManageAccuracyTest = [self.fetchedResultsControllerManageMeterList fetchedObjects];
    
    if ([arrayManageAccuracyTest count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailable;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        matchesManageAccuracyTest = arrayManageAccuracyTest[0];
        arrayManageAccuracyTest=[matchesManageAccuracyTest valueForKey:@"arrayOfManageAccuracyTest"];
        [_tableViewManageAccuracyTest reloadData];
        
    }
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else
    {
    }
    
}

-(void)saveManageAccuracyTestToCoreData :(NSArray*)arrOfManageMeterList
{
    [self deleteManageAccuracyTestFromDB];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageAccuracyTest = [appDelegate managedObjectContext];
    entityManageAccuracyTest=[NSEntityDescription entityForName:@"ManageAccuracyTest" inManagedObjectContext:contextManageAccuracyTest];
    
    //==================================================================================
    //==  ================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    ManageAccuracyTest *objManageMeterList = [[ManageAccuracyTest alloc]initWithEntity:entityManageAccuracyTest insertIntoManagedObjectContext:contextManageAccuracyTest];
    objManageMeterList.userName=[dictLoginDetail valueForKey:@"UserName"];
    objManageMeterList.userType=@"";
    objManageMeterList.arrayOfManageAccuracyTest=arrayManageAccuracyTest;
    NSError *error1;
    [contextManageAccuracyTest save:&error1];
    
    [self fetchFromCoreDataManageAccuracyTestList];
    //==================================================================================
    //==================================================================================
}

-(void)deleteManageAccuracyTestFromDB
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageAccuracyTest = [appDelegate managedObjectContext];
    entityManageAccuracyTest=[NSEntityDescription entityForName:@"ManageAccuracyTest" inManagedObjectContext:contextManageAccuracyTest];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ManageAccuracyTest" inManagedObjectContext:contextManageAccuracyTest]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [contextManageAccuracyTest executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [contextManageAccuracyTest deleteObject:data];
        }
        NSError *saveError = nil;
        [contextManageAccuracyTest save:&saveError];
    }
    
}

- (IBAction)actionOnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView delegate and data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayManageAccuracyTest.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ManageAccuracyTestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ManageAccuracyTestCell"];
    cell.labelNickname.text = [[arrayManageAccuracyTest objectAtIndex:indexPath.row] valueForKey:@"SubmittedBy"];
    cell.labelMeterSerialNumber.text = [[arrayManageAccuracyTest objectAtIndex:indexPath.row] valueForKey:@"MeterNumber"];
    cell.labelStatus.text = [[arrayManageAccuracyTest objectAtIndex:indexPath.row] valueForKey:@"Status"];
    cell.labelDate.text = [[arrayManageAccuracyTest objectAtIndex:indexPath.row] valueForKey:@"TestDate"];
 
    if([[[arrayManageAccuracyTest objectAtIndex:indexPath.row] valueForKey:@"Status"] isEqualToString:@"Rejected"])
    {
        cell.labelReason.hidden = NO;
        cell.lblReasonTitle.hidden = NO;
        cell.labelReason.text = [[arrayManageAccuracyTest objectAtIndex:indexPath.row] valueForKey:@"DeclineReason"];
    }
    else
    {
        cell.labelReason.hidden = YES;
        cell.lblReasonTitle.hidden = YES;
    }
    
    NSString *strUrlImage=[NSString stringWithFormat:@"%@%@%@",MainUrlImageTesting,UrldownloadImages,[[arrayManageAccuracyTest objectAtIndex:indexPath.row] valueForKey:@"Images"]];
    
    [cell.imageViewManageAccuracy setImageWithURL:[NSURL URLWithString:strUrlImage] placeholderImage:[UIImage imageNamed:@"NoImage.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    
    return cell;
}

-(void)getManageAccuracyTestList
{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginResponse=[userDefaults objectForKey:@"LoginData"];
    
    NSDictionary *dictJson = @{@"AccuracyTestID":@"0",
                               @"AuthId":[dictLoginResponse valueForKey:@"AuthId"],
                               @"CustomerId":[dictLoginResponse valueForKey:@"UserId"],
                               @"MeterId":@"0",
                               @"SubmittedDateFrom":@"01/01/1990",
                               @"SubmittedDateTo":strCurrentDate,
                               @"TestDateFrom":@"01/01/1990",
                               @"TestDateTo":strCurrentDate
                               };
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Add Meter Reading JSON: %@", jsonString);
            [self sendingJsonToServer:jsonString];
        }
    }
}

-(void)sendingJsonToServer:(NSString*)jsonString
{
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlGetAccuracyTests];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"" :requestData withCallback:^(BOOL success, id response, NSError *error)
         {
             [DejalBezelActivityView removeView];
             NSLog(@"%@",response);
             dispatch_async(dispatch_get_main_queue(), ^{
                
                 if (success)
                 {
                     arrayManageAccuracyTest = (NSArray*)response;
                     [_tableViewManageAccuracyTest reloadData];
                     [self saveManageAccuracyTestToCoreData:arrayManageAccuracyTest];
                 }
                 else
                 {
                     [DejalBezelActivityView removeView];
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}
@end
