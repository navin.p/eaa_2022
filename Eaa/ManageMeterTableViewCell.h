//
//  ManageMeterTableViewCell.h
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageMeterTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblColorLine;
@property (strong, nonatomic) IBOutlet UILabel *lblMeterSerialNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PermitNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_MeterNickName;
@property (strong, nonatomic) IBOutlet UILabel *lblLastSubmittedReading;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;

@end
