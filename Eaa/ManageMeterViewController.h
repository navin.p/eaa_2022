//
//  ManageMeterViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface ManageMeterViewController : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityManageMeterList;
    NSManagedObjectContext *contextManageMeterList;
    NSFetchRequest *requestManageMeterList;
    NSSortDescriptor *sortDescriptorManageMeterList;
    NSArray *sortDescriptorsManageMeterList;
    NSManagedObject *matchesManageMeterList;
    NSArray *arrAllObjManageMeterList;
}

- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblManageMeter;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerManageMeterList;

@end
