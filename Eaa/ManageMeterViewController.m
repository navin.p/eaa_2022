//
//  ManageMeterViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ManageMeterViewController.h"
#import "ManageMeterTableViewCell.h"
#import "DashBoardViewController.h"
#import "AddMeterViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "ManageMeterList+CoreDataProperties.h"
#import "TechDashBoardViewController.h"
#import "HelpViewController.h"
@interface ManageMeterViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    Global *global;
    NSArray *arrManageMeterList;
}

@end

@implementation ManageMeterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [self fetchFromCoreDataManageMeterList];
        
//        NSString *strTitle = Alert;
//        NSString *strMsg = ErrorInternetMsg;
//        [global AlertMethod:strTitle :strMsg];
//        [DejalBezelActivityView removeView];
    }
    else
    {

    [self FetchManageMeterList];
        
    }
    
    _tblManageMeter.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrManageMeterList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ManageMeterTableViewCell *cell = (ManageMeterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ManageMeterTableViewCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
    
}
- (void)configureCell:(ManageMeterTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    if (indexPath.row%2==0) {
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:161/255.0f green:172/255.0f blue:179/255.0f alpha:1.0];
    }else{
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:204/255.0f green:172/255.0f blue:84/255.0f alpha:1.0];
    }

    NSDictionary *dictData=arrManageMeterList[indexPath.row];
    
    
    cell.lbl_MeterNickName.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterName"]];
    cell.lblMeterSerialNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNumber"]];
    cell.lbl_PermitNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PermitNumber"]];
    cell.lblLastSubmittedReading.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastMeterReading"]];
    cell.lblStatus.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterStatus"]];

    cell.btnDelete.tag=indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(btnDeleteClick:) forControlEvents:UIControlEventTouchDown];

    cell.btnEdit.tag=indexPath.row;
    [cell.btnEdit addTarget:self action:@selector(btnEditClick:) forControlEvents:UIControlEventTouchDown];

    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
//        [cell.btnDelete setHidden:NO];
//        [cell.btnDelete setHidden:NO];
        if([[dictData valueForKey:@"MeterStatus"] isEqualToString:@"Active"])
        {
            cell.btnEdit.hidden  = NO;
            cell.btnDelete.hidden = NO;
        }
        else
        {
            cell.btnEdit.hidden  = YES;
            cell.btnDelete.hidden = YES;
        }

        
    }else{
        
        [cell.btnDelete setHidden:YES];
        [cell.btnDelete setHidden:YES];

    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dictData = arrManageMeterList[indexPath.row];
    
    if([[dictData valueForKey:@"MeterStatus"] isEqualToString:@"Active"])
    {
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"FromManageMeter"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    AddMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMeterViewController"];
    objByProductVC.dictDataManageMeter=arrManageMeterList[indexPath.row];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170.0;
}
/*
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Confirm"
                                   message:@"Are you sure to delete."
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        UIAlertAction* No = [UIAlertAction actionWithTitle:@"Yes-Delete" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  NSDictionary *dictData=arrManageMeterList[indexPath.row];
                                 // [dictData valueForKey:@"MeterId"];
                                  NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                                  NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
                                  [self deleteMeter:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterId"]] :[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"AuthId"]]];
                                  
                              }];
        [alert addAction:No];
        [self presentViewController:alert animated:YES completion:nil];

        
    }
}
*/

#pragma mark - UIButton action
- (IBAction)actionOnHelp:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HelpViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    
    objByProductVC.strHelpMessage = @"This function allows you to Edit (Pencil button) your meter’s Nickname, or to Delete (Trash Can button) a meter from your App.\nWhen you select the Pencil icon (Edit), you can:\n\n1. Confirm the Meter Serial Number.\n\n2. Edit (change) your meter’s Nickname.\n\n3. See your EAA Place of Use (POU) Number (input by EAA Staff).\n\n4. See your EAA Well Number (input by EAA Staff).\n\n5. See your Start of Year Meter Reading (input by EAA Staff).\n\nWhen you select the Trash Can icon (Delete), you can:\n\n1. Delete that Meter from your App view.\n\nDon’t worry, if you delete the meter by mistake, EAA staff can undo your action and replace the meter on your App if you give us a call.";
    
    [self.navigationController pushViewController:objByProductVC animated:NO];
}


-(void)btnDeleteClick:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm"
                               message:@"Are you sure to delete."
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* No = [UIAlertAction actionWithTitle:@"Yes-Delete" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             NSDictionary *dictData=arrManageMeterList[btn.tag];
                             // [dictData valueForKey:@"MeterId"];
                             NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                             NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
                             [self deleteMeter:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterId"]] :[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"AuthId"]]];
                             
                         }];
    [alert addAction:No];
    [self presentViewController:alert animated:YES completion:nil];

}
-(void)btnEditClick:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"FromManageMeter"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    AddMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMeterViewController"];
    objByProductVC.dictDataManageMeter=arrManageMeterList[btn.tag];
    [self.navigationController pushViewController:objByProductVC animated:NO];

    
}
-(void)deleteMeter :(NSString*)meterId :(NSString*)authId{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Deleting..."];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@%@%@%@",MainUrl,UrlDeleteMeter,meterId,UrlDeleteMeterAuthId,authId];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"DeleteMeter" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     
                     [self afterServerResponsedeleteMeter:response];
                     
//                     NSString *strstatuss=[response valueForKey:@"ReturnMsg"];
//                     if ([strstatuss isEqualToString:@"yesDeleted"]) {
//                         
//                         NSString *strTitle = @"Success";
//                         //Meter Deleted Successfully
//                         NSString *strMsg = @"Meter Deleted Successfully";
//                         [global AlertMethod:strTitle :strMsg];
//
//                         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                                  bundle: nil];
//                         ManageMeterViewController
//                         *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ManageMeterViewController"];
//                         [self.navigationController pushViewController:objByProductVC animated:NO];
//
//                         
//                     }else{
//                         
////                         NSString *strTitle = @"Alert";
////                         NSString *strMsg = @"Unable to delete please try again later.";
////                         [global AlertMethod:strTitle :strMsg];
//
//                         NSString *strTitle = @"Success";
//                         NSString *strMsg = @"Meter Deleted Successfully";
//                         [global AlertMethod:strTitle :strMsg];
//                         
//                         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                                  bundle: nil];
//                         ManageMeterViewController
//                         *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ManageMeterViewController"];
//                         [self.navigationController pushViewController:objByProductVC animated:NO];
//
//                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}

-(void)afterServerResponsedeleteMeter :(NSDictionary*)dictResponse{
    
    if (dictResponse.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:Sorry
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        BOOL isSuccess=[[dictResponse valueForKey:@"IsSuccess"] boolValue];
        
        if (isSuccess) {
            
            NSString *strTitle = @"Success";
            //Meter Deleted Successfully
            NSString *strMsg = @"Meter Deleted Successfully";
            [global AlertMethod:strTitle :strMsg];
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            ManageMeterViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ManageMeterViewController"];
            [self.navigationController pushViewController:objByProductVC animated:NO];

        } else {
            
            NSArray *arrOfError=[dictResponse valueForKey:@"Error"];
            if ([arrOfError isKindOfClass:[NSArray class]]) {
                
                if (arrOfError.count==0) {
                    
                    NSString *strTitle = Alert;
                    NSString *strMsg = Sorry;
                    [global AlertMethod:strTitle :strMsg];
                    
                } else {
                    
                    NSString *joinedError = [arrOfError componentsJoinedByString:@","];
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert!"
                                               message:joinedError
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              
                                          }];
                    [alert addAction:yes];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }
                
            } else {
                
                NSString *strTitle = Alert;
                NSString *strMsg = Sorry;
                [global AlertMethod:strTitle :strMsg];
                
            }
            
        }
    }
}

//============================================================================
#pragma mark- Button Action METHODS
//============================================================================

- (IBAction)action_Back:(id)sender {
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k=0; k<arrstack.count; k++) {
            if ([[arrstack objectAtIndex:k] isKindOfClass:[DashBoardViewController class]]) {
                index=k;
                // break;
            }
        }
        DashBoardViewController *myController = (DashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        
    }else{
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k=0; k<arrstack.count; k++) {
            if ([[arrstack objectAtIndex:k] isKindOfClass:[TechDashBoardViewController class]]) {
                index=k;
                // break;
            }
        }
        TechDashBoardViewController *myController = (TechDashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        
    }

}

-(void)goToAddMeter{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    AddMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMeterViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)FetchManageMeterList{
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    if (dictLoginDetail.count==0) {
        
    } else {
        
        NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
        NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
        
        NSString *strCustomerId;
        
        if ([strUserType isEqualToString:@"Customer"]) {
            strCustomerId=[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]];
        }else{
            strCustomerId=@"0";
        }
        
        NSArray *objects = [NSArray arrayWithObjects:
                            strCustomerId,
                            @"0",
                            @"",
                            @"",
                            @"",nil];
        
        NSArray *keys = [NSArray arrayWithObjects:
                         @"CustomerId",
                         @"MeterId",
                         @"MeterName",
                         @"MeterNo",
                         @"CustomerName",nil];
    
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        
    
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Get Manage Meter List JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlGetManageMeterList];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Meters..."];
        
        //============================================================================
        //============================================================================
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrlPostMethod:strUrl :@"ManageMeterAPI" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         [self afterServerResponseOnLogin:response];
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
        
    }
        //============================================================================
        //============================================================================
}
-(void)afterServerResponseOnLogin :(NSDictionary*)dictResponse{
    
    arrManageMeterList=(NSArray*)dictResponse;
    
    if (arrManageMeterList.count==0) {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailable
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        [self saveToCoreDataLeads:arrManageMeterList];
        //[_tblManageMeter reloadData];
        
    }
}


-(void)saveToCoreDataLeads :(NSArray*)arrOfManageMeterList
{
    
    //==================================================================================
    //==================================================================================
    
    [self deleteManageMeterListFromDB];
    
    //==================================================================================
    //==================================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList];
    
    //==================================================================================
    //==  ================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    ManageMeterList *objManageMeterList = [[ManageMeterList alloc]initWithEntity:entityManageMeterList insertIntoManagedObjectContext:contextManageMeterList];
    objManageMeterList.userName=[dictLoginDetail valueForKey:@"UserName"];
    objManageMeterList.userType=@"";
    objManageMeterList.arrOfManageMeterList=arrOfManageMeterList;
    NSError *error1;
    [contextManageMeterList save:&error1];
    
    [self fetchFromCoreDataManageMeterList];
    //==================================================================================
    //==================================================================================
}

-(void)deleteManageMeterListFromDB
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [contextManageMeterList executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [contextManageMeterList deleteObject:data];
        }
        NSError *saveError = nil;
        [contextManageMeterList save:&saveError];
    }
    
}

-(void)fetchFromCoreDataManageMeterList
{    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList];
    requestManageMeterList = [[NSFetchRequest alloc] init];
    [requestManageMeterList setEntity:entityManageMeterList];
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestManageMeterList setPredicate:predicate];
    
    sortDescriptorManageMeterList = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsManageMeterList = [NSArray arrayWithObject:sortDescriptorManageMeterList];
    
    [requestManageMeterList setSortDescriptors:sortDescriptorsManageMeterList];
    
    self.fetchedResultsControllerManageMeterList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestManageMeterList managedObjectContext:contextManageMeterList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerManageMeterList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerManageMeterList performFetch:&error];
    arrAllObjManageMeterList = [self.fetchedResultsControllerManageMeterList fetchedObjects];
    
    if ([arrAllObjManageMeterList count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailable;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        matchesManageMeterList = arrAllObjManageMeterList[0];
        arrManageMeterList=[matchesManageMeterList valueForKey:@"arrOfManageMeterList"];
        [_tblManageMeter reloadData];
        
    }
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else
    {
    }
    
}

@end
