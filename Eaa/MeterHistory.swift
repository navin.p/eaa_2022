//
//  MeterHistory.swift
//  Eaa
//
//  Created by Navin Patidar on 11/4/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class MeterHistory: UIViewController {
    
    
    // MARK: - ----------------IBOutlet
    // MARK: -
   
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMeterNumber: UILabel!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblTotal: UILabel!

    // MARK: - --------------Variable
    // MARK: -
    var aryList = NSMutableArray()
   @objc var dictData = NSMutableDictionary()
    var aryMeterData = NSMutableArray()
    var aryPermittedData = NSMutableArray()
    var aryLPWData = NSMutableArray()

    // MARK: - ----------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvlist.tag = 0 //For permitted
        lblTitle.text = "\(dictData.value(forKey: "MeterType")!)"
        lblMeterNumber.text = "Meter Number - \(dictData.value(forKey: "MeterSerialNumber")!)"
        if(aryPeriodDataGloble.count != 0){
             self.callGetGetMeterYearReadingAPI()
        }else{
            callUrlGetReportingPeriodDataAPI()
        }
        self.tvlist.tableFooterView = UIView()
    }
    
    //MARK: IBAction
    //MARK:
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: API Calling
    //MARK:
    func callUrlGetReportingPeriodDataAPI() {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let loader = Global().loader_ShowObjectiveC(self, "Please wait...")
            WebServiceClass.callAPIBYGET(parameter: NSDictionary(), url: UrlGetReportingPeriodData) { (responce, status) in
                loader!.dismiss(animated: false, completion: {
                    if(status){
                        print(responce)
                        aryPeriodDataGloble = NSMutableArray()
                        aryPeriodDataGloble = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                        self.callGetGetMeterYearReadingAPI()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
                
            }
        }
    }
    func callGetGetMeterYearReadingAPI() {
        let meterNumber = "\(dictData.value(forKey: "MeterID")!)"
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let loader = Global().loader_ShowObjectiveC(self, "Please wait...")
            let url = UrlGetGetMeterYearReading + "\(meterNumber)"
            WebServiceClass.callAPIBYGET(parameter: NSDictionary(), url: url) { (responce, status) in
                print(responce)
                
                loader!.dismiss(animated: false, completion: {
                    if(status){
                        
                        self.dataPloatOnPermitted_AND_LPW(dictReading: responce)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
                
                
                
            }
        }
    }
    
    
    func dataPloatOnPermitted_AND_LPW(dictReading: NSDictionary)  {
        var error = ""
        if let val = dictReading["Message"] {
            error = "\(val)"
        }
        if(error == "An error has occurred."){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: error, viewcontrol: self)
        }else{
            self.aryMeterData = NSMutableArray()
            self.aryMeterData = (dictReading.value(forKey: "ReadingDetail")as! NSArray).mutableCopy()as! NSMutableArray
            lblTitle.text = "\(dictData.value(forKey: "MeterType")!)"
            lblMeterNumber.text = "Meter Number - \(dictData.value(forKey: "MeterSerialNumber")!)"
            self.lblTotal.text = "Total - \(0) Acre feet"
            
            var TotalUse = Float()
            if(dictReading.value(forKey: "Master") is NSArray){
                let aryMaster = dictReading.value(forKey: "Master") as! NSArray
                if(aryMaster.count != 0){
                    let strTotalUse = "\((aryMaster.object(at: 0)as! NSDictionary).value(forKey: "TotalUse")!)"
                    if(strTotalUse.count != 0){
                        TotalUse = Float((strTotalUse as NSString).doubleValue)
                        self.lblTotal.text = "Total - \(TotalUse) Acre feet"
                    }else{
                        TotalUse  = 0
                        self.lblTotal.text = "Total - \(TotalUse) Acre feet"
                    }
                }
            }
            
            if("\(dictData.value(forKey: "MeterType")!)" == "Permitted"){
                self.tvlist.tag = 0
                let resultPredicate = NSPredicate(format: "Type contains[c] %@", argumentArray: ["Month"])
                let array = (aryPeriodDataGloble as NSArray).filtered(using: resultPredicate)
                print(array)
                self.aryPermittedData = NSMutableArray()
                self.aryPermittedData = (array as NSArray).mutableCopy()as! NSMutableArray
                let tempPermitted = NSMutableArray()
                for itemPeriodData in aryPermittedData{
                    let strValue = "\((itemPeriodData as AnyObject).value(forKey: "Value")!)"
                    for itemMeterDataData in aryMeterData{
                        let strYearType = "\((itemMeterDataData as AnyObject).value(forKey: "YearValue")!)"
                        let strCurrentMeterReading = "\((itemMeterDataData as AnyObject).value(forKey: "CurrentMeterReading")!)"
                        let strStatus = "\((itemMeterDataData as AnyObject).value(forKey: "Status")!)"
                        let strReadingDetailStartDate = "\((itemMeterDataData as AnyObject).value(forKey: "StartDate")!)"
                        let strReadingDetailEndDate = "\((itemMeterDataData as AnyObject).value(forKey: "EndDate")!)"
                        let strYearValue = "\((itemMeterDataData as AnyObject).value(forKey: "YearValue")!)"
                        let strMonthlyUsageAcre = "\((itemMeterDataData as AnyObject).value(forKey: "MonthlyUsageAcre")!)"
                        
                        if(strValue == strYearType){
                            var dictTemp = NSMutableDictionary()
                            dictTemp = ((itemPeriodData as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            dictTemp.setValue(strCurrentMeterReading, forKey: "CurrentMeterReading")
                            dictTemp.setValue(strStatus, forKey: "Status")
                            dictTemp.setValue(strReadingDetailStartDate, forKey: "ReadingDetailStartDate")
                            dictTemp.setValue(strReadingDetailEndDate, forKey: "ReadingDetailEndDate")
                            dictTemp.setValue(strYearValue, forKey: "YearValue")
                            dictTemp.setValue(strMonthlyUsageAcre, forKey: "MonthlyUsageAcre")
                            
                            tempPermitted.add(dictTemp)
                            break
                        }
                    }
                }
                self.aryPermittedData = NSMutableArray()
                
                let mySortedArray = tempPermitted.sorted(by: {(int1, int2)  -> Bool in
                    return (Int("\((int1 as! NSDictionary).value(forKey: "YearValue") ?? "0")") ?? 0) > (Int("\((int2 as! NSDictionary).value(forKey: "YearValue") ?? "0")") ?? 0) // It sorted the values and return to the mySortedArray
                  })
                
                self.aryPermittedData = (mySortedArray as NSArray).mutableCopy()as! NSMutableArray
                self.tvlist.reloadData()
                
            }
            else{
                self.tvlist.tag = 1
                let resultPredicate = NSPredicate(format: "Type contains[c] %@", argumentArray: ["Quarter"])
                let array = (aryPeriodDataGloble as NSArray).filtered(using: resultPredicate)
                
                print(array)
                
                self.aryLPWData = NSMutableArray()
                self.aryLPWData = (array as NSArray).mutableCopy()as! NSMutableArray
                let tempLPW = NSMutableArray()
                for itemPeriodData in aryLPWData{
                    let strValue = "\((itemPeriodData as AnyObject).value(forKey: "Value")!)"
                    for itemMeterDataData in aryMeterData{
                        let strYearType = "\((itemMeterDataData as AnyObject).value(forKey: "YearValue")!)"
                        let strCurrentMeterReading = "\((itemMeterDataData as AnyObject).value(forKey: "CurrentMeterReading")!)"
                        let strStatus = "\((itemMeterDataData as AnyObject).value(forKey: "Status")!)"
                        let strReadingDetailStartDate = "\((itemMeterDataData as AnyObject).value(forKey: "StartDate")!)"
                        let strReadingDetailEndDate = "\((itemMeterDataData as AnyObject).value(forKey: "EndDate")!)"
                        let strYearValue = "\((itemMeterDataData as AnyObject).value(forKey: "YearValue")!)"
                        let strMonthlyUsageAcre = "\((itemMeterDataData as AnyObject).value(forKey: "MonthlyUsageAcre")!)"
                        
                        if(strValue == strYearType){
                            var dictTemp = NSMutableDictionary()
                            dictTemp = ((itemPeriodData as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            dictTemp.setValue(strCurrentMeterReading, forKey: "CurrentMeterReading")
                            dictTemp.setValue(strStatus, forKey: "Status")
                            dictTemp.setValue(strReadingDetailStartDate, forKey: "ReadingDetailStartDate")
                            dictTemp.setValue(strReadingDetailEndDate, forKey: "ReadingDetailEndDate")
                            dictTemp.setValue(strYearValue, forKey: "YearValue")
                            dictTemp.setValue(strMonthlyUsageAcre, forKey: "MonthlyUsageAcre")
                            
                            tempLPW.add(dictTemp)
                            break
                        }
                    }
                }
                self.aryLPWData = NSMutableArray()
                let mySortedArray = tempLPW.sorted(by: {(int1, int2)  -> Bool in
                    return (Int("\((int1 as! NSDictionary).value(forKey: "YearValue") ?? "0")") ?? 0) > (Int("\((int2 as! NSDictionary).value(forKey: "YearValue") ?? "0")") ?? 0) // It sorted the values and return to the mySortedArray
                  })
                self.aryLPWData = (mySortedArray as NSArray).mutableCopy() as! NSMutableArray
                self.tvlist.reloadData()
            }
            
            
            print("PlatDataONPermitted----------")
        }
      
        
    }
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MeterHistory: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.tag == 0 ? aryPermittedData.count : aryLPWData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "MeterHistoryCell", for: indexPath as IndexPath) as! MeterHistoryCell
        
        var dictData = NSMutableDictionary()
        if(self.tvlist.tag == 0){ // Permitted
            dictData = (self.aryPermittedData.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        }else{
            dictData = (self.aryLPWData.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        }
        cell.lblTitleMonth.text = "\(dictData.value(forKey: "DisplayName")!)"
        cell.lblMeterReading.text = "\(dictData.value(forKey: "CurrentMeterReading")!)"
        cell.lblUseSubmission.text = "\(dictData.value(forKey: "MonthlyUsageAcre")!) Acre feet"

        cell.lblMeterReading.layer.cornerRadius = 5.0
        cell.lblMeterReading.layer.borderColor = UIColor.darkGray.cgColor
        cell.lblMeterReading.layer.borderWidth = 1.0
        cell.lblUseSubmission.layer.cornerRadius = 5.0
        cell.lblUseSubmission.layer.borderColor = UIColor.darkGray.cgColor
        cell.lblUseSubmission.layer.borderWidth = 1.0
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
    }
    

}

// MARK: - ----------------MeterHistoryCell
// MARK: -
class MeterHistoryCell: UITableViewCell {
    @IBOutlet weak var lblTitleMonth: UILabel!
    @IBOutlet weak var lblMeterReading: UILabel!
    @IBOutlet weak var lblUseSubmission: UILabel!

    
    
}
