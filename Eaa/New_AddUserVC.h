//
//  New_AddUserVC.h
//  Eaa
//
//  Created by Navin Patidar on 12/5/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "Header.h"
#import "DejalActivityView.h"
#import "Reachability.h"
NS_ASSUME_NONNULL_BEGIN

@interface New_AddUserVC : UIViewController<UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tv_ForList;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddressName;

- (IBAction)actionOnExistingUser:(id)sender;
- (IBAction)actionOnBack:(id)sender;
- (IBAction)actionOnSubmit:(id)sender;


@end

NS_ASSUME_NONNULL_END
