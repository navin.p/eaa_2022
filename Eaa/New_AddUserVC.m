//
//  New_AddUserVC.m
//  Eaa
//
//  Created by Navin Patidar on 12/5/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "New_AddUserVC.h"
#import "CommonTBLCellVC.h"
#import "New_ExistingUserVC.h"
#import "New_DashBoardVC.h"
@interface New_AddUserVC ()
{
    Global *global;
    NSMutableArray *arrDictResponse,*arrSelectedMeterId,*arrSelectedIdToSend;
}
@end

@implementation New_AddUserVC

#pragma mark-
#pragma mark- Life Cycel
- (void)viewDidLoad {
    [super viewDidLoad];
    global=[[Global alloc]init];

    _tv_ForList.tableFooterView = [UIView new];
    _tv_ForList.estimatedRowHeight = 85.0;
    _txtLastName.delegate = self;
    _txtFirstName.delegate  = self;
    _txtEmailAddressName.delegate = self;
    arrDictResponse=[[NSMutableArray alloc]init];
    arrSelectedMeterId=[[NSMutableArray alloc]init];
    arrSelectedIdToSend=[[NSMutableArray alloc]init];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Details..."];
        dispatch_async(dispatch_get_main_queue(), ^{
            [DejalBezelActivityView removeView];
            [self getMeterListDetailsForNewUser];
            [_tv_ForList reloadData];
        });
        
    }
    /*arrDictResponse=[[NSMutableArray alloc]init];
    NSMutableDictionary *dict1=[[NSMutableDictionary alloc]init];
    [dict1 setObject:@"123" forKey:@"meternumber"];
    [dict1 setObject:@"1" forKey:@"checkbox"];
    
    NSMutableDictionary *dict2=[[NSMutableDictionary alloc]init];
    [dict2 setObject:@"1234" forKey:@"meternumber"];
    [dict2 setObject:@"1" forKey:@"checkbox"];
    
    NSMutableDictionary *dict3=[[NSMutableDictionary alloc]init];
    [dict3 setObject:@"1235" forKey:@"meternumber"];
    [dict3 setObject:@"1" forKey:@"checkbox"];
    
    NSMutableDictionary *dict4=[[NSMutableDictionary alloc]init];
    [dict4 setObject:@"1236" forKey:@"meternumber"];
    [dict4 setObject:@"1" forKey:@"checkbox"];
    
    NSMutableDictionary *dict5=[[NSMutableDictionary alloc]init];
    [dict5 setObject:@"1237" forKey:@"meternumber"];
    [dict5 setObject:@"1" forKey:@"checkbox"];
    
    [arrDictResponse addObject:dict1];
    [arrDictResponse addObject:dict2];
    [arrDictResponse addObject:dict3];
    [arrDictResponse addObject:dict4];
    [arrDictResponse addObject:dict5];
    
    [_tv_ForList reloadData];*/
}
#pragma mark-
#pragma mark- UITEXTFiled Delegate
-(bool)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return  true;
}

#pragma mark-
#pragma mark- TABLE VIEW DELEGATE METHODS

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrDictResponse.count;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommonTBLCellVC *cell = (CommonTBLCellVC *)[tableView dequeueReusableCellWithIdentifier:@"New_UserCell" forIndexPath:indexPath];
    
    NSDictionary *dict=[arrDictResponse objectAtIndex:indexPath.row];
    
    if([NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterNumber"]].length>0)
    {
        cell.lblMeterSerialNumber.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterNumber"]];
    }
    else
    {
        cell.lblMeterSerialNumber.text = @" ";
    }
    
    cell.btnCheckBox.tag = indexPath.row;
    [cell.btnCheckBox addTarget:self action:@selector(checkBoxClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *strMeterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterId"]];
    
    if ([arrSelectedMeterId containsObject:strMeterId])
    {
        [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
    }
    /* NSString *strStatus=[NSString stringWithFormat:@"%@",[dict valueForKey:@"checkbox"]];
     if ([strStatus isEqualToString:@"1"])
     {
     [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
     }
     else
     {
     [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
     }*/
    // cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    
    return cell;
    
}
- (void)checkBoxClicked:(UIButton *)sender
{
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    NSDictionary *dict=[arrDictResponse objectAtIndex:indexPath.row];
    NSString *strMeterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterId"]];
    
    if ([arrSelectedMeterId containsObject:strMeterId])
    {
        [arrSelectedMeterId removeObject:strMeterId];
    }
    else
    {
        [arrSelectedMeterId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterId"]]];
    }
    /*NSString *strStatus=[dict valueForKey:@"checkbox"];
    NSMutableDictionary *dict1=[[NSMutableDictionary alloc]init];
    
    if ([strStatus isEqualToString:@"1"])
    {
        strStatus=@"0";
        [dict1 setObject:[dict valueForKey:@"meternumber"] forKey:@"meternumber"];
        [dict1 setObject:@"0" forKey:@"checkbox"];
        
        
    }
    else
    {
        strStatus=@"1";
        [dict1 setObject:[dict valueForKey:@"meternumber"] forKey:@"meternumber"];
        [dict1 setObject:@"1" forKey:@"checkbox"];
        
    }
    [arrDictResponse replaceObjectAtIndex:indexPath.row withObject:dict1];*/
    [_tv_ForList reloadData];
    
}
- (IBAction)actionOnExistingUser:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    New_ExistingUserVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"New_ExistingUserVC"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
}
- (IBAction)actionOnBack:(id)sender {
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[New_DashBoardVC class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:NO];

            return;
        }
    }
}

- (IBAction)actionOnSubmit:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
        NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        
        if ([_txtFirstName.text length] == 0 ) {
            [global AlertMethod:Alert :@"Enter first name"];
        }
        else if ([_txtLastName.text length] == 0)
        {
            [global AlertMethod:Alert :@"Enter last name"];
        }
        else if ([_txtEmailAddressName.text length] == 0)
        {
            [global AlertMethod:Alert :@"Enter email address"];
        }
        else if (![emailPredicate evaluateWithObject:_txtEmailAddressName.text])
        {
             [global AlertMethod:Alert :@"Please enter valid email id only"];
        }
        else
        {
            arrSelectedIdToSend=[[NSMutableArray alloc]init];
            for (int i=0; i<arrSelectedMeterId.count; i++)
            {
                NSString *strMeterId=[arrSelectedMeterId objectAtIndex:i];
                
                for (int j=0; j<arrDictResponse.count; j++)
                {
                    NSDictionary *dict=[arrDictResponse objectAtIndex:j];
                    
                    if ([strMeterId isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterId"]]])
                    {
                        [arrSelectedIdToSend addObject:strMeterId];
                    }
                }
            }
            if (arrSelectedIdToSend.count==0)
            {
                [DejalBezelActivityView removeView];
                [global AlertMethod:Alert :@"Please select atleast one meter"];
            }
            else
            {
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"please wait..."];
            [self performSelector:@selector(callApiToSubmitDataOnServerForNewUser) withObject:self afterDelay:0.2];
            }
        }
    }
    
}
#pragma mark-  ----------- GETTING METERLIST DETAIL ----------------
-(void)getMeterListDetailsForNewUser
{
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strCustId= [NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]];
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Details..."];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlGetMeterListDetails,strCustId];//138
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        
        dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict=dictData;
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception)
        {
            
        } @finally
        {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
            [DejalBezelActivityView removeView];
            [self afterServerResponseDashBoard:ResponseDict];
        } else {
            ResponseDict=nil;
            // [DejalBezelActivityView removeView];
        }
        
        
        NSLog(@"Response on DashBoard Details = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [global AlertMethod:Alert :Sorry];
        //[DejalBezelActivityView removeView];
    }
    @finally {
    }
    [DejalBezelActivityView removeView];
}

-(void)afterServerResponseDashBoard :(NSDictionary*)dictResponse{
    
    NSArray *arrDashBoardResponse;
    arrDashBoardResponse=(NSArray*)dictResponse;
    
    if (arrDashBoardResponse.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailable
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        /*NSMutableArray *tempArray=[NSMutableArray new];
        for(NSInteger i=0;i<arrDashBoardResponse.count ;i++)
        {
            NSMutableDictionary *dic =[[NSMutableDictionary alloc] initWithDictionary:[arrDashBoardResponse objectAtIndex:i]];
            
            
            NSString *dateFormat = @"MM/dd/yyyy";
            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
            [inputDateFormatter setTimeZone:inputTimeZone];
            [inputDateFormatter setDateFormat:dateFormat];
            
            NSString *inputString = [dic valueForKey:@"SubmittedDate"];
            NSDate *date = [inputDateFormatter dateFromString:inputString];
            
            
            
            NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
            [outputDateFormatter setTimeZone:outputTimeZone];
            [outputDateFormatter setDateFormat:dateFormat];
            NSString *outputString = [outputDateFormatter stringFromDate:date];
            
            NSDate *dateConverted = [inputDateFormatter dateFromString:inputString];
            
            
            [dic setValue:dateConverted forKey:@"SubmittedDate"];
            [tempArray addObject:dic];
        }
        
        
        NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:@"SubmittedDate" ascending:NO];
        NSArray *descriptors=[NSArray arrayWithObject: descriptor];
        NSArray *sortedArray=[tempArray sortedArrayUsingDescriptors:descriptors];
        NSLog(@"%@",sortedArray);*/
        
        [arrDictResponse addObjectsFromArray:arrDashBoardResponse];
    }
}
#pragma mark- -----------SENDING DATA POST API -----------


-(void)callApiToSubmitDataOnServerForNewUser
{
    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
   
    if ([_txtFirstName.text length] == 0 )
    {
        [DejalBezelActivityView removeView];
        [global AlertMethod:Alert :@"Enter first name"];
    }
    else if ([_txtLastName.text length] == 0)
    {
        [DejalBezelActivityView removeView];
        [global AlertMethod:Alert :@"Enter last name"];
    }
    else if ([_txtEmailAddressName.text length] == 0)
    {
        [DejalBezelActivityView removeView];
        [global AlertMethod:Alert :@"Enter email address"];
    }
    else if (![emailPredicate evaluateWithObject:_txtEmailAddressName.text])
    {
        [global AlertMethod:Alert :@"Please enter valid email id only"];
    }
    else if (arrSelectedIdToSend.count==0)
    {
        [DejalBezelActivityView removeView];
        [global AlertMethod:Alert :@"Please select atleast one meter"];
    }
    else
    {
        //NSString *joinedMeterIds = [arrSelectedIdToSend componentsJoinedByString:@","];

       // NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
        //NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
        //NSString* strUserID = [dictResponse valueForKey:@"UserId"];
        NSDictionary *dictData = @{
                                   @"UserName":@"",//158
                                   @"FirstName":_txtFirstName.text,
                                   @"LastName":_txtLastName.text,
                                   @"EmailId":_txtEmailAddressName.text,
                                   @"MeterIds":arrSelectedIdToSend,
                                   @"NewUser":@"true"
                                   };
        
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dictData])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictData options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"JSON: %@", jsonString);
            }
        }
        
        [self sendingFinalJsonForNewUser:jsonString];
        
    }
}
-(void)sendingFinalJsonForNewUser :(NSString*)jsonString
{
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlForSubmitUser];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             
             NSLog(@"%@",response);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [DejalBezelActivityView removeView];
                 if([[response valueForKey:@"IsSuccess"] boolValue]==YES || [[response valueForKey:@"IsSuccess"] intValue]==1)
                 {
                     [global AlertMethod:@"Message" :[[response valueForKey:@"Error"] objectAtIndex:0]];
                     
                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                              bundle: nil];
                     New_DashBoardVC
                     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"New_DashBoardVC"];
                     [self.navigationController pushViewController:objByProductVC animated:NO];
                     
                     //[self.navigationController popViewControllerAnimated:YES];
                 }
                 else
                 {
                     [global AlertMethod:@"Message" :[[response valueForKey:@"Error"] objectAtIndex:0]];
                 }
                // arrSelectedIdToSend=[[NSMutableArray alloc]init];
                 //arrSelectedMeterId=[[NSMutableArray alloc]init];
                 // [_tv_ForList reloadData];
                 
             });
         }];
    });
}
@end
