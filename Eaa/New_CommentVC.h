//
//  New_CommentVC.h
//  Eaa
//
//  Created by Navin Patidar on 12/6/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Global.h"
#import "Header.h"
#import "DejalActivityView.h"

NS_ASSUME_NONNULL_BEGIN

@interface New_CommentVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate,
UIImagePickerControllerDelegate>
- (IBAction)actionONBAck:(id)sender;
- (IBAction)actionOnSubmit:(id)sender;
- (IBAction)actionOnCamera:(id)sender;
- (IBAction)actionOnGallery:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_MeterNumber;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionImage;
    @property (weak, nonatomic)  NSDictionary *dictData;

@end

NS_ASSUME_NONNULL_END
