//
//  New_CommentVC.m
//  Eaa
//
//  Created by Navin Patidar on 12/6/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "New_CommentVC.h"
#import "New_CommonCollectionCell.h"

static int imageUploadCount = 0;

@interface New_CommentVC ()
{
    NSMutableArray *aryImages;
}
@end

@implementation New_CommentVC
{
    Global *global;
    NSString *strMeterImageName;
    NSString *strUserID;
}
#pragma mark-
#pragma mark- Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    _txtComment.layer.cornerRadius = 4.0;
    _txtComment.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    _txtComment.layer.borderWidth = 1.0;
    _txtComment.text=@"";
    aryImages = [NSMutableArray new];
    
    global = [[Global alloc] init];
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    strUserID = [dictResponse valueForKey:@"UserId"];
    strMeterImageName = @"";
    _lbl_MeterNumber.text=[NSString stringWithFormat:@"%@",[_dictData valueForKey:@"AMRSerialNumber"]];
    _txtComment.placeholder=@"Message";
}

#pragma mark-
#pragma mark- IBAction's

- (IBAction)actionONBAck:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)actionOnSubmit:(id)sender
{
    if ([_txtComment.text length] == 0)// || [_txtComment.text isEqualToString:@"Message"])
    {
        [global AlertMethod:Alert :@"Comment required !"];
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"please wait..."];
        [self performSelector:@selector(callApiToSubmitDataOnServer) withObject:self afterDelay:0.2];
    }
}

- (IBAction)actionOnCamera:(id)sender  {
    NSLog(@"takePhoto");
    if (aryImages.count >= 3) {
        [global AlertMethod:Alert :imageThree];

    }else{
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *pickerViewController =[[UIImagePickerController alloc]init];
            pickerViewController.allowsEditing = YES;
            pickerViewController.delegate = self;
            pickerViewController.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:pickerViewController animated:YES completion:nil];
        } else {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Camera is not available" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    

}

- (IBAction)actionOnGallery:(id)sender  {
    NSLog(@"selectImage");
    if (aryImages.count >= 3) {
        [global AlertMethod:Alert :imageThree];
        
    }else{
    UIImagePickerController *pickerViewController = [[UIImagePickerController alloc] init];
    pickerViewController.allowsEditing = YES;
    pickerViewController.delegate = self;
    [pickerViewController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:pickerViewController animated:YES completion:nil];
    }
}
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
 
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    
   NSString* strImageName = [NSString stringWithFormat:@"MeterImgg%@%@.jpg",strDate,strTime];
   
    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    if (image.imageOrientation==UIImageOrientationUp)
    {
        NSLog(@"UIImageOrientationUp");
    }
    else if (image.imageOrientation==UIImageOrientationLeft)
    {
        NSLog(@"UIImageOrientationLeft");
    }
    else if (image.imageOrientation==UIImageOrientationRight)
    {
        NSLog(@"UIImageOrientationRight");
    }
    else if (image.imageOrientation==UIImageOrientationDown)
    {
        NSLog(@"UIImageOrientationDown");
    }
    
    [UIImage imageWithCGImage:[image CGImage]
                        scale:[image scale]
                  orientation: UIImageOrientationUp];
    
    
    [self resizeImage:image :strImageName];
    NSDictionary *dictImage = @{@"imageName": strImageName,
                                @"image": image
                                };
    [aryImages addObject:dictImage];
    
    [self dismissViewControllerAnimated:picker completion:nil];
   
    [_collectionImage reloadData];
    
}

-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = actualHeight/2;
    float maxWidth = actualWidth/2;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

#pragma mark-
#pragma mark- UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return aryImages.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    New_CommonCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewCommentImgCell" forIndexPath:indexPath];
    cell.comment_img.image =  [[aryImages objectAtIndex:indexPath.row] valueForKey:@"image"];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

// akshay 13 Dec 2018

-(void)callApiToSubmitDataOnServer
{
    NSString *strMessage = @"";// = [self validationCheck];
    if(strMessage.length>0)
    {
        [DejalBezelActivityView removeView];
        [global AlertMethod:@"Alert!" :strMessage];
    }
    else
    {
        
        for(NSDictionary *dict in aryImages)
        {
            if(strMeterImageName.length>0)
            {
                 strMeterImageName = [NSString stringWithFormat:@"%@,%@",strMeterImageName,[dict valueForKey:@"imageName"]];
            }
            else
            {
               strMeterImageName = [dict valueForKey:@"imageName"];
            }
        }
        
        NSString *strComment = @"";
        if(_txtComment.text.length>0)
        {
            strComment = _txtComment.text;
        }
        
        NSDictionary *dictData = @{
                                           @"CustomerId":strUserID,//158
                                           @"MeterSerialNumber":[NSString stringWithFormat:@"%@",[_dictData valueForKey:@"AMRSerialNumber"]],
                                           @"Comment":strComment,
                                           @"MeterId":[NSString stringWithFormat:@"%@",[_dictData valueForKey:@"AMRMeterID"]],
                                           @"Images":strMeterImageName,
                                           @"Status":@"Active",
                                           @"Notes":@"",
                                           @"wellId":[NSString stringWithFormat:@"%@",[_dictData valueForKey:@"AMRWellID"]]
                                           };
        
       
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dictData])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictData options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Add Meter Reading JSON: %@", jsonString);
            }
        }
        if(aryImages.count>0)
        {
            for(int i=0 ;i<aryImages.count;i++)
            {
                [self uploadImage:[[aryImages objectAtIndex:i] valueForKey:@"imageName"] :jsonString];
            }
        }
        else
        {
            [self sendingFinalJson:jsonString];
        }
    }
}

-(void)uploadImage :(NSString*)strImageName :(NSString*)jsonString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];
    
    NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",MainUrl,UrlImageUpload];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    NSError *jsonError;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSArray *json = [NSJSONSerialization JSONObjectWithData:returnData
                                                    options:NSJSONReadingMutableContainers
                                                      error:&jsonError];
    
    //NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    //    if ([returnString isEqualToString:@"OK"])
    //    {
    //        NSLog(@"Image Sent");
    //        imageUploadCount++;
    //    }
    //    NSLog(@"Image Sent === %@",returnString);
    
    if([[[json objectAtIndex:0] valueForKey:@"Name"] length]>0)
    {
        imageUploadCount++;
    }
    
    if(imageUploadCount == aryImages.count)// after no.of images uploaded,send json string to server
    {
        imageUploadCount = 0;
        [self sendingFinalJson:jsonString];
    }
}

-(void)sendingFinalJson :(NSString*)jsonString
{
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlInsertMeterRequestComment];
  
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             
             NSLog(@"%@",response);
            
             dispatch_async(dispatch_get_main_queue(), ^{

                 [DejalBezelActivityView removeView];
               if([[response valueForKey:@"IsSuccess"] boolValue]==YES || [[response valueForKey:@"IsSuccess"] intValue]==1)
               {
                    [global AlertMethod:@"Message" :[[response valueForKey:@"Error"] objectAtIndex:0]];
                   [self.navigationController popViewControllerAnimated:YES];
               }
                else
                 {
                     [global AlertMethod:@"Message" :[[response valueForKey:@"Error"] objectAtIndex:0]];
                 }

             });
         }];
    });
}
//-(NSString*)validationCheck
//{
//    NSString *strMessage = @"";
//
//    UIImage *imageToCheckFor = [UIImage imageNamed:@"NoImage.png"];
//
//    UIImage *img = _imageViewNewMeter.image;
//
//    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
//
//    NSData *imgData2 = UIImagePNGRepresentation(img);
//
//    BOOL isCompare =  [imgData1 isEqual:imgData2];
//
//    if(isCompare==true)
//    {
//        strMessage = @"Please Take Picture Of New Meter";
//    }
//    return strMessage;
//}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    if([text isEqualToString:@"\n"])
    {
        [_txtComment resignFirstResponder];
        return NO;
    }
    return textView.text.length + (text.length - range.length) <= 3000;
    
    
    return YES;
}
@end
