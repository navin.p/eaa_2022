//
//  New_CommonCollectionCell.h
//  Eaa
//
//  Created by Navin Patidar on 12/6/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface New_CommonCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *comment_img;

@end

NS_ASSUME_NONNULL_END
