//
//  New_DashBoardVC.h
//  Eaa
//
//  Created by Navin Patidar on 12/5/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "Header.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface New_DashBoardVC : UIViewController<UITableViewDelegate, UITableViewDataSource,NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityDashBoardList;
    NSManagedObjectContext *contextDashBoardList;
    NSFetchRequest *requestDashBoardList;
    NSSortDescriptor *sortDescriptorDashBoardList;
    NSArray *sortDescriptorsDashBoardList;
    NSManagedObject *matchesDashBoardList;
    NSArray *arrAllObjDashBoardList;
    
}
- (IBAction)actionOnMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tv_ForList;
@property (weak, nonatomic) IBOutlet UITableView *tv_ForMenuList;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerDashBoardList;


- (IBAction)actionOnBtnTransprant:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewMenu;
@property (strong, nonatomic) IBOutlet UIButton *btnTransprant;


@end

NS_ASSUME_NONNULL_END
