//
//  New_DashBoardVC.m
//  Eaa
//
//  Created by Navin Patidar on 12/5/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "New_DashBoardVC.h"
#import "CommonTBLCellVC.h"
#import "New_ExistingUserVC.h"
#import "New_MeterReadingHistoryVC.h"
#import "LoginViewController.h"
#import "DashBoardListt+CoreDataProperties.h"
#import "MBProgressHUD.h"

@interface New_DashBoardVC ()

@end

@implementation New_DashBoardVC
{
    Global *global;
    NSArray *arrDashBoardResponse;
}
#pragma mark-
#pragma mark- Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _tv_ForList.tableFooterView = [UIView new];
    _tv_ForList.estimatedRowHeight = 120.0;
    _tv_ForMenuList.tableFooterView = [UIView new];
    _tv_ForMenuList.estimatedRowHeight = 85.0;
    [self.viewMenu setHidden:YES];
    [self.btnTransprant setHidden:YES];
    
    global = [[Global alloc] init];
    
    // [self getDashBoardData];
    
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        //        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        //        [DejalActivityView removeView];
        
        [self fetchFromCoreDataDashBoardList];
        
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Dashboard Details..."];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [DejalBezelActivityView removeView];
            [self newAPI];
            // [_tv_ForList reloadData];
        });
        
        
    }
}
#pragma mark-
#pragma mark- TABLE VIEW DELEGATE METHODS

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _tv_ForList) {
        return arrDashBoardResponse.count;
    }else{
        return 2;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tv_ForList) {
        CommonTBLCellVC *cell = (CommonTBLCellVC *)[tableView dequeueReusableCellWithIdentifier:@"NewDashboardCell" forIndexPath:indexPath];
        NSDictionary *dict=[arrDashBoardResponse objectAtIndex:indexPath.row];
        cell.Dash_lbl_MeterNumber.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AMRSerialNumber"]];
        cell.Dash_lbl_MeterWell.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AMRWellID"]];
        cell.Dash_lbl_MeterNickName.text=@"N/A";
        cell.Dash_lbl_Date.text=[global ChangeDateToLocalDateOtherNilind:[NSString stringWithFormat:@"%@",[dict valueForKey:@"CurrentDateTime"]]];
        cell.Dash_lbl_MeterReading.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CurrentReading"]];
        
        NSString *strStatus=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterStatus"]];
        if ([strStatus isEqualToString:@"Active"])
        {
            [cell.Dash_Btn_Status setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.Dash_Btn_Status setImage:[UIImage imageNamed:@"dislike"] forState:UIControlStateNormal];
        }
        
        
        return cell;
        
    }else{
        
        NSString *strIdentifire  = @"";
        if (indexPath.row == 0) {
            strIdentifire = @"NewMenuCell";
        }else{
            strIdentifire = @"NewMenuCelllogout";
        }
        CommonTBLCellVC *cell = (CommonTBLCellVC *)[tableView dequeueReusableCellWithIdentifier:strIdentifire forIndexPath:indexPath];
        return cell;
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _tv_ForMenuList) {
        if (indexPath.row == 0) {
            [self.viewMenu setHidden:YES];
            [self.btnTransprant setHidden:YES];
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            
            New_ExistingUserVC
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"New_ExistingUserVC"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }else{
            [self logOut];
            
        }
    }else{
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        
        New_MeterReadingHistoryVC
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"New_MeterReadingHistoryVC"];
        NSDictionary *dict=[arrDashBoardResponse objectAtIndex:indexPath.row];
        objByProductVC.strSerialMeterNo=[dict valueForKey:@"AMRSerialNumber"];
        objByProductVC.dictData = [arrDashBoardResponse objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
    
}
-(void)logOut{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm!"
                               message:@"Do you want to logout ?"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Logout" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                             [defs setBool:NO forKey:@"RememberMe"];
                             [defs synchronize];
                             
                             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                      bundle: nil];
                             LoginViewController
                             *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                             [self.navigationController pushViewController:objByProductVC animated:NO];
                             
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
#pragma mark-
#pragma mark- IBACTION's

- (IBAction)actionOnMenu:(id)sender {
    
    if ([self.viewMenu isHidden] == YES) {
        [UIView animateWithDuration:0.5f animations:^{
            [self.viewMenu setHidden:NO];
            [self.btnTransprant setHidden:NO];
            
        }];
    }else{
        [UIView animateWithDuration:0.5f animations:^{
            [self.viewMenu setHidden:YES];
            [self.btnTransprant setHidden:YES];
            
        }];
    }
    //   _viewMenu.frame = CGRectMake(-160, 64, 160, self.view.frame.size.height - 64);
    
    
}
- (IBAction)actionOnBtnTransprant:(id)sender {
    [self.viewMenu setHidden:YES];
    [self.btnTransprant setHidden:YES];
    
}

#pragma mark - Web Service

-(void)getDashBoardData
{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Dashboard Details..."];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strCustId= [NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]];
    
    
    //NSString *strMainUrl=@"http://localhost:4205/api/mobile/GetIsAMRMeterListByCustomerId?CustomerId=138";
    NSString *strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlGetDashBoardDetailsNew,@"138"];
    
    // strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     [self afterServerResponseDashBoard:response];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}

-(void)afterServerResponseDashBoard :(NSDictionary*)dictResponse{
    
    arrDashBoardResponse=(NSArray*)dictResponse;
    
    if (arrDashBoardResponse.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailable
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        NSMutableArray *tempArray=[NSMutableArray new];
        for(NSInteger i=0;i<arrDashBoardResponse.count ;i++)
        {
            NSMutableDictionary *dic =[[NSMutableDictionary alloc] initWithDictionary:[arrDashBoardResponse objectAtIndex:i]];
            
            
            NSString *dateFormat = @"MM/dd/yyyy";
            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
            [inputDateFormatter setTimeZone:inputTimeZone];
            [inputDateFormatter setDateFormat:dateFormat];
            
            NSString *inputString = [dic valueForKey:@"SubmittedDate"];
            NSDate *date = [inputDateFormatter dateFromString:inputString];
            
            
            
            NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
            [outputDateFormatter setTimeZone:outputTimeZone];
            [outputDateFormatter setDateFormat:dateFormat];
            NSString *outputString = [outputDateFormatter stringFromDate:date];
            
            NSDate *dateConverted = [inputDateFormatter dateFromString:inputString];
            
            
            [dic setValue:dateConverted forKey:@"SubmittedDate"];
            [tempArray addObject:dic];
        }
        
        
        NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:@"SubmittedDate" ascending:NO];
        NSArray *descriptors=[NSArray arrayWithObject: descriptor];
        NSArray *sortedArray=[tempArray sortedArrayUsingDescriptors:descriptors];
        NSLog(@"%@",sortedArray);
        
        [self saveToCoreDataDashBoardData:sortedArray];
        
        //        [self saveToCoreDataDashBoardData:arrDashBoardResponse];
        
        // [_tblViewHistory reloadData];
        
    }
}
-(void)newAPI
{
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strCustId= [NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]];
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Dashboard Details..."];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlGetDashBoardDetailsNew,strCustId];//138
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        
        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict=dictData;
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception)
        {
            
        } @finally
        {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
            [DejalBezelActivityView removeView];
            [self afterServerResponseDashBoard:ResponseDict];
        } else {
            ResponseDict=nil;
            // [DejalBezelActivityView removeView];
        }
        
        
        NSLog(@"Response on DashBoard Details = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [global AlertMethod:Alert :Sorry];
        //[DejalBezelActivityView removeView];
    }
    @finally {
    }
    [DejalBezelActivityView removeView];
}

//============================================================================
//============================================================================
#pragma mark--- -----------------Null Conversion-------------------
//============================================================================
//============================================================================

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

// akshay 10 Jan 2019
#pragma mark - Local DB Related Methods

-(void)saveToCoreDataDashBoardData :(NSArray*)arrOfDashBoardList{
    
    //==================================================================================
    //==================================================================================
    
    [self deleteDashBoardListFromDB];
    
    //==================================================================================
    //==================================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDashBoardList = [appDelegate managedObjectContext];
    entityDashBoardList=[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList];
    
    //==================================================================================
    //==  ================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    DashBoardListt *objDashBoardList = [[DashBoardListt alloc]initWithEntity:entityDashBoardList insertIntoManagedObjectContext:contextDashBoardList];
    objDashBoardList.userName=[dictLoginDetail valueForKey:@"UserName"];
    objDashBoardList.userType=@"";
    objDashBoardList.arrOfDashBoardList=arrOfDashBoardList;
    NSError *error1;
    [contextDashBoardList save:&error1];
    
    [self fetchFromCoreDataDashBoardList];
    //==================================================================================
    //==================================================================================
}
-(void)fetchFromCoreDataDashBoardList{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDashBoardList = [appDelegate managedObjectContext];
    entityDashBoardList=[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList];
    requestDashBoardList = [[NSFetchRequest alloc] init];
    [requestDashBoardList setEntity:entityDashBoardList];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestDashBoardList setPredicate:predicate];
    
    sortDescriptorDashBoardList = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsDashBoardList = [NSArray arrayWithObject:sortDescriptorDashBoardList];
    
    [requestDashBoardList setSortDescriptors:sortDescriptorsDashBoardList];
    
    self.fetchedResultsControllerDashBoardList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestDashBoardList managedObjectContext:contextDashBoardList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerDashBoardList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerDashBoardList performFetch:&error];
    arrAllObjDashBoardList = [self.fetchedResultsControllerDashBoardList fetchedObjects];
    if ([arrAllObjDashBoardList count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailable;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        matchesDashBoardList = arrAllObjDashBoardList[0];
        arrDashBoardResponse=[matchesDashBoardList valueForKey:@"arrOfDashBoardList"];
        [_tv_ForList reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}
-(void)deleteDashBoardListFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDashBoardList = [appDelegate managedObjectContext];
    entityDashBoardList=[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [contextDashBoardList executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [contextDashBoardList deleteObject:data];
        }
        NSError *saveError = nil;
        [contextDashBoardList save:&saveError];
    }
    
}
@end
