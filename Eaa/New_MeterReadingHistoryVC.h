//
//  New_MeterReadingHistoryVC.h
//  Eaa
//
//  Created by Navin Patidar on 12/5/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "Global.h"

NS_ASSUME_NONNULL_BEGIN

@interface New_MeterReadingHistoryVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tv_ForList;
- (IBAction)actionOnBack:(id)sender;
- (IBAction)actionOnComment:(id)sender;
@property(strong,nonatomic)NSString *strSerialMeterNo;
    @property (weak, nonatomic)  NSDictionary *dictData;

@end

NS_ASSUME_NONNULL_END
