//
//  New_MeterReadingHistoryVC.m
//  Eaa
//
//  Created by Navin Patidar on 12/5/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "New_MeterReadingHistoryVC.h"
#import "CommonTBLCellVC.h"
#import "New_CommentVC.h"
@interface New_MeterReadingHistoryVC ()

@end

@implementation New_MeterReadingHistoryVC
{
    Global *global;
    NSArray *arrMeterReadingHistory;
}
#pragma mark-
#pragma mark- Life Cycle 
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",_dictData);
    _tv_ForList.tableFooterView = [UIView new];
    _tv_ForList.estimatedRowHeight = 120.0;
    global=[[Global alloc]init];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
         [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Details..."];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
          [self newAPI:_strSerialMeterNo];
            [_tv_ForList reloadData];
         });
       

    }
    
}

#pragma mark-
#pragma mark- TABLE VIEW DELEGATE METHODS

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrMeterReadingHistory.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommonTBLCellVC *cell = (CommonTBLCellVC *)[tableView dequeueReusableCellWithIdentifier:@"NewMeterReadingHistoryCell" forIndexPath:indexPath];
    NSDictionary *dict=[arrMeterReadingHistory objectAtIndex:indexPath.row];
    cell.Dash_lbl_MeterReading.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CurrentReading"]];
    
    cell.Dash_lbl_Date.text=[global ChangeDateToLocalDateOtherNilind:[NSString stringWithFormat:@"%@",[dict valueForKey:@"CurrentDateTime"]]];
    cell.Dash_lbl_MeterWell.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AMRWellID"]];
    cell.Dash_lbl_MeterNumber.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AMRSerialNumber"]];
    cell.Dash_lbl_MeterNickName.text=@"N/A";
    
    cell.lblGPMAverage.text =[NSString stringWithFormat:@"%@",[dict valueForKey:@"FlowRate"]];
    cell.lblBattery_Voltage.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SiteBatteryVoltage"]];
        return cell;
   
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}


- (IBAction)actionOnBack:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)actionOnComment:(id)sender{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    New_CommentVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"New_CommentVC"];
    objByProductVC.dictData = _dictData;
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

#pragma mark - Web Service

-(void)getMeterReadingHistory
{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Dashboard Details..."];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlGetDashBoardDetails,@""];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"DashBoardDetails" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     [self afterServerResponseDashBoard:response];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}

-(void)afterServerResponseDashBoard :(NSDictionary*)dictResponse{
    
    arrMeterReadingHistory=(NSArray*)dictResponse;
    [DejalBezelActivityView removeView];
    if (arrMeterReadingHistory.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailable
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    } else {
        
        NSMutableArray *tempArray=[NSMutableArray new];
        for(NSInteger i=0;i<arrMeterReadingHistory.count ;i++)
        {
            NSMutableDictionary *dic =[[NSMutableDictionary alloc] initWithDictionary:[arrMeterReadingHistory objectAtIndex:i]];
            
            NSString *dateFormat = @"MM/dd/yyyy";
            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
            [inputDateFormatter setTimeZone:inputTimeZone];
            [inputDateFormatter setDateFormat:dateFormat];
            
            NSString *inputString = [dic valueForKey:@"SubmittedDate"];
            NSDate *date = [inputDateFormatter dateFromString:inputString];
            
            NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
            [outputDateFormatter setTimeZone:outputTimeZone];
            [outputDateFormatter setDateFormat:dateFormat];
            NSString *outputString = [outputDateFormatter stringFromDate:date];
            
            NSDate *dateConverted = [inputDateFormatter dateFromString:inputString];
            
            
            [dic setValue:dateConverted forKey:@"SubmittedDate"];
            [tempArray addObject:dic];
        }
        
        
        NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:@"SubmittedDate" ascending:NO];
        NSArray *descriptors=[NSArray arrayWithObject: descriptor];
        NSArray *sortedArray=[tempArray sortedArrayUsingDescriptors:descriptors];
        NSLog(@"%@",sortedArray);
        
        //  [self saveToCoreDataDashBoardData:sortedArray];
        
        //        [self saveToCoreDataDashBoardData:arrDashBoardResponse];
        
        // [_tblViewHistory reloadData];
        
    }
}
//Nilind

-(void)newAPI :(NSString *)strMeterNo
{
    strMeterNo=[strMeterNo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strCustId= [NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]];
   // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Dashboard Details..."];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlGetARMMeterReadingHistoryNew,strMeterNo];//138
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        
        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict=dictData;
        
        NSString *strException;
        @try
        {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        }
        @catch (NSException *exception)
        {
            
        } @finally
        {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
          //  [DejalBezelActivityView removeView];
            [self afterServerResponseDashBoard:ResponseDict];
        } else
        {
            ResponseDict=nil;
           // [DejalBezelActivityView removeView];
        }
        
        
        NSLog(@"Response on DashBoard Details = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [global AlertMethod:Alert :Sorry];
        //[DejalBezelActivityView removeView];
    }
    @finally {
    }
}

//============================================================================
//============================================================================
#pragma mark--- -----------------Null Conversion-------------------
//============================================================================
//============================================================================

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}


@end
