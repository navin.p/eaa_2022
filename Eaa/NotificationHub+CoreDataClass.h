//
//  NotificationHub+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 28/01/19.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface NotificationHub : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "NotificationHub+CoreDataProperties.h"
