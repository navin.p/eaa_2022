//
//  NotificationHub+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 28/01/19.
//
//

#import "NotificationHub+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface NotificationHub (CoreDataProperties)

+ (NSFetchRequest<NotificationHub *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *arrOfNotificationHub;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *userType;

@end

NS_ASSUME_NONNULL_END
