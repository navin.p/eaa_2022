//
//  NotificationHub+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 28/01/19.
//
//

#import "NotificationHub+CoreDataProperties.h"

@implementation NotificationHub (CoreDataProperties)

+ (NSFetchRequest<NotificationHub *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"NotificationHub"];
}

@dynamic arrOfNotificationHub;
@dynamic userName;
@dynamic userType;

@end
