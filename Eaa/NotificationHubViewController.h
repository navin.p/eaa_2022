//
//  NotificationHubViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 28/01/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
NS_ASSUME_NONNULL_BEGIN

@interface NotificationHubViewController : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityHistoryList;
    NSManagedObjectContext *contextHistoryList;
    NSFetchRequest *requestHistoryList;
    NSSortDescriptor *sortDescriptorHistoryList;
    NSArray *sortDescriptorsHistoryList;
    NSManagedObject *matchesHistoryList;
    NSArray *arrAllObjHistoryList;
}
@property (strong, nonatomic) IBOutlet UITableView *tblViewNotificationHub;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerHistoryList;
- (IBAction)action_Back:(id)sender;

@end

NS_ASSUME_NONNULL_END
