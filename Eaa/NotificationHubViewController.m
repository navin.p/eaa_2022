//
//  NotificationHubViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 28/01/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import "NotificationHubViewController.h"
#import "HistoryTableViewCell.h"
#import "SlideMenuTableViewCell.h"
#import "ReportMeterReadingViewController.h"
#import "AddMeterViewController.h"
#import "ManageMeterViewController.h"
#import "OutboxViewController.h"
#import "DisclaimerViewController.h"
#import "HelpViewController.h"
#import "LoginViewController.h"
#import "TermsConditionViewController.h"
#import "DashBoardViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "HelpViewGlobalViewController.h"
#import "TechDashBoardViewController.h"
#import "HistoryListt+CoreDataProperties.h"
#import "NotificationHub+CoreDataClass.h"
#import "NotificationHub+CoreDataClass.h"


@interface NotificationHubViewController ()
{
    NSMutableArray *arrOfNotificationHistory;
    Global *global;
    
}
@end

@implementation NotificationHubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    global = [[Global alloc] init];
    
    arrOfNotificationHistory = [NSMutableArray new];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [self fetchFromCoreDataHistoryList];
        
    }
    else
    {
        
        [self getNotificationHistory];
        
    }
    
}

//============================================================================
#pragma mark- Button Actions METHODS
//============================================================================

- (IBAction)action_Back:(id)sender {
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k=0; k<arrstack.count; k++) {
        if ([[arrstack objectAtIndex:k] isKindOfClass:[DashBoardViewController class]]) {
            index=k;
            // break;
        }
    }
    DashBoardViewController *myController = (DashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
    [self.navigationController popToViewController:myController animated:NO];
    
}

//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrOfNotificationHistory.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableView.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"NotificationTableViewCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    [cell updateConstraintsIfNeeded];
    return cell;
}
- (void)configureCell:(HistoryTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictData = arrOfNotificationHistory[indexPath.row];
    //cell.lbl_NickName.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterName"]];
    cell.lblNotificationTitle.text = [dictData valueForKey:@"Notification"];
    cell.lblNotificationSentDate.text = [global ChangeDateToLocalDateFull:[dictData valueForKey:@"SentDate"]];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSDictionary *dictData = arrOfNotificationHistory[indexPath.row];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Notification"
                                  message:[dictData valueForKey:@"Notification"]
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    //[alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - get notification history from server
-(void)getNotificationHistory
{
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strAuthId= [NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"AuthId"]];
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Details..."];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlGetNotificationsHistoryByAuthId,strAuthId];//138 strCustId
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        
        dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict=dictData;
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception)
        {
            
        } @finally
        {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
            [DejalBezelActivityView removeView];
            arrOfNotificationHistory = [[dict valueForKey:@"response"] mutableCopy];
            [self afterServerResponseHistory:ResponseDict];
        } else {
            ResponseDict=nil;
            // [DejalBezelActivityView removeView];
        }
        
        
        NSLog(@"Response on DashBoard Details = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [global AlertMethod:Alert :Sorry];
        //[DejalBezelActivityView removeView];
    }
    @finally {
    }
    [DejalBezelActivityView removeView];
}

//============================================================================
#pragma mark- Core Data METHODS
//============================================================================

-(void)afterServerResponseHistory :(NSDictionary*)dictResponse{
    
    
    NSArray *arrOfHistory=(NSArray*)dictResponse;
    
    if (arrOfHistory.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailable
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  [self.navigationController popViewControllerAnimated:YES];
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        [self saveToCoreDataNotications:arrOfHistory];
        
    }
}
-(void)saveToCoreDataNotications :(NSArray*)arrOfHistoryList{
    
    //==================================================================================
    //==================================================================================
    
    [self deleteHistoryListFromDB];
    
    //==================================================================================
    //==================================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextHistoryList = [appDelegate managedObjectContext];
    entityHistoryList=[NSEntityDescription entityForName:@"NotificationHub" inManagedObjectContext:contextHistoryList];
    
    //==================================================================================
    //==  ================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    NotificationHub *objHistoryList = [[NotificationHub alloc]initWithEntity:entityHistoryList insertIntoManagedObjectContext:contextHistoryList];
    objHistoryList.userName=[dictLoginDetail valueForKey:@"UserName"];
    objHistoryList.userType=@"";
    objHistoryList.arrOfNotificationHub=arrOfHistoryList;
    NSError *error1;
    [contextHistoryList save:&error1];
    
    [self fetchFromCoreDataHistoryList];
    //==================================================================================
    //==================================================================================
}

-(void)deleteHistoryListFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextHistoryList = [appDelegate managedObjectContext];
    entityHistoryList=[NSEntityDescription entityForName:@"NotificationHub" inManagedObjectContext:contextHistoryList];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"NotificationHub" inManagedObjectContext:contextHistoryList]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [contextHistoryList executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [contextHistoryList deleteObject:data];
        }
        NSError *saveError = nil;
        [contextHistoryList save:&saveError];
    }
    
}

-(void)fetchFromCoreDataHistoryList{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextHistoryList = [appDelegate managedObjectContext];
    entityHistoryList=[NSEntityDescription entityForName:@"NotificationHub" inManagedObjectContext:contextHistoryList];
    requestHistoryList = [[NSFetchRequest alloc] init];
    [requestHistoryList setEntity:entityHistoryList];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestHistoryList setPredicate:predicate];
    
    sortDescriptorHistoryList = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsHistoryList = [NSArray arrayWithObject:sortDescriptorHistoryList];
    
    [requestHistoryList setSortDescriptors:sortDescriptorsHistoryList];
    
    self.fetchedResultsControllerHistoryList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestHistoryList managedObjectContext:contextHistoryList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerHistoryList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerHistoryList performFetch:&error];
    arrAllObjHistoryList = [self.fetchedResultsControllerHistoryList fetchedObjects];
    if ([arrAllObjHistoryList count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailable;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        matchesHistoryList = arrAllObjHistoryList[0];
        arrOfNotificationHistory=[matchesHistoryList valueForKey:@"arrOfNotificationHub"];
        [_tblViewNotificationHub reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}


@end
