//
//  OutboxTableViewCell.h
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OutboxTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_datenTime;
@property (strong, nonatomic) IBOutlet UILabel *lbl_MeterSerialNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_MeterReading;

@end
