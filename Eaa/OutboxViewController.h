//
//  OutboxViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>

@interface OutboxViewController : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityMeterReading;
    NSManagedObjectContext *contextMeterReading;
    NSFetchRequest *requestMeterReading;
    NSSortDescriptor *sortDescriptorMeterReading;
    NSArray *sortDescriptorsMeterReading;
    NSManagedObject *matchesMeterReading;
    NSArray *arrAllObjMeterReading;
}
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMeterReading;
@property (strong, nonatomic) IBOutlet UITableView *tblViewOutBox;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Count;
-(void)fetchFromCoreDataMeterReading;

@end
