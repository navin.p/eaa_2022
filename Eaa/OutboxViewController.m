//
//  OutboxViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "OutboxViewController.h"
#import "OutboxTableViewCell.h"
#import "DashBoardViewController.h"
//#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "TechDashBoardViewController.h"

@interface OutboxViewController ()
{
    Global *global;
}

@end

@implementation OutboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchFromCoreDataMeterReadingOutbox) name:@"OutBoxRefresh" object:nil];

    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

    [self fetchFromCoreDataMeterReading];
    
    _tblViewOutBox.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrAllObjMeterReading.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OutboxTableViewCell *cell = (OutboxTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"OutboxTableViewCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
    
}
- (void)configureCell:(OutboxTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    
    matchesMeterReading=arrAllObjMeterReading[indexPath.row];
   // NSDictionary *dictOfData=[matchesMeterReading valueForKey:@"jsonString"];
    cell.lbl_datenTime.text=[matchesMeterReading valueForKey:@"datenTime"];
    cell.lbl_MeterReading.text=[matchesMeterReading valueForKey:@"meterReading"];
    cell.lbl_MeterSerialNo.text=[matchesMeterReading valueForKey:@"meterSerialNo"];
}

- (IBAction)action_Back:(id)sender {
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k=0; k<arrstack.count; k++) {
            if ([[arrstack objectAtIndex:k] isKindOfClass:[DashBoardViewController class]]) {
                index=k;
                // break;
            }
        }
        DashBoardViewController *myController = (DashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        
    }else{
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k=0; k<arrstack.count; k++) {
            if ([[arrstack objectAtIndex:k] isKindOfClass:[TechDashBoardViewController class]]) {
                index=k;
                // break;
            }
        }
        TechDashBoardViewController *myController = (TechDashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        
    }

}

//============================================================================
#pragma mark- METHODS
//============================================================================

-(void)fetchFromCoreDataMeterReading{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextMeterReading = [appDelegate managedObjectContext];
    entityMeterReading=[NSEntityDescription entityForName:@"AddMeterReading" inManagedObjectContext:contextMeterReading];
    requestMeterReading = [[NSFetchRequest alloc] init];
    [requestMeterReading setEntity:entityMeterReading];
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestMeterReading setPredicate:predicate];
    
    sortDescriptorMeterReading = [[NSSortDescriptor alloc] initWithKey:@"dateAscending" ascending:NO];
    sortDescriptorsMeterReading = [NSArray arrayWithObject:sortDescriptorMeterReading];
    
    [requestMeterReading setSortDescriptors:sortDescriptorsMeterReading];
    
    self.fetchedResultsControllerMeterReading = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMeterReading managedObjectContext:contextMeterReading sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMeterReading setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMeterReading performFetch:&error];
    arrAllObjMeterReading = [self.fetchedResultsControllerMeterReading fetchedObjects];
    
    if ([arrAllObjMeterReading count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailable;
        [global AlertMethod:strTitle :strMsg];
       // [DejalBezelActivityView removeView];
        [_tblViewOutBox setHidden:YES];
        _lbl_Count.text=@"OUTBOX : 0";
    }
    else
    {
        [_tblViewOutBox setHidden:NO];
       // matchesMeterReading = arrAllObjMeterReading[0];
        
        _lbl_Count.text=[NSString stringWithFormat:@"OUTBOX : %lu",(unsigned long)arrAllObjMeterReading.count];
        
        [_tblViewOutBox reloadData];
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            
        }
        else
        {

    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{

        for (int k=0; k<arrAllObjMeterReading.count; k++) {
            
               matchesMeterReading = arrAllObjMeterReading[k];
            
                [self sendMeterReadingSavedToServer:[matchesMeterReading valueForKey:@"meterImageName"] :[matchesMeterReading valueForKey:@"jsonString"]];

        }
          dispatch_async(dispatch_get_main_queue(), ^{
              // Update the UI
          });
      });

      }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }

}
-(void)fetchFromCoreDataMeterReadingOutbox{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextMeterReading = [appDelegate managedObjectContext];
    entityMeterReading=[NSEntityDescription entityForName:@"AddMeterReading" inManagedObjectContext:contextMeterReading];
    requestMeterReading = [[NSFetchRequest alloc] init];
    [requestMeterReading setEntity:entityMeterReading];
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestMeterReading setPredicate:predicate];
    
    sortDescriptorMeterReading = [[NSSortDescriptor alloc] initWithKey:@"dateAscending" ascending:NO];
    sortDescriptorsMeterReading = [NSArray arrayWithObject:sortDescriptorMeterReading];
    
    [requestMeterReading setSortDescriptors:sortDescriptorsMeterReading];
    
    self.fetchedResultsControllerMeterReading = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMeterReading managedObjectContext:contextMeterReading sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMeterReading setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMeterReading performFetch:&error];
    arrAllObjMeterReading = [self.fetchedResultsControllerMeterReading fetchedObjects];
    
    if ([arrAllObjMeterReading count] == 0)
    {
//        NSString *strTitle = Alert;
//        NSString *strMsg = NoDataAvailable;
//        [global AlertMethod:strTitle :strMsg];
        //[DejalBezelActivityView removeView];
        [_tblViewOutBox setHidden:YES];
        _lbl_Count.text=@"OUTBOX : 0";
    }
    else
    {
        [_tblViewOutBox setHidden:NO];
        
        // matchesMeterReading = arrAllObjMeterReading[0];
        _lbl_Count.text=[NSString stringWithFormat:@"OUTBOX : %lu",(unsigned long)arrAllObjMeterReading.count];
        [_tblViewOutBox reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}

//dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
//dispatch_async(myQueue, ^{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        // Update the UI
//    });
//});

-(void)sendMeterReadingSavedToServer :(NSString*)strImageName :(NSString*)jsonString{
    
    NSLog(@"Json offline being sent======%@ , %@",strImageName,jsonString);
    
    if (strImageName.length>0) {
        
        [self uploadImage:strImageName :jsonString];
        
    }else{
        
        [self sendingFinalJsonOutBox:strImageName :jsonString];
        
    }
    
}
-(void)sendingFinalJsonOutBox :(NSString*)strImageName :(NSString*)jsonString{
    
    NSLog(@"Offline Json Being Sent = = = %@",jsonString);
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlAddMeterReading];
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Submitting Request..."];
    
    //============================================================================
    //============================================================================
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        //        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
        //                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
        //         {
        //             NSData* jsonData = [NSData dataWithData:data];
        //             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        //         }];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        NSString *strException;
        
        if (strException.length==0) {
            
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            if (ResponseDict.count==0) {
                
            } else {
                
                BOOL isSuccess=[[ResponseDict valueForKey:@"IsSuccess"] boolValue];
                
                if (isSuccess) {
                    
                    [self deleteFromOutBoxCoreData:strImageName];

                    
                } else {
                    
                    NSArray *arrOfError=[ResponseDict valueForKey:@"Error"];
                    if ([arrOfError isKindOfClass:[NSArray class]]) {
                        
                        if (arrOfError.count==0) {
                            
                            
                        } else {
                            
                            NSString *strErrorMsg=arrOfError[0];
                            if ([strErrorMsg isEqualToString:@"Reading can be submitted only once in 24 hrs."]) {
                              
                                [self deleteFromOutBoxCoreData:strImageName];
 
                            } else {
                                
                            }                            
                        }
                        
                    } else {
                        
                        NSString *strTitle = Alert;
                        NSString *strMsg = Sorry;
                        [global AlertMethod:strTitle :strMsg];
                        
                    }
                    
                }
            }
        }
    }
    @catch (NSException *exception) {
        // [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    
}
//============================================================================
//============================================================================
#pragma mark--- -----------------deleting From OutBox Core Data-------------------
//============================================================================
//============================================================================

-(void)deleteFromOutBoxCoreData :(NSString *)strImageName{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextMeterReading = [appDelegate managedObjectContext];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"AddMeterReading" inManagedObjectContext:contextMeterReading]];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"meterImageName=%@",strImageName];
    [allData setPredicate:p];
    NSError * error = nil;
    NSArray *Data = [contextMeterReading executeFetchRequest:allData error:&error];
    for (NSManagedObject * car in Data) {
        [contextMeterReading  deleteObject:car];
        NSError *saveError = nil;
        [contextMeterReading save:&saveError];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OutBoxRefresh" object:self];
    
}

//============================================================================
//============================================================================
#pragma mark--- -----------------Null Conversion-------------------
//============================================================================
//============================================================================

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Image Upload Method -----------------------
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName :(NSString*)jsonString{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];
    
    NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",MainUrl,UrlImageUpload];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Image Sent");
    }
    NSLog(@"Image Sent");
    
    [self sendingFinalJsonOutBox:strImageName :jsonString];
}

@end
