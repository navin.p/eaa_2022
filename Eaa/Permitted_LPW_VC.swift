//
//  Permitted_LPW_VC.swift
//  Eaa
//
//  Created by Navin Patidar on 10/30/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit


class Permitted_LPW_VC: UIViewController {

    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblMeterNumber: UILabel!
    
    @IBOutlet weak var viewContain: UIView!
    
    
    @IBOutlet weak var collectionPermitted: UICollectionView!
    @IBOutlet var viewPermitted: UIView!
    
    
    @IBOutlet var viewLPW: UIView!
    @IBOutlet weak var scalimage: UIImageView!
    @IBOutlet weak var lblLPWMsg: UILabel!

    @IBOutlet weak var verticalSlider: UISlider!{
        didSet{
            verticalSlider.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2))
        }
    }
    @IBOutlet weak var collectionLPW: UICollectionView!



    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btNext: UIButton!

    //MARK:---------Variable
    @objc var dictMeterData = NSMutableDictionary()
    @objc var aryCollection = NSMutableArray()
    @objc var currentIndex = Int()
    
    var aryMeterData = NSMutableArray()
    var aryPermittedData = NSMutableArray()
    var aryLPWData = NSMutableArray()
    var strSwipDirection = ""
    
     //MARK:
     //MARK:-----------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let directions: [UISwipeGestureRecognizer.Direction] = [.up, .down, .right, .left]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(gesture:)))
            gesture.direction = direction
            self.view?.addGestureRecognizer(gesture)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callUrlGetReportingPeriodDataAPI()
      
    }
    
    
    
    //MARK:
    //MARK:-----------handleSwipe
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        print(gesture.direction)
        switch gesture.direction {
        case UISwipeGestureRecognizer.Direction.down:
            print("down swipe")
        case UISwipeGestureRecognizer.Direction.up:
            print("up swipe")
        case UISwipeGestureRecognizer.Direction.left:
            print("left swipe")
            strSwipDirection = "left"
            if(currentIndex < aryCollection.count - 1){
                btNext.alpha = 1.0
                currentIndex = currentIndex + 1
                setDataOnView(animation: true)
            }else{
                btNext.alpha = 0.8
            }
        case UISwipeGestureRecognizer.Direction.right:
            print("right swipe")
            strSwipDirection = "right"

            if(currentIndex > 0){
                btnPrevious.alpha = 1.0
                currentIndex = currentIndex - 1
                setDataOnView(animation: true)
            }else{
                
                btnPrevious.alpha = 0.8
            }
        default:
            print("other swipe")
        }
    }
    
    
    //MARK:
    //MARK:-----------Extra
    func setDataOnView(animation : Bool) {
        self.viewPermitted.removeFromSuperview()
        self.viewLPW.removeFromSuperview()
        self.callGetGetMeterYearReadingAPI()
        if(currentIndex == 0){
            btnPrevious.alpha = 0.8
        }else{
            btnPrevious.alpha = 1.0
        }
        if(currentIndex == aryCollection.count - 1){
            btNext.alpha = 0.8
        }else{
            btNext.alpha = 1.0
        }
    }
    
    
    
    func returnForLPWImage(dict : NSDictionary , index : Int) -> UIImage {
        let dict1 = aryCollection.object(at: currentIndex)as! NSDictionary
        let status = "\(dict1.value(forKey: "Status")!)"
        let CurrentMeterReading = "\(dict.value(forKey: "CurrentMeterReading")!)".replacingOccurrences(of: ".", with: "")
        // 1/25/2019 12:00:00 AM
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM/dd/yyy hh:mm:ss a"
       
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        
        let strStartDate = "\(dict.value(forKey: "ReadingDetailStartDate")!)"
        dateFormatter1.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter1.date(from: strStartDate)
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let strDateFormatted = dateFormatter1.string(from: dt!)
     
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        dateFormatter2.timeZone = TimeZone.current
        dateFormatter2.timeZone = TimeZone(abbreviation: "UTC")
        let myStartDate = dateFormatter2.date(from: strDateFormatted)!
        
        dateFormatter1.dateFormat = "MM/dd/yyy hh:mm:ss a"
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        
        let strEndDate = "\(dict.value(forKey: "ReadingDetailEndDate")!)"
        dateFormatter1.timeZone = TimeZone(abbreviation: "UTC")
        let dt1 = dateFormatter1.date(from: strEndDate)
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let strDateFormatted1 = dateFormatter1.string(from: dt1!)
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        dateFormatter2.timeZone = TimeZone.current
        dateFormatter2.timeZone = TimeZone(abbreviation: "UTC")
        let myEndDate = dateFormatter2.date(from: strDateFormatted1)!
        
        dateFormatter1.dateFormat = "MM/dd/yyy hh:mm:ss a"
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        
        let date = Date()
        let dateString = dateFormatter2.string(from: date)
        let myCurrentDate = dateFormatter2.date(from: dateString)!
        let fallsBetween = (myStartDate ... myEndDate).contains(myCurrentDate)
        
        
        
        print(index)
        
        if(fallsBetween){
            if(status == "" || CurrentMeterReading == "0"){
                //For last Index show Start
                if(aryLPWData.count == index){
                    return  UIImage(named: "endOfYear")!
                }
                //----------------
                return UIImage(named: "quarter_4")! // Blue Image
            }else{
                //For last Index show Start
                if(aryLPWData.count == index){
                    return  UIImage(named: "endOfYear")!
                }
                //----------------
                return UIImage(named: "quarter_2")! // Like image
            }
        }else{
            var image = UIImage()
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM"
            let nameOfMonth = dateFormatter.string(from: now)
            print(nameOfMonth)
            print(index)
          //  print(dict)
            
            if(Int(CurrentMeterReading) ==  0){
                image = UIImage(named: "quarter_3")! //Not Like
            }else if (Int(CurrentMeterReading)! >  0){
                image = UIImage(named: "quarter_2")! // Like
            }

            //For last Index show Start
            if(aryLPWData.count == index){
                image =    UIImage(named: "endOfYear")!
            }
            //----------------
            
            let strYearValue = "\(dict.value(forKey: "YearValue")!)"
            let dateFormatter11 = DateFormatter()
            dateFormatter11.dateFormat = "Q"
            let QU = dateFormatter11.string(from: Date())
           
            // Then change the quarter NSLog too
            NSLog("quarter : ", QU);
            if(Int(QU)! < Int(strYearValue)!){
                image = UIImage(named: "quarter_1")! //Gray
                //For last Index show Start
                if(aryLPWData.count == index){
                    image =    UIImage(named: "endOfYear-1")!
                }
                //----------------
            }
            else if (Int(QU) == Int(strYearValue)!){
                if(myStartDate > myCurrentDate){
                    image = UIImage(named: "quarter_1")! //Gray
                    //For last Index show Start
                    if(aryLPWData.count == index){
                        image =    UIImage(named: "endOfYear-1")!
                    }
                    //----------------
                }
            }
           
            
            return image
        }
        
    }
    func returnForPermittedImage(dict : NSDictionary , index : Int) -> UIImage {
       
        print(dict)
        
        let dict1 = aryCollection.object(at: currentIndex)as! NSDictionary
        let status = "\(dict1.value(forKey: "Status")!)"
        let CurrentMeterReading = "\(dict.value(forKey: "CurrentMeterReading")!)".replacingOccurrences(of: ".", with: "")
       // 1/25/2019 12:00:00 AM
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM/dd/yyy hh:mm:ss a"
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        
        let strStartDate = "\(dict.value(forKey: "ReadingDetailStartDate")!)"
        dateFormatter1.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter1.date(from: strStartDate)
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let strDateFormatted = dateFormatter1.string(from: dt!)
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        dateFormatter2.timeZone = TimeZone.current
        dateFormatter2.timeZone = TimeZone(abbreviation: "UTC")
        let myStartDate = dateFormatter2.date(from: strDateFormatted)!
     
        dateFormatter1.dateFormat = "MM/dd/yyy hh:mm:ss a"
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        
        let strEndDate = "\(dict.value(forKey: "ReadingDetailEndDate")!)"
        dateFormatter1.timeZone = TimeZone(abbreviation: "UTC")
        let dt1 = dateFormatter1.date(from: strEndDate)
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let strDateFormatted1 = dateFormatter1.string(from: dt1!)
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        dateFormatter2.timeZone = TimeZone.current
        dateFormatter2.timeZone = TimeZone(abbreviation: "UTC")
        let myEndDate = dateFormatter2.date(from: strDateFormatted1)!
        
        dateFormatter1.dateFormat = "MM/dd/yyy hh:mm:ss a"
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        
        let date = Date()
        let dateString = dateFormatter2.string(from: date)
        let myCurrentDate = dateFormatter2.date(from: dateString)!
     //
          var fallsBetween = Bool()
        if(myStartDate < myEndDate){
              fallsBetween = (myStartDate ... myEndDate).contains(myCurrentDate)
        }else{
            fallsBetween = (myEndDate ... myStartDate).contains(myCurrentDate)
        }
        if(fallsBetween){
            if(status == "" || CurrentMeterReading == "0"){
                //For last Index show Start
                if(aryPermittedData.count == index){
                    return  UIImage(named: "endOfYear")!
                }
                //----------------
                return UIImage(named: "quarter_4")! // Blue Image
            }else{
                //For last Index show Start
                if(aryPermittedData.count == index){
                    return  UIImage(named: "endOfYear")!
                }
                //----------------
                return UIImage(named: "quarter_2")! // Like image
            }
           
        }else{
            var image = UIImage()
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM"
            let nameOfMonth = dateFormatter.string(from: now)
         //   print(nameOfMonth)
            print(index)
          //  print(dict)

            if(Int(CurrentMeterReading) ==  0){
                image = UIImage(named: "quarter_3")! //Not Like
            }else if (Int(CurrentMeterReading)! >  0){
                image = UIImage(named: "quarter_2")! // Like
            }
            
            //For last Index show Start
            if(aryPermittedData.count == index){
                image =  UIImage(named: "endOfYear")!
            }
            //----------------
            
            if(index > Int(nameOfMonth)!){
                //For last Index show Start
                if(aryPermittedData.count == index){
                    image =    UIImage(named: "endOfYear-1")!
                } else{
                    image = UIImage(named: "quarter_1")! //Gray
                }
            }
            
            else if (index == Int(nameOfMonth)!){
                if(myStartDate > myCurrentDate){
                    image = UIImage(named: "quarter_1")! //Gray
                    //For last Index show Start
                    if(aryPermittedData.count == index){
                        //Deepak.s
//                        image =    UIImage(named: "endOfYear_1")!
                        image =    UIImage(named: "endOfYear-1")!
                    }

                }
            }

            return image
        }
        
    }

    
    
    
    
    func dataPloatOnPermitted_AND_LPW(dictReading: NSDictionary)  {
        
        if(aryCollection.count != 0){
            self.aryMeterData = NSMutableArray()
            self.aryMeterData = (dictReading.value(forKey: "ReadingDetail")as! NSArray).mutableCopy()as! NSMutableArray
            let dict = aryCollection.object(at: currentIndex)as! NSDictionary
            lblTitle.text = "\(dict.value(forKey: "MeterType")!)"
            lblMeterNumber.text = "Meter Number - \(dict.value(forKey: "MeterSerialNumber")!)"
            lblLPWMsg.text = ""
         var TotalUse = Float()
            if(dictReading.value(forKey: "Master") is NSArray){
                let aryMaster = dictReading.value(forKey: "Master") as! NSArray
                if(aryMaster.count != 0){
                    let strTotalUse = "\((aryMaster.object(at: 0)as! NSDictionary).value(forKey: "TotalUse")!)"
                    print("Total Meter Reading : ===-----\(strTotalUse)")
                    if(strTotalUse.count != 0){
                        TotalUse = Float((strTotalUse as NSString).doubleValue)
                        verticalSlider.value = TotalUse
                        if(TotalUse > 1.4){
                            lblLPWMsg.text = "You have over pumped water"
                            lblLPWMsg.textColor = UIColor.red
                        }else {
                            lblLPWMsg.text = "Water limit 1.4 acre-feet per year"
                            lblLPWMsg.textColor = UIColor.green
                        }
                        
                    }else{
                        TotalUse  = 0.0
                    }
                }
            }
           
           
            if("\(dict.value(forKey: "MeterType")!)" == "Permitted"){
                
                 let resultPredicate = NSPredicate(format: "Type contains[c] %@", argumentArray: ["Month"])
                let array = (aryPeriodDataGloble as NSArray).filtered(using: resultPredicate)
                
                //print(array)
                self.aryPermittedData = NSMutableArray()
                self.aryPermittedData = (array as NSArray).mutableCopy()as! NSMutableArray
                let tempPermitted = NSMutableArray()
                for itemPeriodData in aryPermittedData{
                    let strValue = "\((itemPeriodData as AnyObject).value(forKey: "Value")!)"
                    
                    
                    for itemMeterDataData in aryMeterData{
                        let strYearType = "\((itemMeterDataData as AnyObject).value(forKey: "YearValue")!)"
                        let strCurrentMeterReading = "\((itemMeterDataData as AnyObject).value(forKey: "CurrentMeterReading")!)"
                        let strStatus = "\((itemMeterDataData as AnyObject).value(forKey: "Status")!)"
                        let strReadingDetailStartDate = "\((itemMeterDataData as AnyObject).value(forKey: "StartDate")!)"
                        let strReadingDetailEndDate = "\((itemMeterDataData as AnyObject).value(forKey: "EndDate")!)"
                        let strYearValue = "\((itemMeterDataData as AnyObject).value(forKey: "YearValue")!)"

                        if(strValue == strYearType){
                            var dictTemp = NSMutableDictionary()
                            dictTemp = ((itemPeriodData as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            dictTemp.setValue(strCurrentMeterReading, forKey: "CurrentMeterReading")
                            dictTemp.setValue(strStatus, forKey: "Status")
                            dictTemp.setValue(strReadingDetailStartDate, forKey: "ReadingDetailStartDate")
                            dictTemp.setValue(strReadingDetailEndDate, forKey: "ReadingDetailEndDate")
                            dictTemp.setValue(strYearValue, forKey: "YearValue")

                            tempPermitted.add(dictTemp)
                            break
                        }
                    }
                }
                self.aryPermittedData = NSMutableArray()
                self.aryPermittedData = tempPermitted
                self.viewPermitted.removeFromSuperview()
                self.viewLPW.removeFromSuperview()
                var xAxis = 0.0
                if(strSwipDirection == "right"){
                    xAxis = Double(-self.view.frame.width)
                }else{
                    xAxis = Double(self.view.frame.width)
                }
                self.viewPermitted.frame = CGRect(x: Int(xAxis), y: 0, width: Int(self.viewContain.frame.width), height: Int(self.viewContain.frame.height))
                UIView.animate(withDuration: 0.50, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: {
                    //Set x position what ever you want
                    self.viewPermitted.frame = CGRect(x: 0, y: 0, width: self.viewContain.frame.width, height: self.viewContain.frame.height)
                }, completion: nil)
                self.viewContain.addSubview(self.viewPermitted)
                self.collectionPermitted.reloadData()
            }
            else{
                let resultPredicate = NSPredicate(format: "Type contains[c] %@", argumentArray: ["Quarter"])
                let array = (aryPeriodDataGloble as NSArray).filtered(using: resultPredicate)
                
              //  print(array)
                
                self.aryLPWData = NSMutableArray()
                self.aryLPWData = (array as NSArray).mutableCopy()as! NSMutableArray
                let tempLPW = NSMutableArray()
                for itemPeriodData in aryLPWData{
                    let strValue = "\((itemPeriodData as AnyObject).value(forKey: "Value")!)"
                    for itemMeterDataData in aryMeterData{
                        let strYearType = "\((itemMeterDataData as AnyObject).value(forKey: "YearValue")!)"
                        let strCurrentMeterReading = "\((itemMeterDataData as AnyObject).value(forKey: "CurrentMeterReading")!)"
                        let strStatus = "\((itemMeterDataData as AnyObject).value(forKey: "Status")!)"
                        let strReadingDetailStartDate = "\((itemMeterDataData as AnyObject).value(forKey: "StartDate")!)"
                        let strReadingDetailEndDate = "\((itemMeterDataData as AnyObject).value(forKey: "EndDate")!)"
                        let strYearValue = "\((itemMeterDataData as AnyObject).value(forKey: "YearValue")!)"

                        if(strValue == strYearType){
                            var dictTemp = NSMutableDictionary()
                            dictTemp = ((itemPeriodData as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            dictTemp.setValue(strCurrentMeterReading, forKey: "CurrentMeterReading")
                            dictTemp.setValue(strStatus, forKey: "Status")
                            dictTemp.setValue(strReadingDetailStartDate, forKey: "ReadingDetailStartDate")
                            dictTemp.setValue(strReadingDetailEndDate, forKey: "ReadingDetailEndDate")
                            dictTemp.setValue(strYearValue, forKey: "YearValue")

                            tempLPW.add(dictTemp)
                            break
                        }
                    }
                }
                self.aryLPWData = NSMutableArray()
                self.aryLPWData = tempLPW
                self.viewPermitted.removeFromSuperview()
                self.viewLPW.removeFromSuperview()
                
                
                
                var xAxis = 0.0
                if(strSwipDirection == "right"){
                    xAxis = Double(-self.view.frame.width)
                }else{
                    xAxis = Double(self.view.frame.width)
                }
                self.viewLPW.frame = CGRect(x: Int(xAxis), y: 0, width: Int(self.viewContain.frame.width), height: Int(self.viewContain.frame.height))
                UIView.animate(withDuration: 0.50, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: {
                    //Set x position what ever you want
                    self.viewLPW.frame = CGRect(x: 0, y: 0, width: self.viewContain.frame.width, height: self.viewContain.frame.height)
                }, completion: nil)
                
                self.verticalSlider.frame = CGRect(x: 05, y: 63, width: 30, height: 295)
                self.viewContain.addSubview(self.viewLPW)
                self.collectionLPW.reloadData()
            }
            
        }
        
        print("PlatDataONPermitted----------")
        
    }
    
    
    //MARK:
    //MARK:-----------IBAction
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnPrevious(_ sender: Any) {
        strSwipDirection = "right"
        if(currentIndex > 0){
            btnPrevious.alpha = 1.0
            currentIndex = currentIndex - 1
            setDataOnView(animation: true)
        }else{
            
            btnPrevious.alpha = 0.8
        }
        
    }
    @IBAction func actionOnNext(_ sender: Any) {
        
        strSwipDirection = "left"
        if(currentIndex < aryCollection.count - 1){
            btNext.alpha = 1.0
            currentIndex = currentIndex + 1
            setDataOnView(animation: true)
        }else{
            btNext.alpha = 0.8
        }
       
    }
    
}



//MARK:
//MARK:------------API CAlling
extension Permitted_LPW_VC {
    
    func callGetGetMeterYearReadingAPI() {
        
        self.lblTitle.text = ""
        self.lblMeterNumber.text = ""

        let dict = aryCollection.object(at: currentIndex)as! NSDictionary
        let meterNumber = "\(dict.value(forKey: "MeterID")!)"
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let loader = Global().loader_ShowObjectiveC(self, "Please wait...")
            let url = UrlGetGetMeterYearReading + "\(meterNumber)"
            WebServiceClass.callAPIBYGET(parameter: NSDictionary(), url: url) { (responce, status) in
               print(responce)
                
                loader!.dismiss(animated: false, completion: {
                    if(status){
                        if let _ = responce["ReadingDetail"] {
                            self.dataPloatOnPermitted_AND_LPW(dictReading: responce)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
            }
        }
    }
    
    func callUrlGetReportingPeriodDataAPI() {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let loader = Global().loader_ShowObjectiveC(self, "Please wait...")
            WebServiceClass.callAPIBYGET(parameter: NSDictionary(), url: UrlGetReportingPeriodData) { (responce, status) in
                loader!.dismiss(animated: false, completion: {
                    if(status){
                       // print(responce)
                        aryPeriodDataGloble = NSMutableArray()
                        aryPeriodDataGloble = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                        self.setDataOnView(animation: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
            }
        }
    }
}








// MARK: -
// MARK: - ----------------UICollectionViewDelegate

extension Permitted_LPW_VC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UIScrollViewDelegate{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(collectionView.tag == 99){ // Permitted
            return self.aryPermittedData.count
        }else{
           return self.aryLPWData.count
           
        }
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var identifire = ""
        if(collectionView.tag == 99){
            identifire = "Permitted"
        }else{
            identifire = "LPW"
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifire, for: indexPath as IndexPath) as! PermittedLPWCell
    
        //for Permitted
        if(identifire == "Permitted"){
            let dict = aryPermittedData.object(at: indexPath.row)as! NSDictionary
            cell.permittedLBL_1.text = "\(dict.value(forKey: "DisplayName")!)"
            let img = returnForPermittedImage(dict: dict, index: indexPath.row + 1)
            cell.permittedImage_1.alpha = 1
            if(img == UIImage(named: "endOfYear-1")){
                cell.permittedImage_1.alpha = 0.3
            }
            cell.permittedImage_1.image = img
     

        }else{
            let dict = aryLPWData.object(at: indexPath.row)as! NSDictionary
            cell.LPWLBL_1.text = "\(dict.value(forKey: "DisplayName")!)"
         
            let img = returnForLPWImage(dict: dict, index: indexPath.row + 1)
            cell.LPWImage_1.alpha = 1
            if(img == UIImage(named: "endOfYear-1")){
                cell.LPWImage_1.alpha = 0.3
            }
            cell.LPWImage_1.image = img
        }
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(collectionView.tag == 99){
            return CGSize(width: (collectionView.frame.size.width/3 - 10), height:(collectionView.frame.size.width/3))

        }else{
            return CGSize(width: (collectionView.frame.size.width/2 - 5), height:150)

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dict1 = aryCollection.object(at: currentIndex)as! NSDictionary
        let status = "\(dict1.value(forKey: "Status")!)"

        var dict = NSMutableDictionary()
        if(collectionView.tag == 99){
             dict = (aryPermittedData.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        }else{
             dict = (aryLPWData.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        }
        
        let CurrentMeterReading = "\(dict.value(forKey: "CurrentMeterReading")!)".replacingOccurrences(of: ".", with: "")
        
        // 1/25/2019 12:00:00 AM
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM/dd/yyy hh:mm:ss a"
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        
        let strStartDate = "\(dict.value(forKey: "ReadingDetailStartDate")!)"
        dateFormatter1.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter1.date(from: strStartDate)
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let strDateFormatted = dateFormatter1.string(from: dt!)
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        dateFormatter2.timeZone = TimeZone.current
        dateFormatter2.timeZone = TimeZone(abbreviation: "UTC")
        let myStartDate = dateFormatter2.date(from: strDateFormatted)!
        
        dateFormatter1.dateFormat = "MM/dd/yyy hh:mm:ss a"
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        
        let strEndDate = "\(dict.value(forKey: "ReadingDetailEndDate")!)"
        dateFormatter1.timeZone = TimeZone(abbreviation: "UTC")
        let dt1 = dateFormatter1.date(from: strEndDate)
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let strDateFormatted1 = dateFormatter1.string(from: dt1!)
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        dateFormatter2.timeZone = TimeZone.current
        dateFormatter2.timeZone = TimeZone(abbreviation: "UTC")
        let myEndDate = dateFormatter2.date(from: strDateFormatted1)!
        
        dateFormatter1.dateFormat = "MM/dd/yyy hh:mm:ss a"
        dateFormatter2.dateFormat = "MM/dd/yyyy"
        
        let date = Date()
        let dateString = dateFormatter2.string(from: date)
        let myCurrentDate = dateFormatter2.date(from: dateString)!
        let fallsBetween = (myStartDate ... myEndDate).contains(myCurrentDate)
        
        
        print(index)
        
        if(fallsBetween){
            if(status == "" || CurrentMeterReading == "0"){
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let obj = mainStoryboard.instantiateViewController(withIdentifier: "ReportMeterReadingViewController") as! ReportMeterReadingViewController
                  let strMeterNumber = "\(dict1.value(forKey: "MeterSerialNumber")!)"
                  let strMeterNamee = "\(dict1.value(forKey: "MeterNickName")!)"
               let MeterID = "\(dict1.value(forKey: "MeterID")!)"
                var strName = ""
                if(strMeterNumber.count == 0){
                    if(strMeterNamee.count == 0){
                        strName = ""
                    }else{
                           strName = strMeterNamee
                    }
                }else{
                    if(strMeterNamee.count == 0){
                       strName = strMeterNumber
                    }else{
                        strName = strMeterNamee + "-" + strMeterNumber
                    }
                }

                
                obj.strUnit = ""
                if (dict1.value(forKey: "Unit") is String){
                   obj.strUnit = "\(dict1.value(forKey: "Unit")!)"
                }
                obj.strMeteridDefault = MeterID
              obj.strMeterNickNamenSerialNoDefault=strName;
                
//                let dict = aryCollection.object(at: currentIndex)as! NSDictionary
//                obj.strMeterNickNamenSerialNoDefault = "\(dict.value(forKey: "MeterSerialNumber")!)"
                
                
                obj.dictDataManageMeterList = (dict1 as! [AnyHashable : Any])
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
}

class PermittedLPWCell: UICollectionViewCell {
    //LPW
    @IBOutlet weak var LPWImage_1: UIImageView!
    @IBOutlet weak var LPWLBL_1: UILabel!
    //Permitted
    @IBOutlet weak var permittedImage_1: UIImageView!
    @IBOutlet weak var permittedLBL_1: UILabel!
 
}



extension String {
    /// Returns a date from a string in MMMM dd, yyyy. Will return today's date if input is invalid.
    var asDate: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy"
        return formatter.date(from: self) ?? Date()
    }
}
extension Date {
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
}




