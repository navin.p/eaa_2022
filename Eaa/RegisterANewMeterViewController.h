//
//  RegisterANewMeterViewController.h
//  Eaa
//
//  Created by Akshay Hastekar on 7/31/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "Header.h"
#import "DejalActivityView.h"
@interface RegisterANewMeterViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textFieldMeterSerialNo;

@property (weak, nonatomic) IBOutlet UITextField *textFieldMeterReading;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewNewMeter;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewNewCalibrationImage;
@property (weak, nonatomic) IBOutlet UIButton *buttonDatePicker;
@property (strong,nonatomic) NSDictionary *dictOldMeterInfo;
@property (strong,nonatomic) NSString *strNewMeterImageName;
@property (strong,nonatomic) UIImage *ImageMeter1;
@property (strong,nonatomic) NSString *strImageMeter1Name;
@property (weak, nonatomic) IBOutlet UILabel *lblThankYouMsg;
@property (weak, nonatomic) IBOutlet UIView *viewThankYou;
@property (strong,nonatomic) NSString *strMeterSerialNo;
@property (strong,nonatomic) NSString *strMeterNickName;

@end
