//
//  RegisterANewMeterViewController.m
//  Eaa
//
//  Created by Akshay Hastekar on 7/31/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "RegisterANewMeterViewController.h"
#import "DashBoardViewController.h"
#import "HelpViewGlobalViewController.h"
#import "HelpViewController.h"
static int imageUploadCount = 0;
@interface RegisterANewMeterViewController ()
{
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    Global *global;
    NSString *strImageType;
    NSString *strMeterImageName;
    NSString *strCalibrationImageName;
    NSString *strUserID;
    NSMutableArray *arrayImages;
    NSMutableDictionary *dictJsonData;
    NSString *currentDate;
}
@end

@implementation RegisterANewMeterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // set current date on button date
    
    _textFieldMeterSerialNo.text=_strMeterSerialNo;
    
    _textFieldMeterSerialNo.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    strMeterImageName = @"";
    strCalibrationImageName = @"";
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    [_buttonDatePicker setTitle:[dateFormat stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm:mm"];
    currentDate = [dateFormat stringFromDate:[NSDate date]];
    
    //memory allocation of global class object
    global = [[Global alloc]init];
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    strUserID = [dictResponse valueForKey:@"UserId"];
    arrayImages = [[NSMutableArray alloc]init];
    dictJsonData = [[NSMutableDictionary alloc]init];
    
    if(_strImageMeter1Name.length>0 && _ImageMeter1 != nil)// image taken from replace a meter screen
    {
        NSDictionary *dictImage = @{@"imageName": _strImageMeter1Name,
                                    @"image": _ImageMeter1
                                    };
        [arrayImages addObject:dictImage];
    }
}

#pragma mark - UIButton action

- (IBAction)actionOnDoneThankYou:(id)sender
{
    [_viewThankYou removeFromSuperview];
    for(UIViewController *vc in self.navigationController.viewControllers)
    {
       if([vc isKindOfClass:[DashBoardViewController class]])
       {
           [self.navigationController popToViewController:vc animated:YES];
       }
    }
}

- (IBAction)actionOnHelp:(id)sender
{
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                             bundle: nil];
//    HelpViewGlobalViewController
//    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewGlobalViewController"];
//    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HelpViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    objByProductVC.strHelpMessage = @"Allows you to add a meter you have registered with the EAA into your App menu.  This feature also allows you to inform the EAA if you are adding a meter to your App menu that is not already registered with the EAA.\n\nIf you are adding a meter as part of Replacing a Meter, please use Replace a Meter to report this action to the EAA.\n\n1.  Enter the Meter Serial Number, as registered with the EAA (or the serial number of your new meter).\n\n2.  Enter a Meter Nickname, to easily identify the meter in the future (Optional)\n\n 3.  Indicate (Yes/No) if this meter is already registered with the EAA.\n\n4.  Submit your entry.\n\nNote: If this a new meter that is not registered with the EAA, you will be taken to a Register a New Meter screen to complete the registration process.";
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
- (IBAction)actionOnHelpSerialNumber:(id)sender
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Help!"
                               message:@"1.If the serial number contains only numbers, it can be maximum 13 characters long.\n 2.If the serial number is a combination of characters, numbers and hyphen (-), it's maximum length is 17 characters."
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];

}

- (IBAction)actionOnDatePicker:(id)sender
{
    [self addPickerViewDateTo];
}
- (IBAction)actionOnCameraNewMeter:(id)sender
{
    strImageType = @"NewMeter";
    [self openCamera];
}
- (IBAction)actionOnCameraNewMeterCalibration:(id)sender
{
    strImageType = @"CalibrationNewMeter";
    [self openCamera];
}
- (IBAction)actionOnSubmit:(id)sender
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Submitting Request..."];
    [self performSelector:@selector(makeRequest) withObject:self afterDelay:0.2];
}
-(void)makeRequest
{
    NSString *strMessage = [self validationCheck];
    if(strMessage.length>0)
    {
        [DejalBezelActivityView removeView];
        [global AlertMethod:@"Alert!" :strMessage];
    }
    else
    {
        /*,
        
         NSDictionary *dictLoginResponse=[userDefaults objectForKey:@"LoginData"];
         
         NSDictionary *dictJson = @{@"AccuracyTestID":@"0",
         @"AuthId":[dictLoginResponse valueForKey:@"AuthId"],
         @"CustomerId":[dictLoginResponse valueForKey:@"UserId"],
         @"MeterId":@"0",
         @"SubmittedDateFrom":@"01/01/1990",
         @"SubmittedDateTo":strCurrentDate,
         @"TestDateFrom":@"01/01/1990",
         @"TestDateTo":strCurrentDate
         };
         
*/
        NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginResponse=[userDefaults objectForKey:@"LoginData"];
      
        NSDictionary *dictNewMeterInfo = @{
                                           @"CalibrationCertImage":strCalibrationImageName,
                                           @"CustomerId":strUserID,
                                           @"InitialMeterReading":_textFieldMeterReading.text,
                                           @"InstalledDate":_buttonDatePicker.titleLabel.text,
                                           @"MeterId":@"0",
                                           @"MeterImages":strMeterImageName,
                                           @"MeterNumber":[_textFieldMeterSerialNo.text uppercaseString],
                                           
                                           @"MeterName":_strMeterNickName,
                                           @"MeterLat":@"0.0",
                                           @"MeterLong":@"0.0",
                                           @"CreatedDate":currentDate,
                                           @"CreatedBy":[dictLoginResponse valueForKey:@"AuthId"],
                                           @"ModifyBy":[dictLoginResponse valueForKey:@"AuthId"],
                                           @"MeterStatus":@"New",
                                           @"ModifyDate":currentDate
                                           };
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dictNewMeterInfo])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictNewMeterInfo options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Add Meter Reading JSON: %@", jsonString);
            }
        }
        if(arrayImages.count>0)
        {
            for(int i=0 ;i<arrayImages.count;i++)
            {
                [self uploadImage:[[arrayImages objectAtIndex:i] valueForKey:@"imageName"] :jsonString];
            }
        }
        else
        {
            [self sendingFinalJson:jsonString];
        }
    }
}

#pragma mark - UIImagePickerController delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    
    if([strImageType isEqualToString:@"NewMeter"])
    {
        strMeterImageName = [NSString stringWithFormat:@"MeterImgg%@%@.jpg",strDate,strTime];
    }
    else
    {
        strCalibrationImageName = [NSString stringWithFormat:@"MeterImgg%@%@.jpg",strDate,strTime];
    }
    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    if (image.imageOrientation==UIImageOrientationUp)
    {
        NSLog(@"UIImageOrientationUp");
    }
    else if (image.imageOrientation==UIImageOrientationLeft)
    {
        NSLog(@"UIImageOrientationLeft");
    }
    else if (image.imageOrientation==UIImageOrientationRight)
    {
        NSLog(@"UIImageOrientationRight");
    }
    else if (image.imageOrientation==UIImageOrientationDown)
    {
        NSLog(@"UIImageOrientationDown");
    }
    
    [UIImage imageWithCGImage:[image CGImage]
                        scale:[image scale]
                  orientation: UIImageOrientationUp];
    
    if([strImageType isEqualToString:@"NewMeter"])
    {
       _imageViewNewMeter.image = image;
         [self resizeImage:image :strMeterImageName];
        NSDictionary *dictImage = @{@"imageName": strMeterImageName,
                                    @"image": image
                                    };
        [arrayImages addObject:dictImage];
    }
    else
    {
        _imageViewNewCalibrationImage.image = image;
         [self resizeImage:image :strCalibrationImageName];
        NSDictionary *dictImage = @{@"imageName": strCalibrationImageName,
                                    @"image": image
                                    };
        [arrayImages addObject:dictImage];
    }
    
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}

-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = actualHeight/2;
    float maxWidth = actualWidth/2;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
}


-(void)uploadImage :(NSString*)strImageName :(NSString*)jsonString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];
    
    NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",MainUrl,UrlImageUpload];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    NSError *jsonError;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSArray *json = [NSJSONSerialization JSONObjectWithData:returnData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    
    //NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
//    if ([returnString isEqualToString:@"OK"])
//    {
//        NSLog(@"Image Sent");
//        imageUploadCount++;
//    }
//    NSLog(@"Image Sent === %@",returnString);
    
    if([[[json objectAtIndex:0] valueForKey:@"Name"] length]>0)
    {
          imageUploadCount++;
    }
   
    if(imageUploadCount == arrayImages.count)// after no.of images uploaded,send json string to server
    {
        imageUploadCount = 0;
        [self sendingFinalJson:jsonString];
    }
}

-(void)sendingFinalJson :(NSString*)jsonString
{
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlRegisterNewMeter];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             [DejalBezelActivityView removeView];
             NSLog(@"%@",response);
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     [self showThankYouView:[[response valueForKey:@"Error"] objectAtIndex:0]];
                 }
                 else
                 {
                     [DejalBezelActivityView removeView];
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}

#pragma mark - UIButton action

- (IBAction)actionOnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.datePickerMode = UIDatePickerModeDate;
    pickerDate.frame=CGRectMake(0,0, self.view.frame.size.width, 350);
    pickerDate.datePickerMode =UIDatePickerModeDate;
    pickerDate.maximumDate = [NSDate date];
    
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor clearColor];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width, ([UIScreen mainScreen].bounds.size.height*53.52)/100)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width*14.0625/100.0), lblLine.frame.origin.y+5, 100, 35)];
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btnClose.frame)+30, btnClose.frame.origin.y, 100,35)];
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)tapDetectedOnBackGroundView
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    //  strDate = [dateFormat stringFromDate:pickerDate.date];
    [_buttonDatePicker setTitle:[dateFormat stringFromDate:pickerDate.date] forState:UIControlStateNormal];
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(NSString*)validationCheck
{
    NSString *strMessage = @"";
    
    if(!(_textFieldMeterSerialNo.text.length>0))
    {
       strMessage = @"Please Enter Meter Serial Number";
    }
    else if(!(_buttonDatePicker.titleLabel.text.length>0))
    {
      strMessage = @"Please Select Installed Date";
    }
    else if(!(_textFieldMeterReading.text.length>0))
    {
      strMessage = @"Please Enter Meter Reading";
    }
    UIImage *imageToCheckFor = [UIImage imageNamed:@"NoImage.png"];
    
    UIImage *img = _imageViewNewMeter.image;
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        strMessage = @"Please Take Picture Of New Meter";
    }
    return strMessage;
}

-(void)replaceMeterOnServer
{
    
}
-(void)openCamera
{
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=self;
    imagePickController.allowsEditing=false;
    [self presentViewController:imagePickController animated:YES completion:nil];
}
-(void)showThankYouView:(NSString*)strMsg
{
    CGRect frameForHiddenView=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    frameForHiddenView.origin.x=0;
    frameForHiddenView.origin.y=0;
    [_viewThankYou setFrame:frameForHiddenView];
    _lblThankYouMsg.text = strMsg;
    [self.view addSubview:_viewThankYou];
}

#pragma mark - UITextField delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
