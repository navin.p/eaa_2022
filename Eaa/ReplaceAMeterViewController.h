//
//  ReplaceAMeterViewController.h
//  Eaa
//
//  Created by Akshay Hastekar on 7/27/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "RegisterANewMeterViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "ReplaceAMeter2ViewController.h"
@interface ReplaceAMeterViewController : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate>
{
    // variables
    AppDelegate *appDelegate;
    NSEntityDescription *entityManageMeterList,*entityDateReading;
    NSManagedObjectContext *contextManageMeterList,*contextDateReading;
    NSFetchRequest *requestManageMeterList,*requestDateReading;
    NSSortDescriptor *sortDescriptorManageMeterList,*sortDescriptorDateReading;
    NSArray *sortDescriptorsManageMeterList,*sortDescriptorsDateReading;
    NSManagedObject *matchesManageMeterList,*matchesDateReading;
    NSArray *arrAllObjManageMeterList,*arrAllObjDateReading;
}
// outlets

@property (weak, nonatomic) IBOutlet UIButton *buttonDatePicker;

@property (weak, nonatomic) IBOutlet UITextField *texstFieldMeterReading;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewMeter;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectMeter;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerManageMeterList;
@end
