//
//  ReplaceAMeterViewController.m
//  Eaa
//
//  Created by Akshay Hastekar on 7/27/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "ReplaceAMeterViewController.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "HelpViewGlobalViewController.h"
#import "HelpViewController.h"
#import "ManageMeterList+CoreDataProperties.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface ReplaceAMeterViewController ()
{
    Global *global;
    NSArray *arrManageMeterListToShow;
    UITableView *tableViewMeterList;
    UIView *viewBackGround;
    NSString *strMeterID,*strNewMeterImageName,*strMeterName,*strMeterNumber,*strRemoveMeterReading;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    NSDictionary *dictOldMeterInfo;
    CLLocationManager *locationManager;
    NSString *latitude,*longitude;
    NSString *strCustomerId;
    NSString *strAuthID;
}
@end

@implementation ReplaceAMeterViewController

#pragma mark - view's life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    global = [[Global alloc] init];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    [_buttonDatePicker setTitle:[dateFormat stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    
    tableViewMeterList=[[UITableView alloc]init];
    tableViewMeterList.dataSource=self;
    tableViewMeterList.delegate=self;
    [tableViewMeterList.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tableViewMeterList.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tableViewMeterList.layer setBorderWidth: 2.0];
    tableViewMeterList.layer.cornerRadius=20;
    tableViewMeterList.clipsToBounds = YES;
    
    //hide extra separators
    tableViewMeterList.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [self fetchFromCoreDataManageMeterList];
    }
    else
    {
        [self FetchManageMeterList];
    }
    [self configureTextFields];
}

-(void)configureTextFields
{
    _texstFieldMeterReading.layer.cornerRadius = 2.0;
    _texstFieldMeterReading.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _texstFieldMeterReading.layer.borderWidth = 1.0;
    [Global addPaddingView:_texstFieldMeterReading];
}
#pragma mark - UITableView's delegate and data source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrManageMeterListToShow count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UIScreen mainScreen].bounds.size.height==667)
    {
        return 50;
    }
    else if ([UIScreen mainScreen].bounds.size.height==736)
    {
        return 50;
    }
    else
        return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tableViewMeterList dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (!(arrManageMeterListToShow.count==0))
    {
        
        NSInteger i;
        i=tableViewMeterList.tag;
        switch (i)
        {
            case 101:
            {
                NSDictionary *dictdata=arrManageMeterListToShow[indexPath.row];
                
                NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNumber"]];
                NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterName"]];
                
                NSString *strNameToShow;
                
                if (strMeterNumber.length==0) {
                    
                    if (strMeterNamee.length==0) {
                        
                        strNameToShow=@"";
                    }
                    else
                    {
                        strNameToShow=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterName"]];
                    }
                }
                else
                {
                    
                    if (strMeterNamee.length==0) {
                        
                        strNameToShow=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNumber"]];
                        
                    } else {
                        
                        strNameToShow=[NSString stringWithFormat:@"%@-%@",[dictdata valueForKey:@"MeterName"],[dictdata valueForKey:@"MeterNumber"]];
                        
                    }
                }
                
                cell.textLabel.text=strNameToShow;
                cell.textLabel.numberOfLines=2;
                break;
            }
            case 102:
            {
                break;
            }
            default:
                break;
        }
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!(arrManageMeterListToShow.count==0))
    {
        NSInteger i;
        i=tableViewMeterList.tag;
        switch (i)
        {
            case 101:
            {
                NSDictionary *dictdata=arrManageMeterListToShow[indexPath.row];
                NSString *strNameToShow=[NSString stringWithFormat:@"%@-%@",[dictdata valueForKey:@"MeterName"],[dictdata valueForKey:@"MeterNumber"]];
                
                NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
                NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
                NSLog(@"%@",strUserType);
                
                strMeterID=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterId"]];
                strMeterName = [dictdata valueForKey:@"MeterName"];
                strMeterNumber = [dictdata valueForKey:@"MeterNumber"];
                strRemoveMeterReading = [dictdata valueForKey:@"RemoveMeterReading"];
                [_buttonSelectMeter setTitle:strNameToShow forState:UIControlStateNormal];
                
                break;
            }
            case 102:
            {
                break;
            }
                
            default:
                break;
        }
    }
    
    [viewBackGround removeFromSuperview];
    [tableViewMeterList removeFromSuperview];
}

#pragma mark - UIImagePickerController delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    strNewMeterImageName = [NSString stringWithFormat:@"MeterImgg%@%@.jpg",strDate,strTime];
    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    if (image.imageOrientation==UIImageOrientationUp)
    {
        NSLog(@"UIImageOrientationUp");
    }
    else if (image.imageOrientation==UIImageOrientationLeft)
    {
        NSLog(@"UIImageOrientationLeft");
    }
    else if (image.imageOrientation==UIImageOrientationRight)
    {
        NSLog(@"UIImageOrientationRight");
    }
    else if (image.imageOrientation==UIImageOrientationDown)
    {
        NSLog(@"UIImageOrientationDown");
    }
    
    UIImage *imageToDisplay =
    [UIImage imageWithCGImage:[image CGImage]
                        scale:[image scale]
                  orientation: UIImageOrientationUp];
    
    _imageViewMeter.image = image;
    
    [self resizeImage:image :strNewMeterImageName];
    
    //[self saveImage:image :strMeterImageNames];
    
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}
-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = actualHeight/2;
    float maxWidth = actualWidth/2;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
}

#pragma mark - UIButton action

- (IBAction)actionOnHelp:(id)sender
{
    /*   <p>Allows you to report a meter replacement or a meter repair using the App.  This is used for a meter that is already registered in the App. All steps are required to perform this function unless denoted as Optional.</p>
     <p><b>Current Meter Information</b></p>
     
     <p>1. Select the meter you wish to report.</p>
     <p>2. Enter the date the meter was removed.</p>
     <p>3. Enter the meter reading when removed.</p>
     <p>4. Photograph the meter register from the removed meter.</p>
     
     <p><b>Replaced / Repaired Meter Information</b></p>
     
     <p>1. Enter meter serial number (will be different if replaced).</p>
     <p>2. Enter the date the meter was installed.</p>
     <p>3. Enter the meter reading when installed.</p>
     <p>4. Photograph the meter register from the replaced meter.</p>
     <p>5. Photograph the replaced meter Certificate of Calibration. (Optional)</p>
     <p>6. Submit your entry.</p>*/
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HelpViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
   
//     objByProductVC.strHelpMessage = @"Allows you to report a meter replacement or a meter repair using the App.  This is used for a meter that is already registered in the App. All steps are required to perform this function unless denoted as Optional.\n\nCurrent Meter Information \n\n1. Select the meter you wish to report.\n\n2. Enter the date the meter was removed.\n\n3. Enter the meter reading when removed.\n\n4. Photograph the meter register from the removed meter.Replaced / Repaired Meter Information \n\n1. Enter meter serial number (will be different if replaced).\n\n2. Enter the date the meter was installed.\n\n3. Enter the meter reading when installed.\n\n4. Photograph the meter register from the replaced meter.\n\n5. Photograph the replaced meter Certificate of Calibration. (Optional)\n\n6. Submit your entry.";
    
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
    NSString *title = @"Allows you to report a meter replacement or a meter repair using the App.This is used for a meter that is already registered in the App. All steps are required to perform this function unless denoted as Optional.\n\nCurrent Meter Information \n\n1. Select the meter you wish to report.\n\n2. Enter the date the meter was removed.\n\n3. Enter the meter reading when removed.\n\n4. Photograph the meter register from the removed meter.\n\nReplaced / Repaired Meter Information \n\n1. Enter meter serial number (will be different if replaced).\n\n2. Enter the date the meter was installed.\n\n3. Enter the meter reading when installed.\n\n4. Photograph the meter register from the replaced meter.\n\n5. Photograph the replaced meter Certificate of Calibration. (Optional)\n\n6. Submit your entry.";
     
     NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:title];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont systemFontOfSize:14.0 weight:UIFontWeightRegular]
                             range:[title rangeOfString:@"Allows you to report a meter replacement or a meter repair using the App.  This is used for a meter that is already registered in the App. All steps are required to perform this function unless denoted as Optional."]];

    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont systemFontOfSize:14.0 weight:UIFontWeightRegular]
                             range:[title rangeOfString:@"1. Select the meter you wish to report.\n\n2. Enter the date the meter was removed.\n\n3. Enter the meter reading when removed.\n\n4. Photograph the meter register from the removed meter."]];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont systemFontOfSize:14.0 weight:UIFontWeightRegular]
                             range:[title rangeOfString:@"1. Enter meter serial number (will be different if replaced).\n\n2. Enter the date the meter was installed.\n\n3. Enter the meter reading when installed.\n\n4. Photograph the meter register from the replaced meter.\n\n5. Photograph the replaced meter Certificate of Calibration. (Optional)\n\n6. Submit your entry."]];
    
     [attributedString addAttribute:NSFontAttributeName
     value:[UIFont systemFontOfSize:14.0 weight:UIFontWeightBold]
     range:[title rangeOfString:@"Current Meter Information"]];
    
     [attributedString addAttribute:NSFontAttributeName
     value:[UIFont systemFontOfSize:14.0 weight:UIFontWeightBold]
     range:[title rangeOfString:@"Replaced / Repaired Meter Information"]];
    
    objByProductVC.strHelpBoldMessage = attributedString;
    
    // self.tagCommentLabel.attributesText = attributedString;
}

- (IBAction)actionOnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionOnCamera:(id)sender
{
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=self;
    imagePickController.allowsEditing=false;
    [self presentViewController:imagePickController animated:YES completion:nil];
}

- (IBAction)actionOnSubmit:(id)sender
{
    NSString *strMessage = [self validationCheck];
   
    if(strMessage.length>0)
    {
        [global AlertMethod:@"Alert!" :strMessage];
    }
    else
    {
        dictOldMeterInfo = @{ @"MeterId":strMeterID,
                              @"MeterLat":latitude,
                              @"MeterLong":longitude,
                              @"MeterName":strMeterName,
                              @"MeterNumber":strMeterNumber,
                              @"RemoveDate":_buttonDatePicker.titleLabel.text,
                              @"MeterStatus":@"Active",
                              @"RemoveMeterImage":strNewMeterImageName,
                              @"RemoveMeterReading":_texstFieldMeterReading.text,
                              @"RemovedBy":strAuthID
                             };
        
        [self performSegueWithIdentifier:@"ReplaceAMeter2ViewController" sender:nil];
    }
}
- (IBAction)actionOnSelectMeter:(id)sender
{
    [viewBackGround removeFromSuperview];
    [tableViewMeterList removeFromSuperview];
    
    if (arrManageMeterListToShow.count==0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailableMeterName;
        [global AlertMethod:strTitle :strMsg];
    }
    else
    {
        tableViewMeterList.tag=101;
        [self tableLoad:tableViewMeterList.tag];
    }
}

-(void)btnCancelAction:(id)sender
{
    [viewBackGround removeFromSuperview];
    [tableViewMeterList removeFromSuperview];
}

- (IBAction)actionOnDatePicker:(id)sender
{
     [self addPickerViewDateTo];
}

#pragma mark - CLLocation Manger delegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)alocations
{
    
    CLLocation *currentLocation;
    currentLocation =[alocations lastObject];
    
    latitude = [NSString stringWithFormat:@"%.7f",currentLocation.coordinate.latitude];
    // commented by akshay to take lat in decimal format
   // int degrees = currentLocation.coordinate.latitude;
//    double decimal = fabs(currentLocation.coordinate.latitude - degrees);
//    int minutes = decimal * 60;
//    double seconds = decimal * 3600 - minutes * 60;
//    
//    latitude = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
//                degrees, minutes, seconds];
    
    // locationLatitude = currentLocation.coordinate.latitude;
//    degrees = currentLocation.coordinate.longitude;
//    decimal = fabs(currentLocation.coordinate.longitude - degrees);
//    minutes = decimal * 60;
//    seconds = decimal * 3600 - minutes * 60;
//    longitude = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
//                  degrees, minutes, seconds];
    longitude = [NSString stringWithFormat:@"%.7f",currentLocation.coordinate.longitude];
    [locationManager stopUpdatingLocation];
    
}
#pragma mark - API Calling

#pragma mark - fetch meter list from server
-(void)FetchManageMeterList
{
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    if (dictLoginDetail.count==0)
    {
        
    }
    else
    {
        NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
        NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
        
        strAuthID = [dictResponse valueForKey:@"AuthId"];
        if ([strUserType isEqualToString:@"Customer"])
        {
            strCustomerId=[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]];
        }
        else
        {
            strCustomerId=@"0";
        }
        
        NSArray *objects = [NSArray arrayWithObjects:
                            strCustomerId,
                            @"0",
                            @"",
                            @"",
                            @"",nil];
        
        NSArray *keys = [NSArray arrayWithObjects:
                         @"CustomerId",
                         @"MeterId",
                         @"MeterName",
                         @"MeterNo",
                         @"CustomerName",nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Get Manage Meter List JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlGetManageMeterList];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Meters..."];
        
        //============================================================================
        //============================================================================
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrlPostMethod:strUrl :@"ManageMeterAPI" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         

             // commented by Akshay on 11 Jan 2018
//                         arrManageMeterListToShow = (NSArray*)response;
//                         NSLog(@"%@",arrManageMeterListToShow);
                        
                         
                         [self afterServerResponseOnLogin:response];
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
    }
}

#pragma mark - Fetch meter list from local db
-(void)fetchFromCoreDataManageMeterList
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList];
    requestManageMeterList = [[NSFetchRequest alloc] init];
    [requestManageMeterList setEntity:entityManageMeterList];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestManageMeterList setPredicate:predicate];
    
    sortDescriptorManageMeterList = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsManageMeterList = [NSArray arrayWithObject:sortDescriptorManageMeterList];
    
    [requestManageMeterList setSortDescriptors:sortDescriptorsManageMeterList];
    
    self.fetchedResultsControllerManageMeterList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestManageMeterList managedObjectContext:contextManageMeterList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerManageMeterList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerManageMeterList performFetch:&error];
    arrAllObjManageMeterList = [self.fetchedResultsControllerManageMeterList fetchedObjects];
    if ([arrAllObjManageMeterList count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailableMeterName;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
    }
    else
    {
        matchesManageMeterList = arrAllObjManageMeterList[0];
        arrManageMeterListToShow=[matchesManageMeterList valueForKey:@"arrOfManageMeterList"];
        
        if (!(arrAllObjManageMeterList.count==0)) {
            
            //            NSDictionary *dictdata=arrManageMeterListToShow[0];
            //            [_btnSelectMeter setTitle:[dictdata valueForKey:@"MeterName"] forState:UIControlStateNormal];
            //            strMeterIdGlobal=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterId"]];
        }
        [tableViewMeterList reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)tableLoad:(NSInteger)btntag
{
    NSInteger i;
    i=101;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
            
        default:
            break;
    }
    [tableViewMeterList setContentOffset:CGPointZero animated:YES];
    tableViewMeterList.dataSource=self;
    tableViewMeterList.delegate=self;
    tableViewMeterList.backgroundColor=[UIColor whiteColor];
    [tableViewMeterList reloadData];
}

-(void)setTableFrame
{
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tableViewMeterList.frame=CGRectMake(20, CGRectGetMaxY(_buttonSelectMeter.frame), [UIScreen mainScreen].bounds.size.width-40, ([UIScreen mainScreen].bounds.size.height*60)/100);
  
//    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
//    {
//        tableViewMeterList.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
//    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tableViewMeterList];
    
    UIButton *btnCancel=[[UIButton alloc]initWithFrame:CGRectMake(tableViewMeterList.frame.origin.x, tableViewMeterList.frame.origin.y+tableViewMeterList.frame.size.height+20, tableViewMeterList.frame.size.width, 42)];
    
    [btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCancel.backgroundColor=[UIColor colorWithRed:69.0f/255 green:88.0f/255 blue:103.0f/255 alpha:1];
    [btnCancel addTarget:self action:@selector(btnCancelAction:) forControlEvents:UIControlEventTouchDown];
    [viewBackGround addSubview:btnCancel];
    
}

-(void)afterServerResponseOnLogin :(NSDictionary*)dictResponse{
    
    // work for showing only Active Meter status//
    NSArray *arrayTemp=(NSArray*)(NSArray*)dictResponse;
    NSMutableArray *arrayOnlyActiveMeterList = [NSMutableArray new];
    
    for(int i=0;i<arrayTemp.count;i++)
    {
        if([[[arrayTemp objectAtIndex:i] valueForKey:@"MeterStatus"] isEqualToString:@"Active"])
        {
            [arrayOnlyActiveMeterList addObject:[arrayTemp objectAtIndex:i]];
        }
    }
    
    arrManageMeterListToShow = (NSArray*)arrayOnlyActiveMeterList;
    //////////////////////////
    
    
    if (arrManageMeterListToShow.count==0) {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailableMeterName
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        if (!(arrAllObjManageMeterList.count==0)) {
            
        }
        
          [self saveToCoreDataLeads:arrManageMeterListToShow];
        
        
    }
}
-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.datePickerMode = UIDatePickerModeDate;
    pickerDate.frame=CGRectMake(0,0, self.view.frame.size.width, 350);
    pickerDate.datePickerMode =UIDatePickerModeDate;
    pickerDate.maximumDate = [NSDate date];
    
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor clearColor];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width, ([UIScreen mainScreen].bounds.size.height*53.52)/100)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width-230)/2, lblLine.frame.origin.y+5, 100, 35)];
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btnClose.frame)+30, btnClose.frame.origin.y, 100,35)];
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:69.0/255.0 green:88.0/255.0 blue:103.0/255.0 alpha:1.0];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)tapDetectedOnBackGroundView
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    //  strDate = [dateFormat stringFromDate:pickerDate.date];
    [_buttonDatePicker setTitle:[dateFormat stringFromDate:pickerDate.date] forState:UIControlStateNormal];
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}

-(NSString*)validationCheck
{
    NSString *strMessage = @"";
    UIImage *imageToCheckFor = [UIImage imageNamed:@"NoImage.png"];
    
    UIImage *img = _imageViewMeter.image;
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    if([_buttonSelectMeter.titleLabel.text isEqualToString:@"---Select Meter---"])
    {
        strMessage = @"Please Select Meter";
    }
    else if (!(_buttonDatePicker.titleLabel.text.length>0))
    {
        strMessage = @"Please Select Meter Removed Date";
    }
    else if (!(_texstFieldMeterReading.text.length>0))
    {
        strMessage = @"Please Enter Meter Reading";
    }
    else if (_imageViewMeter.image == nil)
    {
        strMessage = @"Please Enter Meter Reading";
    }
    else if(isCompare==true)
    {
         strMessage = @"Please take picture to continue";
    }
    return strMessage;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   if([segue.identifier isEqualToString:@"ReplaceAMeter2ViewController"])
   {
       ReplaceAMeter2ViewController *vc = (ReplaceAMeter2ViewController*)[segue destinationViewController];
       vc.dictOldMeterInfo = dictOldMeterInfo;
       vc.ImageMeter1 = _imageViewMeter.image;
       vc.strImageMeter1Name = strNewMeterImageName;
       
   }
}

-(void)saveToCoreDataLeads :(NSArray*)arrOfManageMeterList{
    
    //==================================================================================
    //==================================================================================
    
    [self deleteManageMeterListFromDB];
    
    //==================================================================================
    //==================================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList];
    
    //==================================================================================
    //==  ================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    ManageMeterList *objManageMeterList = [[ManageMeterList alloc]initWithEntity:entityManageMeterList insertIntoManagedObjectContext:contextManageMeterList];
    objManageMeterList.userName=[dictLoginDetail valueForKey:@"UserName"];
    objManageMeterList.userType=@"";
    objManageMeterList.arrOfManageMeterList=arrOfManageMeterList;
    NSError *error1;
    [contextManageMeterList save:&error1];
    
    //==================================================================================
    //==================================================================================
}

-(void)deleteManageMeterListFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [contextManageMeterList executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [contextManageMeterList deleteObject:data];
        }
        NSError *saveError = nil;
        [contextManageMeterList save:&saveError];
    }
    
}
@end
