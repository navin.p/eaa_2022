//
//  ReportAnAccuracyTestViewController.h
//  Eaa
//
//  Created by Akshay Hastekar on 7/29/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Global.h"
#import "DejalActivityView.h"
#import "Header.h"
@interface ReportAnAccuracyTestViewController : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>
{
    // variables
    AppDelegate *appDelegate;
    NSEntityDescription *entityManageMeterList,*entityDateReading;
    NSManagedObjectContext *contextManageMeterList,*contextDateReading;
    NSFetchRequest *requestManageMeterList,*requestDateReading;
    NSSortDescriptor *sortDescriptorManageMeterList,*sortDescriptorDateReading;
    NSArray *sortDescriptorsManageMeterList,*sortDescriptorsDateReading;
    NSManagedObject *matchesManageMeterList,*matchesDateReading;
    NSArray *arrAllObjManageMeterList,*arrAllObjDateReading;
}
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectMeter;

@property (weak, nonatomic) IBOutlet UIButton *buttonDatePicker;
@property (weak, nonatomic) IBOutlet UITextView *textViewNotes;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewMeter;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerManageMeterList;

@property (weak, nonatomic) IBOutlet UILabel *lblThankYouMessage;
@property (weak, nonatomic) IBOutlet UIView *viewThankYou;

@end
