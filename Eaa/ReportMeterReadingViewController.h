//
//  ReportMeterReadingViewController.h
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>

@interface ReportMeterReadingViewController : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityManageMeterList,*entityDateReading;
    NSManagedObjectContext *contextManageMeterList,*contextDateReading;
    NSFetchRequest *requestManageMeterList,*requestDateReading;
    NSSortDescriptor *sortDescriptorManageMeterList,*sortDescriptorDateReading;
    NSArray *sortDescriptorsManageMeterList,*sortDescriptorsDateReading;
    NSManagedObject *matchesManageMeterList,*matchesDateReading;
    NSArray *arrAllObjManageMeterList,*arrAllObjDateReading;
    CLLocationManager *locationManager;

}

@property (strong, nonatomic) IBOutlet UIView *ThankYouVieww;
- (IBAction)action_DoneThankYou:(id)sender;
- (IBAction)action_Submit:(id)sender;
- (IBAction)action_SelectMeter:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectMeter;
- (IBAction)action_Back:(id)sender;
- (IBAction)action_Help:(id)sender;
- (IBAction)action_Camera:(id)sender;
- (IBAction)hideKeyboard:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *meterImgView;
@property (strong, nonatomic) IBOutlet UITextField *txtMeterReading;
@property (strong, nonatomic) IBOutlet UITextField *txtUnit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthTxtUnit;

@property (strong, nonatomic) IBOutlet UIView *popUpView;
@property (strong, nonatomic) IBOutlet UIImageView *popUpImgView;
- (IBAction)action_CancelPopUp:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_MeterReading;
@property (strong, nonatomic) IBOutlet UILabel *lbl_MeterNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_DateSubmitted;
- (IBAction)action_ConfirmPopUp:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerManageMeterList;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerDateReading;
@property (strong, nonatomic) NSDictionary  *dictDataManageMeterList;
@property (strong, nonatomic) NSString  *strMeterNickNamenSerialNoDefault;
@property (strong, nonatomic) NSString  *strMeteridDefault;
@property (strong, nonatomic) NSString  *strUnit;
@property (strong, nonatomic) NSString  *strComeFrom;

@property (strong, nonatomic) IBOutlet UILabel *lbl_SelectMeterText;

@end
