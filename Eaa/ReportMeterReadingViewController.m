//
//  ReportMeterReadingViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 03/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ReportMeterReadingViewController.h"
#import "DashBoardViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "ManageMeterList+CoreDataProperties.h"
#import "AddMeterReading+CoreDataProperties.h"
#import "DateMeterReading+CoreDataProperties.h"
#import "EXPhotoViewer.h"
#import "TechDashBoardViewController.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface ReportMeterReadingViewController ()
{
    UITableView *tblData;
    NSMutableArray *arrDataTblView;
    UIView *viewForDate;
    UIView *viewBackGround;
    Global *global;
    NSArray *arrManageMeterListToShow;
    
    NSString *latitude,*longtitude,*strMeterIdGlobal,*strMeterImageNames;
    UIAlertController *loaderAlert;
}
@end

@implementation ReportMeterReadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================
    
    _meterImgView.image=[UIImage imageNamed:@"NoImage.png"];
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    _widthTxtUnit.constant = 0.0;
    _txtUnit.text = @"";
    
    NSUserDefaults *sefss=[NSUserDefaults standardUserDefaults];
    
    BOOL isfromDashBaord=[sefss boolForKey:@"fromDashBoard"];
    
    if (isfromDashBaord) {
        
        [sefss setBool:NO forKey:@"fromDashBoard"];
        [sefss synchronize];
        
    } else {
        
        [_btnSelectMeter setTitle:_strMeterNickNamenSerialNoDefault forState:UIControlStateNormal];
        _lbl_SelectMeterText.text=_btnSelectMeter.titleLabel.text;
        strMeterIdGlobal=_strMeteridDefault;
        if (_strUnit.length != 0) {
            _widthTxtUnit.constant = 100.0;
            _txtUnit.text = _strUnit;
        }
        
        
    }
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [self fetchFromCoreDataManageMeterList];
        
    }
    else
    {
        
        [self FetchManageMeterList];
        
    }
    
    UITapGestureRecognizer *singleTapmeterImgView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTapmeterImgView.numberOfTapsRequired = 1;
    [_meterImgView setUserInteractionEnabled:YES];
    [_meterImgView addGestureRecognizer:singleTapmeterImgView];
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedPopUpImgView)];
    singleTap.numberOfTapsRequired = 1;
    [_popUpImgView setUserInteractionEnabled:YES];
    [_popUpImgView addGestureRecognizer:singleTap];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    // Do any additional setup after loading the view.
    
    
    
    
    
}
//============================================================================
//============================================================================
#pragma mark- TapDetected METHODS
//============================================================================
//============================================================================

-(void)tapDetected{
    [EXPhotoViewer showImageFrom:_meterImgView];
}
-(void)tapDetectedPopUpImgView{
    [EXPhotoViewer showImageFrom:_popUpImgView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)action_DoneThankYou:(id)sender {
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        DashBoardViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }else{
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        TechDashBoardViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TechDashBoardViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}

- (IBAction)action_Submit:(id)sender
{
    
    BOOL isAlreadySent;
    isAlreadySent=NO;
    isAlreadySent=[self fetchFromCoreDataDateReadingToCheck:strMeterIdGlobal];
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"])
    {
        
    }
    else
    {
        isAlreadySent=NO;
    }
    isAlreadySent=NO;
    if (isAlreadySent)
    {
        
        NSString *strTitle = @"Alert";
        NSString *strMsg = @"Reading can be submitted only once in 24 hrs.";
        [global AlertMethod:strTitle :strMsg];
    }
    else
    {
        UIImage *imageToCheckFor = [UIImage imageNamed:@"NoImage.png"];
        
        UIImage *img = _meterImgView.image;
        
        NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
        
        NSData *imgData2 = UIImagePNGRepresentation(img);
        
        BOOL isCompare =  [imgData1 isEqual:imgData2];
        
        if(_btnSelectMeter.titleLabel.text.length==0)
        {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert!"
                                       message:@"Please select meter to continue"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else if([_btnSelectMeter.titleLabel.text isEqualToString:@"---Select Meter---"])
        {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert!"
                                       message:@"Please select meter to continue"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else if(_txtMeterReading.text.length==0)
        {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert!"
                                       message:@"Please enter meter reading to continue"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else if(isCompare==true)
        {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert!"
                                       message:@"Please take picture to continue"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            //        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Delete" style:UIAlertActionStyleDefault
            //                                                   handler:^(UIAlertAction * action)
            //                             {
            //
            //                             }];
            //        [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }else{
            
            _lbl_MeterReading.text=_txtMeterReading.text;
            _lbl_MeterNo.text=_btnSelectMeter.titleLabel.text;
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"MM/dd/yyyy HH:mm:ss";
            _lbl_DateSubmitted.text=[formatter stringFromDate:[NSDate date]];
            
            _popUpImgView.image=_meterImgView.image;
            
            CGRect frameForHiddenView=CGRectMake(0, 0, self.view.frame.size.width, _popUpView.frame.size.height);
            frameForHiddenView.origin.x=0;
            frameForHiddenView.origin.y=0;
            [_popUpView setFrame:frameForHiddenView];
            [self.view addSubview:_popUpView];
        }
    }
}

- (IBAction)action_SelectMeter:(id)sender {
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    if (arrManageMeterListToShow.count==0) {
        
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailableMeterName;
        [global AlertMethod:strTitle :strMsg];
        
        
    }else{
        
        //        NSDictionary *dictdata=arrManageMeterListToShow[0];
        //        [_btnSelectMeter setTitle:[dictdata valueForKey:@"MeterName"] forState:UIControlStateNormal];
        //        strMeterIdGlobal=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterId"]];
        
        tblData.tag=101;
        [self tableLoad:tblData.tag];
    }
    
}

- (IBAction)action_Back:(id)sender {
    
    //    for (UIViewController *controller in self.navigationController.viewControllers)
    //    {
    //        if ([controller isKindOfClass:[DashBoardViewController class]])
    //        {
    //            [self.navigationController popToViewController:controller animated:NO];
    //            break;
    //        }
    //    }
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        DashBoardViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }else{
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        TechDashBoardViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TechDashBoardViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    
}

- (IBAction)action_Help:(id)sender {
    
    
    
}

- (IBAction)action_Camera:(id)sender {
    
    BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
    
    if (isCameraPermissionAvailable) {
        
        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
        imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
        imagePickController.delegate=(id)self;
        imagePickController.allowsEditing=false;
        [self presentViewController:imagePickController animated:YES completion:nil];
    }
    else
    {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Camera Permission not allowed.Please to settings and allow"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
        [alert addAction:yes];
        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                                 if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                     NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                     [[UIApplication sharedApplication] openURL:url];
                                 } else {
                                     
                                     //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                     //                                     [alert show];
                                     
                                 }
                                 
                             }];
        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
- (IBAction)hideKeyboard:(id)sender{
    [self resignFirstResponder];
}

- (IBAction)action_CancelPopUp:(id)sender {
    [_popUpView removeFromSuperview];
}
- (IBAction)action_ConfirmPopUp:(id)sender {
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Submitting Request..."];
    loaderAlert = [global loader_ShowObjectiveC:self :@"Submitting Request..."];
    
    [self performSelector:@selector(addMeter) withObject:nil afterDelay:0.3];
    
    //    [self addMeter];
    
}

//============================================================================
//============================================================================
#pragma mark- ImagePicker Finished  Methods
//============================================================================
//============================================================================

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //  isFromImagePicker=YES;
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    strMeterImageNames = [NSString stringWithFormat:@"MeterImgg%@%@.jpg",strDate,strTime];
    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    if (image.imageOrientation==UIImageOrientationUp) {
        NSLog(@"UIImageOrientationUp");
    } else if (image.imageOrientation==UIImageOrientationLeft){
        NSLog(@"UIImageOrientationLeft");
    } else if (image.imageOrientation==UIImageOrientationRight){
        NSLog(@"UIImageOrientationRight");
    } else if (image.imageOrientation==UIImageOrientationDown){
        NSLog(@"UIImageOrientationDown");
    }
    
    //    UIImage *imageToDisplay =
    [UIImage imageWithCGImage:[image CGImage]
                        scale:[image scale]
                  orientation: UIImageOrientationUp];
    
    _meterImgView.image = image;
    
    [self resizeImage:image :strMeterImageNames];
    
    //[self saveImage:image :strMeterImageNames];
    
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}
-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = actualHeight/2;
    float maxWidth = actualWidth/2;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    //    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //    CGFloat screenWidth = screenRect.size.width;
    //    if (screenWidth == 320)
    //    {
    //        actualWidth =1000;
    //        actualHeight =1000;
    //    }
    //    else if (screenWidth == 375)
    //    {
    //        actualWidth =365;
    //        actualHeight =365;
    //    }
    //    else if (screenWidth == 414)
    //    {
    //        actualWidth =404;
    //        actualHeight =404;
    //    }
    //    else
    //    {
    //        actualWidth =404;
    //        actualHeight =404;
    //    }
    //
    //    actualWidth =1000;
    //    actualHeight =1000;
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_txtMeterReading resignFirstResponder];
    
}

//============================================================================
//============================================================================
#pragma mark- ------------METHODS--------------
//============================================================================
//============================================================================
-(void)setTableFrame
{
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
    UIButton *btnCancel=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width, 42)];
    
    [btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCancel.backgroundColor=[UIColor colorWithRed:69.0f/255 green:88.0f/255 blue:103.0f/255 alpha:1];
    [btnCancel addTarget:self action:@selector(btnCancelAction:) forControlEvents:UIControlEventTouchDown];
    [viewBackGround addSubview:btnCancel];
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Button Cancel Action Methods--------------
//============================================================================
//============================================================================

-(void)btnCancelAction:(id)sender{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    
}

-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=101;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
            
        default:
            break;
    }
    [tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrManageMeterListToShow count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (!(arrManageMeterListToShow.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 101:
            {
                NSDictionary *dictdata=arrManageMeterListToShow[indexPath.row];
                
                NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNumber"]];
                NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterName"]];
                
                NSString *strNameToShow;
                
                if (strMeterNumber.length==0) {
                    
                    if (strMeterNamee.length==0) {
                        
                        strNameToShow=@"";
                        
                    } else {
                        
                        strNameToShow=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterName"]];
                        
                    }
                    
                } else {
                    
                    if (strMeterNamee.length==0) {
                        
                        strNameToShow=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNumber"]];
                        
                    } else {
                        
                        strNameToShow=[NSString stringWithFormat:@"%@-%@",[dictdata valueForKey:@"MeterName"],[dictdata valueForKey:@"MeterNumber"]];
                    }
                }
                
                cell.textLabel.text=strNameToShow;
                cell.textLabel.numberOfLines=2;
                break;
            }
            case 102:
            {
                break;
            }
            default:
                break;
        }
        
    }
    if ([UIScreen mainScreen].bounds.size.height==667) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }
    else
        cell.textLabel.font=[UIFont systemFontOfSize:15];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isAlreadySent;
    isAlreadySent=NO;
    if (!(arrManageMeterListToShow.count==0))
    {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 101:
            {
                NSDictionary *dictdata=arrManageMeterListToShow[indexPath.row];
                NSLog(@"%@",dictdata);
                NSString *strNameToShow=[NSString stringWithFormat:@"%@-%@",[dictdata valueForKey:@"MeterName"],[dictdata valueForKey:@"MeterNumber"]];
                
                isAlreadySent=[self fetchFromCoreDataDateReadingToCheck:[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterId"]]];
                
                NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
                NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
                
                if ([strUserType isEqualToString:@"Customer"])
                {
                    
                }
                else
                {
                    
                    isAlreadySent=NO;
                    
                }
                if (isAlreadySent)
                {
                    
                    
                    
                }
                else
                {
                    
                    strMeterIdGlobal=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterId"]];
                    [_btnSelectMeter setTitle:strNameToShow forState:UIControlStateNormal];
                    _lbl_SelectMeterText.text=_btnSelectMeter.titleLabel.text;
                    _lbl_MeterNo.text=strNameToShow;
                    
                    
                    
                }
                
                break;
            }
            case 102:
            {
                break;
            }
                
            default:
                break;
        }
        
    }
    isAlreadySent=NO;
    if (isAlreadySent) {
        
        NSString *strTitle = @"Alert";
        NSString *strMsg = @"Reading can be submitted only once in 24 hrs.";
        [global AlertMethod:strTitle :strMsg];
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }else{
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
    }
    
}
//============================================================================
//============================================================================
#pragma mark- Save Image  Methods
//============================================================================
//============================================================================

- (void)saveImage: (UIImage*)image :(NSString*)imageName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Image Upload Method -----------------------
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName :(NSString*)jsonString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];
    
    NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",MainUrl,UrlImageUpload];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Image Sent");
    }
    NSLog(@"Image Sent === %@",returnString);
    
    [self sendingFinalJson:jsonString];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Core Data Method Meter List -----------------------
//============================================================================
//============================================================================

-(void)saveToCoreDataLeads :(NSArray*)arrOfManageMeterList{
    
    //==================================================================================
    //==================================================================================
    
    [self deleteManageMeterListFromDB];
    
    //==================================================================================
    //==================================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList];
    
    //==================================================================================
    //==  ================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    ManageMeterList *objManageMeterList = [[ManageMeterList alloc]initWithEntity:entityManageMeterList insertIntoManagedObjectContext:contextManageMeterList];
    objManageMeterList.userName=[dictLoginDetail valueForKey:@"UserName"];
    objManageMeterList.userType=@"";
    objManageMeterList.arrOfManageMeterList=arrOfManageMeterList;
    NSError *error1;
    [contextManageMeterList save:&error1];
    
    //==================================================================================
    //==================================================================================
}

-(void)deleteManageMeterListFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [contextManageMeterList executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [contextManageMeterList deleteObject:data];
        }
        NSError *saveError = nil;
        [contextManageMeterList save:&saveError];
    }
    
}

-(void)fetchFromCoreDataManageMeterList
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"ManageMeterList" inManagedObjectContext:contextManageMeterList];
    requestManageMeterList = [[NSFetchRequest alloc] init];
    [requestManageMeterList setEntity:entityManageMeterList];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestManageMeterList setPredicate:predicate];
    
    sortDescriptorManageMeterList = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsManageMeterList = [NSArray arrayWithObject:sortDescriptorManageMeterList];
    
    [requestManageMeterList setSortDescriptors:sortDescriptorsManageMeterList];
    
    self.fetchedResultsControllerManageMeterList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestManageMeterList managedObjectContext:contextManageMeterList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerManageMeterList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerManageMeterList performFetch:&error];
    arrAllObjManageMeterList = [self.fetchedResultsControllerManageMeterList fetchedObjects];
    if ([arrAllObjManageMeterList count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailableMeterName;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        matchesManageMeterList = arrAllObjManageMeterList[0];
        arrManageMeterListToShow=[matchesManageMeterList valueForKey:@"arrOfManageMeterList"];
        
        if (!(arrManageMeterListToShow.count==0)) {
            
            if([_strComeFrom  isEqual: @"AddMeter_New"]){
                
                for (int i =0; i>arrAllObjManageMeterList.count; i++) {
                    
                    NSDictionary *dictdata=arrManageMeterListToShow[i];
                    NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNumber"]];
                    NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterName"]];
                    
                    NSString *strNameToShow;
                    
                    if (strMeterNumber.length==0) {
                        
                        if (strMeterNamee.length==0) {
                            
                            strNameToShow=@"";
                            
                        } else {
                            
                            strNameToShow=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterName"]];
                            
                        }
                        
                    } else {
                        
                        if (strMeterNamee.length==0) {
                            
                            strNameToShow=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNumber"]];
                            
                        } else {
                            
                            strNameToShow=[NSString stringWithFormat:@"%@-%@",[dictdata valueForKey:@"MeterName"],[dictdata valueForKey:@"MeterNumber"]];
                        }
                    }
                    
                    if(strNameToShow == _strMeterNickNamenSerialNoDefault){
                        [_btnSelectMeter setTitle:_strMeterNickNamenSerialNoDefault forState:UIControlStateNormal];
                        _lbl_SelectMeterText.text=_btnSelectMeter.titleLabel.text;
                        strMeterIdGlobal=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterId"]];
                        _strUnit=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"Unit"]];;
                        
                        if (_strUnit.length != 0) {
                            _widthTxtUnit.constant = 100.0;
                            _txtUnit.text = _strUnit;
                        }
                        
                    }
                    
                    
                }
                if(strMeterIdGlobal.length==0){
                    [_btnSelectMeter setTitle:@"---Select Meter---" forState:UIControlStateNormal];
                    _lbl_SelectMeterText.text=_btnSelectMeter.titleLabel.text;
                    strMeterIdGlobal=_strMeteridDefault;
                    if (_strUnit.length != 0) {
                        _widthTxtUnit.constant = 100.0;
                        _txtUnit.text = _strUnit;
                    }
                }
                
            }
            
            
            //            NSDictionary *dictdata=arrManageMeterListToShow[0];
            //            [_btnSelectMeter setTitle:[dictdata valueForKey:@"MeterName"] forState:UIControlStateNormal];
            //            strMeterIdGlobal=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterId"]];
        }else{
            [_btnSelectMeter setTitle:@"---Select Meter---" forState:UIControlStateNormal];
            _lbl_SelectMeterText.text=_btnSelectMeter.titleLabel.text;
            strMeterIdGlobal=_strMeteridDefault;
            if (_strUnit.length != 0) {
                _widthTxtUnit.constant = 100.0;
                _txtUnit.text = _strUnit;
            }
        }
        [tblData reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchFromCoreDataDateReadingToSaveOrUpdate :(NSString*)meterIdd{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDateReading = [appDelegate managedObjectContext];
    entityDateReading=[NSEntityDescription entityForName:@"DateMeterReading" inManagedObjectContext:contextDateReading];
    requestDateReading = [[NSFetchRequest alloc] init];
    [requestDateReading setEntity:entityDateReading];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@ AND meterId = %@",strUsername,meterIdd];
    [requestDateReading setPredicate:predicate];
    
    sortDescriptorDateReading = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsDateReading = [NSArray arrayWithObject:sortDescriptorDateReading];
    
    [requestDateReading setSortDescriptors:sortDescriptorsDateReading];
    
    self.fetchedResultsControllerDateReading = [[NSFetchedResultsController alloc] initWithFetchRequest:requestDateReading managedObjectContext:contextDateReading sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerDateReading setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerDateReading performFetch:&error];
    arrAllObjDateReading = [self.fetchedResultsControllerDateReading fetchedObjects];
    if ([arrAllObjDateReading count] == 0)
    {
        
        DateMeterReading *objDateMeterReading = [[DateMeterReading alloc]initWithEntity:entityDateReading insertIntoManagedObjectContext:contextDateReading];
        objDateMeterReading.userName=[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
        objDateMeterReading.userType=@"";
        objDateMeterReading.meterId=meterIdd;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"MM/dd/yyyy";
        NSString *strDateReadingEntered = [formatter stringFromDate:[NSDate date]];
        
        objDateMeterReading.dateReadingBeingSent=strDateReadingEntered;
        NSError *error1;
        [contextDateReading save:&error1];
    }
    else
    {
        matchesDateReading = arrAllObjDateReading[0];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"MM/dd/yyyy";
        NSString *strDateReadingEntered = [formatter stringFromDate:[NSDate date]];
        [matchesDateReading setValue:strDateReadingEntered forKey:@"dateReadingBeingSent"];
        
        NSError *error1;
        [contextDateReading save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(BOOL)fetchFromCoreDataDateReadingToCheck :(NSString*)meterIdd {
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDateReading = [appDelegate managedObjectContext];
    entityDateReading=[NSEntityDescription entityForName:@"DateMeterReading" inManagedObjectContext:contextDateReading];
    requestDateReading = [[NSFetchRequest alloc] init];
    [requestDateReading setEntity:entityDateReading];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@ AND meterId = %@",strUsername,meterIdd];
    [requestDateReading setPredicate:predicate];
    
    sortDescriptorDateReading = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsDateReading = [NSArray arrayWithObject:sortDescriptorDateReading];
    
    [requestDateReading setSortDescriptors:sortDescriptorsDateReading];
    
    self.fetchedResultsControllerDateReading = [[NSFetchedResultsController alloc] initWithFetchRequest:requestDateReading managedObjectContext:contextDateReading sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerDateReading setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerDateReading performFetch:&error];
    arrAllObjDateReading = [self.fetchedResultsControllerDateReading fetchedObjects];
    if ([arrAllObjDateReading count] == 0)
    {
        return NO;
    }
    else
    {
        
        matchesDateReading = arrAllObjDateReading[0];
        //dateReadingBeingSent
        
        NSString *strReadingDate=[matchesDateReading valueForKey:@"dateReadingBeingSent"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSDate* ReadingDate = [dateFormatter dateFromString:strReadingDate];
        NSString *strCurrentDate = [dateFormatter stringFromDate:[NSDate date]];
        NSDate* CurrentDate = [dateFormatter dateFromString:strCurrentDate];
        
        NSComparisonResult result = [ReadingDate compare:CurrentDate];
        
        if(result == NSOrderedDescending)
        {
            NSLog(@"ReadingDate is Greater");//
            return NO;
        }
        else if(result == NSOrderedAscending)
        {
            NSLog(@"CurrentDate is greater");//
            return NO;
        }
        else
        {
            NSLog(@"Equal date");//
            return YES;
        }
    }
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Fetch Meter List Method -----------------------
//============================================================================
//============================================================================

-(void)FetchManageMeterList
{
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    if (dictLoginDetail.count==0) {
        
    } else {
        
        NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
        NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
        
        NSString *strCustomerId;
        
        if ([strUserType isEqualToString:@"Customer"])
        {
            strCustomerId=[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]];
        }
        else
        {
            strCustomerId=@"0";
        }
        
        NSArray *objects = [NSArray arrayWithObjects:
                            strCustomerId,
                            @"0",
                            @"",
                            @"",
                            @"",nil];
        
        NSArray *keys = [NSArray arrayWithObjects:
                         @"CustomerId",
                         @"MeterId",
                         @"MeterName",
                         @"MeterNo",
                         @"CustomerName",nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Get Manage Meter List JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlGetManageMeterList];
        loaderAlert = [global loader_ShowObjectiveC:self :@"Loading Meters..."];
        
        // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Meters..."];
        
        //============================================================================
        //============================================================================
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrlPostMethod:strUrl :@"ManageMeterAPI" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [loaderAlert dismissViewControllerAnimated:NO completion:^{
                         // [DejalBezelActivityView removeView];
                         if (success)
                         {
                             [self afterServerResponseOnLogin:response];
                         }
                         else
                         {
                                 [_btnSelectMeter setTitle:@"---Select Meter---" forState:UIControlStateNormal];
                                 _lbl_SelectMeterText.text=_btnSelectMeter.titleLabel.text;
                                 strMeterIdGlobal=_strMeteridDefault;
                                 if (_strUnit.length != 0) {
                                     _widthTxtUnit.constant = 100.0;
                                     _txtUnit.text = _strUnit;
                                 }
                             NSString *strTitle = Alert;
                             NSString *strMsg = Sorry;
                             [global AlertMethod:strTitle :strMsg];
                         }
                         
                     }];
                 });
                 
             }];
        });
    }
    //============================================================================
    //============================================================================
}
-(void)afterServerResponseOnLogin :(NSDictionary*)dictResponse{
    
    // work for showing only Active Meter status
    NSArray *arrayTemp=(NSArray*)dictResponse;
    NSMutableArray *arrayOnlyActiveMeterList = [NSMutableArray new];
    
    for(int i=0;i<arrayTemp.count;i++)
    {
        if([[[arrayTemp objectAtIndex:i] valueForKey:@"MeterStatus"] isEqualToString:@"Active"])
        {
            [arrayOnlyActiveMeterList addObject:[arrayTemp objectAtIndex:i]];
        }
    }
    
    arrManageMeterListToShow = (NSArray*)arrayOnlyActiveMeterList;
    
    if (arrManageMeterListToShow.count==0) {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailableMeterName
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        if (!(arrManageMeterListToShow.count==0)) {
            
            
            if([_strComeFrom  isEqual: @"AddMeter_New"]){
                
                for (int i =0; i<=arrAllObjManageMeterList.count; i++) {
                    
                    NSDictionary *dictdata=arrManageMeterListToShow[i];
                    NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNumber"]];
                    NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterName"]];
                    
                    NSString *strNameToShow;
                    
                    if (strMeterNumber.length==0) {
                        
                        if (strMeterNamee.length==0) {
                            
                            strNameToShow=@"";
                            
                        } else {
                            
                            strNameToShow=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterName"]];
                            
                        }
                        
                    } else {
                        
                        if (strMeterNamee.length==0) {
                            
                            strNameToShow=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNumber"]];
                            
                        } else {
                            
                            strNameToShow=[NSString stringWithFormat:@"%@-%@",[dictdata valueForKey:@"MeterName"],[dictdata valueForKey:@"MeterNumber"]];
                        }
                    }
                    
                    if([strNameToShow isEqualToString: _strMeterNickNamenSerialNoDefault]){
                        [_btnSelectMeter setTitle:_strMeterNickNamenSerialNoDefault forState:UIControlStateNormal];
                        _lbl_SelectMeterText.text=_btnSelectMeter.titleLabel.text;
                        strMeterIdGlobal=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterId"]];
                        _strUnit=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"Unit"]];;
                        
                        if (_strUnit.length != 0) {
                            _widthTxtUnit.constant = 100.0;
                            _txtUnit.text = _strUnit;
                        }
                        
                    }
                    
                    
                }
                
                
                
            }
            
            
            //            NSDictionary *dictdata=arrManageMeterListToShow[0];
            //            [_btnSelectMeter setTitle:[dictdata valueForKey:@"MeterName"] forState:UIControlStateNormal];
            //            strMeterIdGlobal=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterId"]];
        }
        
        [self saveToCoreDataLeads:arrManageMeterListToShow];
        //[_tblManageMeter reloadData];
        
    }
}


//============================================================================
//============================================================================
#pragma mark- ---------------------Add Meter Reading Method -----------------------
//============================================================================
//============================================================================

-(void)addMeter
{
    
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    //NSString *strCustomerIdd = [NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserId"]];
    NSString *strAuthIdd = [NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"AuthId"]];
    
    
    NSString *createdDate,*modifyDate,*strSubmittedDate,*strMeterId,*strCurrentMeterReading,*strImageName;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy HH:mm:ss";
    createdDate = [formatter stringFromDate:[NSDate date]];
    strSubmittedDate = [formatter stringFromDate:[NSDate date]];
    strMeterId=strMeterIdGlobal;
    strImageName=strMeterImageNames;
    strCurrentMeterReading=[_txtMeterReading.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    if (latitude.length==0) {
        latitude=@"0.00000";
    }
    if (longtitude.length==0) {
        longtitude=@"0.00000";
    }
    if (strImageName.length==0) {
        strImageName=@"";
    }
    if (strSubmittedDate.length==0) {
        strSubmittedDate=@"";
    }
    if (createdDate.length==0) {
        createdDate=@"";
    }
    if (modifyDate.length==0) {
        modifyDate=@"";
    }
    if (strCurrentMeterReading.length==0) {
        strCurrentMeterReading=@"";
    }
    
    NSArray *objects = [NSArray arrayWithObjects:
                        @"0",
                        strMeterId,
                        strCurrentMeterReading,
                        @"",
                        strImageName,
                        latitude,
                        longtitude,
                        @"New",
                        @"",
                        @"",
                        strSubmittedDate,
                        strAuthIdd,
                        @"",
                        @"",
                        @"",
                        @"",
                        createdDate,
                        @"",
                        strAuthIdd,
                        @"",nil];
    
    NSArray *keys = [NSArray arrayWithObjects:
                     @"ReadingId",
                     @"MeterId",
                     @"CurrentMeterReading",
                     @"Comment",
                     @"Images",
                     @"ReadingLat",
                     @"ReadingLong",
                     @"Status",
                     @"PumpedAmount",
                     @"DeclineReason",
                     @"SubmittedDate",
                     @"SubmittedByLoginId",
                     @"GrabBy",
                     @"GrabDate",
                     @"AudioFile",
                     @"InternalNotes",
                     @"CreatedDate",
                     @"ModifyDate",
                     @"CreatedBy",
                     @"ModifyBy",nil];
    
    
    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Add Meter Reading JSON: %@", jsonString);
        }
    }
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        //[DejalBezelActivityView removeView];
        
        [loaderAlert dismissViewControllerAnimated:NO completion:^{
            
            [self fetchFromCoreDataDateReadingToSaveOrUpdate:strMeterIdGlobal];
            
            [self saveToCoreDataAddMeterDetails:jsonString :strImageName :createdDate];
            [_popUpView removeFromSuperview];
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Info"
                                       message:@"Meter Reading Data Saved Offline."
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                                      NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
                                      NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
                                      
                                      if ([strUserType isEqualToString:@"Customer"]) {
                                          
                                          UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                                   bundle: nil];
                                          DashBoardViewController
                                          *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
                                          [self.navigationController pushViewController:objByProductVC animated:NO];
                                          
                                      }else{
                                          
                                          UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                                   bundle: nil];
                                          TechDashBoardViewController
                                          *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TechDashBoardViewController"];
                                          [self.navigationController pushViewController:objByProductVC animated:NO];
                                          
                                      }
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
        }];
        
        
        
        
    }
    else
    {
        
        //     if (strImageName.length>0) {
        //
        ////         dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        ////         dispatch_async(myQueue, ^{
        ////
        ////             dispatch_async(dispatch_get_main_queue(), ^{
        ////                 // Update the UI
        ////             });
        ////         });
        //
        //     }
        
        
        if (strImageName.length>0) {
            
            [self uploadImage:strImageName :jsonString];
            
        } else {
            
            [self sendingFinalJson:jsonString];
            
        }
        
        
    }
}


-(void)uploadingImageDelaying :(NSString*)strImagename :(NSString*)jsonString
{
    
    
    
}
-(void)saveToCoreDataAddMeterDetails :(NSString*)jsonString :(NSString*)strMeterImageName :(NSString*)strDatenTime{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextManageMeterList = [appDelegate managedObjectContext];
    entityManageMeterList=[NSEntityDescription entityForName:@"AddMeterReading" inManagedObjectContext:contextManageMeterList];
    
    //==================================================================================
    //==================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    AddMeterReading *objAddMeterReading = [[AddMeterReading alloc]initWithEntity:entityManageMeterList insertIntoManagedObjectContext:contextManageMeterList];
    objAddMeterReading.userName=[dictLoginDetail valueForKey:@"UserName"];
    objAddMeterReading.userType=@"";
    objAddMeterReading.meterImageName=strMeterImageName;
    objAddMeterReading.jsonString=jsonString;
    objAddMeterReading.meterReading=_txtMeterReading.text;
    objAddMeterReading.meterSerialNo=_btnSelectMeter.titleLabel.text;
    NSDate *pickerDate = [NSDate date];
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM/DD/YYYY HH:MM:SS.ffff"];
    NSDate *newDate1 =pickerDate;
    objAddMeterReading.dateAscending=newDate1;
    
    objAddMeterReading.datenTime=strDatenTime;
    NSError *error1;
    [contextManageMeterList save:&error1];
    
    //==================================================================================
    //==================================================================================
    
}

-(void)afterServerResponseAddMeter :(NSDictionary*)dictResponse{
    
    if (dictResponse.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:Sorry
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        BOOL isSuccess=[[dictResponse valueForKey:@"IsSuccess"] boolValue];
        
        if (isSuccess) {
            
            [self fetchFromCoreDataDateReadingToSaveOrUpdate:strMeterIdGlobal];
            
            [self showThankYouView];
            
        } else {
            
            NSArray *arrOfError=[dictResponse valueForKey:@"Error"];
            if ([arrOfError isKindOfClass:[NSArray class]]) {
                
                if (arrOfError.count==0) {
                    
                    NSString *strTitle = Alert;
                    NSString *strMsg = Sorry;
                    [global AlertMethod:strTitle :strMsg];
                    
                    [_popUpView removeFromSuperview];
                    
                } else {
                    
                    NSString *joinedError = [arrOfError componentsJoinedByString:@","];
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert!"
                                               message:joinedError
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              [self fetchFromCoreDataDateReadingToSaveOrUpdate:strMeterIdGlobal];
                                              [_popUpView removeFromSuperview];
                                          }];
                    [alert addAction:yes];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }
                
            } else {
                
                NSString *strTitle = Alert;
                NSString *strMsg = Sorry;
                [global AlertMethod:strTitle :strMsg];
                
            }
            
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)alocations
{
    
    CLLocation *currentLocation;
    currentLocation =[alocations lastObject];
    int degrees = currentLocation.coordinate.latitude;
    double decimal = fabs(currentLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    
    //    latitude = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
    //                degrees, minutes, seconds];
    
    // locationLatitude = currentLocation.coordinate.latitude;
    degrees = currentLocation.coordinate.longitude;
    decimal = fabs(currentLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    
    //    longtitude = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
    //                  degrees, minutes, seconds];
    
    float activelat=currentLocation.coordinate.latitude;
    float activelong=currentLocation.coordinate.longitude;
    latitude=[NSString stringWithFormat:@"%f", activelat];
    longtitude=[NSString stringWithFormat:@"%f", activelong];
    
    [locationManager stopUpdatingLocation];
    
}
-(void)showThankYouView{
    
    [_popUpView removeFromSuperview];
    CGRect frameForHiddenView=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-20);
    frameForHiddenView.origin.x=0;
    frameForHiddenView.origin.y=0;
    [_ThankYouVieww setFrame:frameForHiddenView];
    [self.view addSubview:_ThankYouVieww];
    
}
-(void)sendingFinalJson :(NSString*)jsonString
{
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlAddMeterReading];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"AddUpdateMeter" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [loaderAlert dismissViewControllerAnimated:NO completion:^{
                     if (success)
                     {
                         [self afterServerResponseAddMeter:response];
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 }];
                 // [DejalBezelActivityView removeView];
             });
         }];
    });
}
@end
