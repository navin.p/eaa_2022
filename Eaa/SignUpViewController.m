//
//  SignUpViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 27/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "SignUpViewController.h"
#import "DashBoardViewController.h"
#import "LoginViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "AddMeterViewController.h"

@interface SignUpViewController ()
{
    Global *global;
}

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_Continue:(id)sender {
    
    [self SignUpMethod];
    
}

- (IBAction)action_Back:(id)sender {
    
//    for (UIViewController *controller in self.navigationController.viewControllers)
//    {
//        if ([controller isKindOfClass:[LoginViewController class]])
//        {
//            [self.navigationController popToViewController:controller animated:NO];
//            //break;
//        }
//    }
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k=0; k<arrstack.count; k++) {
        if ([[arrstack objectAtIndex:k] isKindOfClass:[LoginViewController class]]) {
            index=k;
            //break;
        }
    }
    LoginViewController *myController = (LoginViewController *)[self.navigationController.viewControllers objectAtIndex:index];
    [self.navigationController popToViewController:myController animated:NO];
    
}
- (IBAction)hideKeyboard:(id)sender{
    
    [self resignFirstResponder];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_txtEmailId resignFirstResponder];
    [_txtFName resignFirstResponder];
    [_txtLName resignFirstResponder];
    [_txtNumber resignFirstResponder];
    [_txtPassword resignFirstResponder];
    [_txtConfirmpassword resignFirstResponder];

}
#pragma mark - UITextField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==_txtNumber)
    {
        if (range.location == 12)
        {
            return NO;
        }
        
        // Backspace
        if ([string length] == 0)
            return YES;
        
        if ((range.location == 3) || (range.location == 7))
        {            
            NSString *str    = [NSString stringWithFormat:@"%@-",textField.text];
            textField.text   = str;
        }
        
        return YES;
    }
    return YES;
}
//============================================================================
//============================================================================
#pragma mark--- Methods
//============================================================================
//============================================================================
-(NSString *)validationCheck
{
    NSString *errorMessage;
    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if (_txtFName.text.length==0) {
        errorMessage = @"Please enter first name";
    }
    else if (_txtLName.text.length==0){
        errorMessage = @"Please enter last name";
    }
    else if (_txtEmailId.text.length==0){
        errorMessage = @"Please enter email address";
    }
    else if (![emailPredicate evaluateWithObject:_txtEmailId.text])
    {
        errorMessage = @"Please enter valid email id only";
    }
    else if (_txtNumber.text.length==0){
        errorMessage = @"Please enter mobile number";
    }
    else if(_txtNumber.text.length<12)
    {
       errorMessage = @"Please enter valid mobile number";
    }
    else if ([self validatePhone:_txtNumber.text]== NO)
    {
     errorMessage = @"Please enter valid mobile number";
    }
    else if (_txtPassword.text.length==0){
        errorMessage = @"Please enter password";
    }
    else if (_txtPassword.text.length<5){
        errorMessage = @"Please enter minimum 5 characters in password";
    }
    else if (_txtConfirmpassword.text.length==0){
        errorMessage = @"Please enter confirm password";
    }
    else if (![_txtConfirmpassword.text isEqualToString:_txtPassword.text]){
        errorMessage = @"Please enter same password";
    }

    return errorMessage;
}


-(void)SignUpMethod{
    
    NSString *errorMessage=[self validationCheck];
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
        [DejalBezelActivityView removeView];
    } else {
        
        // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Logging In..."];
        
        NSString *strMiddleName;
        if ([_txtMName.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            strMiddleName=@"";
        } else {
            strMiddleName=[_txtMName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        
        NSArray *objects = [NSArray arrayWithObjects:
                            [_txtFName.text stringByReplacingOccurrencesOfString:@" " withString:@""],
                            [_txtLName.text stringByReplacingOccurrencesOfString:@" " withString:@""],
                            [_txtEmailId.text stringByReplacingOccurrencesOfString:@" " withString:@""],
                            [_txtNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""],
                            [_txtPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""],nil];
        
        NSArray *keys = [NSArray arrayWithObjects:
                         @"FirstName",
                         @"LastName",
                         @"EmailAddress",
                         @"CellNo",
                         @"Password",nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"SignUp JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlSignUp];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Registering..."];
        
        //============================================================================
        //============================================================================
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrlPostMethod:strUrl :@"SignUpAPI" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         [self afterServerResponseOnLogin:response];
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================

        
    }
}
-(void)afterServerResponseOnLogin :(NSDictionary*)dictResponse{
    
    if (dictResponse.count==0) {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:Sorry
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        //        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Delete" style:UIAlertActionStyleDefault
        //                                                   handler:^(UIAlertAction * action)
        //                             {
        //
        //                             }];
        //        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        NSArray *error=[dictResponse valueForKey:@"Error"];
        
        if (![error isKindOfClass:[NSArray class]]) {
            
            
                //============================================================================
                //============================================================================
                
                NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                [defsLoginData setObject:dictResponse forKey:@"LoginData"];
                [defsLoginData synchronize];
            [self methodToSendFirebaseTokenOnServer:dictResponse];
                NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
                
                if ([strUserType isEqualToString:@"Customer"]) {
                    
                    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                    [defsLoginData setBool:YES forKey:@"isCustomer"];
                    [defsLoginData synchronize];
                    
                    [self goToAddMeter];
                    
                } else {
                    
                    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
                    [defsLoginData setBool:NO forKey:@"isCustomer"];
                    [defsLoginData synchronize];
                    [self goToAddMeter];
                    
                }
            
        } else {
            
            NSString *strErrorMsg;
            if (error.count==0) {
                
                strErrorMsg=@"Unable to register. Please try again later.";
                
            } else {
                
                strErrorMsg=error[0];
                
            }
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert!"
                                       message:strErrorMsg
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            //        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Delete" style:UIAlertActionStyleDefault
            //                                                   handler:^(UIAlertAction * action)
            //                             {
            //
            //                             }];
            //        [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }
}

-(void)goToAddMeter{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"FromManageMeter"];
    [defs synchronize];

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    AddMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMeterViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";

    NSString *phoneRegex = @"^[0-9]+(-[0-9]+)+$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    BOOL valid =  [phoneTest evaluateWithObject:phoneNumber];
    return valid;
}

-(void)methodToSendFirebaseTokenOnServer:(NSDictionary*)dictUserData
{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *strCurrentDate = [dateFormat stringFromDate:[NSDate date]];
    NSString *strAuthId = [dictUserData valueForKey:@"AuthId"];
  
    NSDictionary *dictJson = @{
                               @"AuthId":strAuthId,
                               @"OSID":@"1",
                               @"CreatedBy":strAuthId,
                               @"CreatedDate":strCurrentDate,
                               @"ModifyBy":strAuthId,
                               @"ModifyDate":strCurrentDate,
                               @"IMEI":@"",
                               @"TokenID":[userDefaults valueForKey:@"FirebaseToken"]
                               };
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            [self sendingJsonToServer:jsonString];
        }
    }
}
-(void)sendingJsonToServer:(NSString*)jsonString
{
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlInsertUpdateNotifDevice];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :@"" :requestData withCallback:^(BOOL success, id response, NSError *error)
         {
             [DejalBezelActivityView removeView];
             NSLog(@"%@",response);
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 if (success)
                 {
                     NSLog(@"Firebase token sent successfully");
                 }
                 else
                 {
                     [DejalBezelActivityView removeView];
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}
@end
