//
//  SlideMenuTableViewCell.h
//  Eaa
//
//  Created by Saavan Patidar on 27/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

@end
