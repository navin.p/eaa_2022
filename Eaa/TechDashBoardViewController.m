//
//  TechDashBoardViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 21/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "TechDashBoardViewController.h"
#import "TechDashBoardTableViewCell.h"
#import "SlideMenuTableViewCell.h"
#import "ReportMeterReadingViewController.h"
#import "AddMeterViewController.h"
#import "ManageMeterViewController.h"
#import "OutboxViewController.h"
#import "DisclaimerViewController.h"
#import "HelpViewController.h"
#import "LoginViewController.h"
#import "TermsConditionViewController.h"
#import "HistoryViewController.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "DashBoardListt+CoreDataProperties.h"
#import "MBProgressHUD.h"

@interface TechDashBoardViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *arrOfImgSlideMenu,*arrNameSlideMenu,*arrOfStatus;
    Global *global;
    NSArray *arrDashBoardResponse;
    OutboxViewController *outBoxObj;
    BOOL yesFiltered;
    NSMutableArray *filteredArray;
}


@end

@implementation TechDashBoardViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    yesFiltered=NO;
    
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================
    
    arrOfImgSlideMenu=[NSArray arrayWithObjects:@"home_icon.png",@"add meter reading.png",@"add meter.png",@"history.png",@"help.png",@"help.png",@"disclaimer.png",@"logout.png",@"offline mode.png", nil];
    
    arrNameSlideMenu=[NSArray arrayWithObjects:@"Home",@"Report a Meter Reading",@"Manage Meters",@"History",@"Outbox",@"Help",@"Terms & Conditions",@"Logout", nil];
    
    arrOfStatus=[NSArray arrayWithObjects:@"New",@"Approved",@"Rejected",@"In Progress",@"Approved with edit",@"Rejected",@"New",@"Approved",@"Rejected", nil];
    
    if ([UIScreen mainScreen].bounds.size.height==480) {
        _const_MenuUserName_H.constant=40;
        _const_lblMenuUserName_Bottom.constant=0;
    }else if ([UIScreen mainScreen].bounds.size.height==568){
        _const_MenuUserName_H.constant=70;
        _const_lblMenuUserName_Bottom.constant=15;
    }
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    if (dictLoginDetail.count==0) {
        
    } else {
        
        _lbl_UserName.text=[dictLoginDetail valueForKey:@"UserName"];
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            [self fetchFromCoreDataDashBoardList];
            
//            NSString *strTitle = Alert;
//            NSString *strMsg = ErrorInternetMsg;
//            [global AlertMethod:strTitle :strMsg];
//            [DejalBezelActivityView removeView];
        }
        else
        {
            
            [self getDashBoardDetails:[NSString stringWithFormat:@"%@",@"0"]];
            
            // [self performSelector:@selector(callOutboxSendMethod) withObject:nil afterDelay:5.0];
            
        }
    }
    
    NSLog(@"User Details =====  %@",dictLoginDetail);
    
  //  [self advanceSearchButton];
    
    _tblViewHistory.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    _tblViewSideMenu.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    self.tblViewHistory.rowHeight = UITableViewAutomaticDimension;
    self.tblViewHistory.estimatedRowHeight = 150.0;

    // Do any additional setup after loading the view.
}
-(void)callOutboxSendMethod{
    
    outBoxObj=[[OutboxViewController alloc]init];
    [outBoxObj fetchFromCoreDataMeterReading];
    
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)action_MenuBar:(id)sender {
    
    CGRect frameForHiddenView=CGRectMake(0, 0, self.view.frame.size.width-50, _hiddenView.frame.size.height+200);
    frameForHiddenView.origin.x=200;
    frameForHiddenView.origin.y=20;
    [_hiddenView setFrame:frameForHiddenView];
    [self.view addSubview:_hiddenView];
    
    CGRect frameForCheckView=CGRectMake(0, 20, self.view.frame.size.width-50, _viewwSideMenu.frame.size.height+200);
    frameForCheckView.origin.x=0;
    frameForCheckView.origin.y=20;
    [_viewwSideMenu setFrame:frameForCheckView];
    [self.view addSubview:_viewwSideMenu];
    
    _profileImgView.layer.cornerRadius = _const_Img_W.constant/2;
    _profileImgView.clipsToBounds = YES;
    
    [self performSelector:@selector(profileImageViewSetting) withObject:nil afterDelay:2.0];
}

-(void)profileImageViewSetting{
    
    _profileImgView.layer.cornerRadius = _const_Img_W.constant/2;
    _profileImgView.clipsToBounds = YES;
    
}

- (IBAction)action_AddMeterReading:(id)sender {
}

//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag==1) {
        if (yesFiltered) {
           
            return filteredArray.count;
            
        }else{
            
            return arrDashBoardResponse.count;
            
        }
        
    }else{
        
        return arrNameSlideMenu.count;
        
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==1) {
        
        return tableView.rowHeight;
        
//        NSDictionary *dictdata=arrDashBoardResponse[indexPath.row];
//        NSString *strStatuss=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"Status"]];
//        if ([strStatuss isEqualToString:@""]) {
//            return 90;
//        }else{
//            return tableView.rowHeight;
//        }
        
    }else{
        if ([UIScreen mainScreen].bounds.size.height==480) {
            return 40;
        }
        else if ([UIScreen mainScreen].bounds.size.height==568) {
            return 47;
        }
        else{
            return tableView.rowHeight;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==1) {
        
        TechDashBoardTableViewCell *cell = (TechDashBoardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TechDashBoardTableViewCell" forIndexPath:indexPath];
        
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
        
    } else {
        
        SlideMenuTableViewCell *cell = (SlideMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SlideMenuTableViewCell" forIndexPath:indexPath];
        
        // Configure Table View Cell
        [self configureCellSlideMenuTableViewCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}
- (void)configureCell:(TechDashBoardTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    
    if (indexPath.row%2==0) {
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:161/255.0f green:172/255.0f blue:179/255.0f alpha:1.0];
    }else{
        cell.lblColorLine.backgroundColor=[UIColor colorWithRed:204/255.0f green:172/255.0f blue:84/255.0f alpha:1.0];
    }
    NSDictionary *dictdata;
    
    if (yesFiltered) {
        
        dictdata=filteredArray[indexPath.row];
        
    }else{

        dictdata=arrDashBoardResponse[indexPath.row];
        
    }
    
    cell.lbl_Status.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"Status"]];
    cell.lbl_CommentValue.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"DeclineReason"]];
    cell.lbl_PumpedAmountValue.text=[NSString stringWithFormat:@"%@ Acre-Feet",[dictdata valueForKey:@"PumpedAmount"]];
    cell.lbl_ReasonValue.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"DeclineReason"]];
    cell.lblNickName.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNickName"]];
    cell.lblMeterSerialNumber.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterSerialNumber"]];
    cell.lblLastMeterreading.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"LastMeterReading"]];
    cell.lblDate.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"SubmittedDate"]];
    cell.lbl_CustName_Value.text=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"CustomerName"]];

    if (cell.lblLastMeterreading.text.length==0) {
        cell.lblLastMeterreading.text=@"NA";
    }

    int const_custName;
    const_custName=20;
    
    if ([cell.lbl_Status.text caseInsensitiveCompare:@"New"] == NSOrderedSame || [cell.lbl_Status.text isEqualToString:@""]) {
        
        NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
        NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
        
        if ([strUserType isEqualToString:@"Customer"]) {
            if ([cell.lbl_Status.text isEqualToString:@""]) {
                cell.lbl_Status.text=@"NA";
            }else{
                cell.lbl_Status.text=@"Pending";
            }
        }else{
            if ([cell.lbl_Status.text isEqualToString:@""]) {
                cell.lbl_Status.text=@"NA";
            }
        }
        [cell.lbl_PumpedAmount setHidden:YES];
        [cell.lbl_PumpedAmountValue setHidden:YES];
        [cell.lbl_Reason setHidden:YES];
        [cell.lbl_ReasonValue setHidden:YES];
        [cell.const_PumpedAmount_H setConstant:0];
        [cell.lbl_Comment setHidden:YES];
        [cell.lbl_CommentValue setHidden:YES];
        
        cell.const_PumpedAmount_H.constant=0;
        cell.const_ResetReason_H.constant=0;
        const_custName=0;
        
    } else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Approved"] == NSOrderedSame) {
        
        [cell.lbl_PumpedAmount setHidden:NO];
        [cell.lbl_PumpedAmountValue setHidden:NO];
        [cell.lbl_Reason setHidden:YES];
        [cell.lbl_ReasonValue setHidden:YES];
        [cell.lbl_Comment setHidden:YES];
        [cell.lbl_CommentValue setHidden:YES];
        cell.const_ResetReason_H.constant=0;
        cell.const_PumpedAmount_H.constant=21;
        const_custName=0;
        
    }else if ([cell.lbl_Status.text caseInsensitiveCompare:@"In Progress"] == NSOrderedSame) {
        
        [cell.lbl_PumpedAmount setHidden:YES];
        [cell.lbl_PumpedAmountValue setHidden:YES];
        [cell.lbl_Reason setHidden:YES];
        [cell.lbl_ReasonValue setHidden:YES];
        [cell.const_PumpedAmount_H setConstant:0];
        cell.const_PumpedAmount_H.constant=0;
        cell.const_ResetReason_H.constant=0;
        const_custName=0;
        
    }else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Approved with edit"] == NSOrderedSame) {
        
        [cell.lbl_PumpedAmount setHidden:NO];
        [cell.lbl_PumpedAmountValue setHidden:NO];
        [cell.lbl_Reason setHidden:NO];
        [cell.lbl_ReasonValue setHidden:NO];
        [cell.const_PumpedAmount_H setConstant:21];
        cell.const_ResetReason_H.constant=23;
        const_custName=20;
    }
    else if ([cell.lbl_Status.text caseInsensitiveCompare:@"Rejected"] == NSOrderedSame) {
        
        [cell.lbl_PumpedAmount setHidden:YES];
        [cell.lbl_PumpedAmountValue setHidden:YES];
        [cell.lbl_Reason setHidden:NO];
        [cell.lbl_ReasonValue setHidden:NO];
        [cell.const_PumpedAmount_H setConstant:0];
        cell.const_ResetReason_H.constant=23;
        const_custName=20;
    }
    else{
        [cell.lbl_PumpedAmount setHidden:NO];
        [cell.lbl_PumpedAmountValue setHidden:NO];
        [cell.lbl_Reason setHidden:NO];
        [cell.lbl_ReasonValue setHidden:NO];
        cell.const_ResetReason_H.constant=23;
        cell.const_PumpedAmount_H.constant=21;
        const_custName=20;
    }
    
    cell.const_CustomerName_Top.constant=const_custName;
    
    if ([cell.lbl_Status.text isEqualToString:@"Reject"]) {
        
        cell.lbl_Status.text=@"Rejected";
        
    }

    cell.btnAdd.tag=indexPath.row;
    [cell.btnAdd addTarget:self action:@selector(btnAddClick:) forControlEvents:UIControlEventTouchDown];
    
}

- (void)configureCellSlideMenuTableViewCell:(SlideMenuTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    cell.imgView.image=[UIImage imageNamed:arrOfImgSlideMenu[indexPath.row]];
    cell.lblName.text=arrNameSlideMenu[indexPath.row];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==1) {
        
        NSUserDefaults *sefss=[NSUserDefaults standardUserDefaults];
        [sefss setBool:NO forKey:@"fromDashBoard"];
        [sefss synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ReportMeterReadingViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
        NSDictionary *dictData;
        
        if (yesFiltered) {
            
            dictData=filteredArray[indexPath.row];
            
        }else{
            
            dictData=arrDashBoardResponse[indexPath.row];
            
        }
        NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];
        NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];
        
        NSString *strName;
        
        if (strMeterNumber.length==0) {
            if (strMeterNamee.length==0) {
                
                strName=@"";
                
            } else {
                
                strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];
                
            }
            
        }else{
            
            if (strMeterNamee.length==0) {
                
                strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];
                
            } else {
                
                strName=[NSString stringWithFormat:@"%@-%@",[dictData valueForKey:@"MeterNickName"],[dictData valueForKey:@"MeterSerialNumber"]];
                
            }
            
        }
        objByProductVC.strMeteridDefault=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterID"]];
        objByProductVC.strMeterNickNamenSerialNoDefault=strName;
        objByProductVC.dictDataManageMeterList=dictData;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    }else{
        
        NSInteger i;
        i=indexPath.row;
        switch (i)
        {
            case 0:
            {
                //Home
                [_viewwSideMenu removeFromSuperview];
                [_hiddenView removeFromSuperview];
                break;
            }
//            case 1:
//            {
//                //Ad Meter
//                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
//                [defs setBool:NO forKey:@"FromManageMeter"];
//                [defs synchronize];
//                
//                [self goToAddMeter];
//                break;
//            }
            case 1:
            {
                //Report Meter Reading
                NSUserDefaults *sefss=[NSUserDefaults standardUserDefaults];
                [sefss setBool:YES forKey:@"fromDashBoard"];
                [sefss synchronize];
                
                [self goToReportMeterReading];
                break;
            }
            case 2:
            {
                //Manage Meters
                [self goToManageMeter];
                break;
            }
            case 3:
            {
                //History
                [self goToHistoryView];
                break;
            }
            case 4:
            {
                //Disclaimer
                [self goToOutbox];
                break;
            }
            case 5:
            {
                //Help
                [self goToHelp];
                break;
            }
            case 6:
            {
                //Help
                [self goTermsConditions];
                break;
            }
            case 7:
            {
                //Outbox
                [self logOut];
                break;
            }
            case 8:
            {
                //Outbox
                [self logOut];
                break;
            }
                
            default:
                break;
        }
        
    }
}

//============================================================================
#pragma mark- -------------------------METHODS--------------------------------
//============================================================================
-(void)btnAddClick:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    
    NSUserDefaults *sefss=[NSUserDefaults standardUserDefaults];
    [sefss setBool:NO forKey:@"fromDashBoard"];
    [sefss synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ReportMeterReadingViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
    NSDictionary *dictData;
    
    if (yesFiltered) {
        
        dictData=filteredArray[btn.tag];
        
    }else{
        
        dictData=arrDashBoardResponse[btn.tag];
        
    }

    NSString *strMeterNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];
    NSString *strMeterNamee=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];
    
    NSString *strName;
    
    if (strMeterNumber.length==0) {
        if (strMeterNamee.length==0) {
            
            strName=@"";
            
        } else {
            
            strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterNickName"]];
            
        }
        
    }else{
        
        if (strMeterNamee.length==0) {
            
            strName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterSerialNumber"]];
            
        } else {
            
            strName=[NSString stringWithFormat:@"%@-%@",[dictData valueForKey:@"MeterNickName"],[dictData valueForKey:@"MeterSerialNumber"]];
            
        }
        
    }
    objByProductVC.strMeteridDefault=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MeterID"]];
    objByProductVC.strMeterNickNamenSerialNoDefault=strName;
    objByProductVC.dictDataManageMeterList=dictData;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToReportMeterReading{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ReportMeterReadingViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportMeterReadingViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToAddMeter{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    AddMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddMeterViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToManageMeter{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ManageMeterViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ManageMeterViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToDisclaimer{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    DisclaimerViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DisclaimerViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goToHistoryView{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)goToHelp{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HelpViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)goTermsConditions{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    TermsConditionViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TermsConditionViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)goToOutbox{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    OutboxViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"OutboxViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)logOut{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm!"
                               message:@"Do you want to logout ?"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Logout" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                             [defs setBool:NO forKey:@"RememberMe"];
                             [defs synchronize];
                             
                             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                      bundle: nil];
                             LoginViewController
                             *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                             [self.navigationController pushViewController:objByProductVC animated:NO];
                             
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_viewwSideMenu removeFromSuperview];
    [_hiddenView removeFromSuperview];
    
}
-(void)getDashBoardDetails :(NSString*)customerId{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Dashboard Details..."];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@%@",MainUrl,UrlGetDashBoardDetails,customerId];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"DashBoardDetails" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     [self afterServerResponseDashBoard:response];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}

-(void)afterServerResponseDashBoard :(NSDictionary*)dictResponse{
    
    
    arrDashBoardResponse=(NSArray*)dictResponse;
    
    if (arrDashBoardResponse.count==0) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert!"
                                   message:NoDataAvailable
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        [self saveToCoreDataDashBoardData:arrDashBoardResponse];
        
       // [_tblViewHistory reloadData];
        
    }
}
//============================================================================
//============================================================================
#pragma mark- Advance Search Method
//============================================================================
//============================================================================

-(void)advanceSearchButton{
    
    UIView *view_AddTask=[[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-80,[UIScreen mainScreen].bounds.size.height-120,60,60)];
    view_AddTask.layer.cornerRadius = view_AddTask.frame.size.width/2;
    view_AddTask.clipsToBounds = YES;
    view_AddTask.backgroundColor=[UIColor colorWithRed:69/255.0f green:88/255.0f blue:103/255.0f alpha:1];
    [self.view addSubview:view_AddTask];
    
    UIButton *buttonAddTask=[[UIButton alloc]initWithFrame:CGRectMake(13, 12, 35, 35)];
    [buttonAddTask addTarget:self action:@selector(goToAddTask:) forControlEvents:UIControlEventTouchDown];
    [buttonAddTask setImage:[UIImage imageNamed:@"advanceFilter.png"] forState:UIControlStateNormal];
    [view_AddTask addSubview:buttonAddTask];
    
}
-(void)goToAddTask:(id)sender
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Enter Details To Search"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Meter Number";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Customer Name";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    
//    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.placeholder = @"Email Address";
//        textField.textColor = [UIColor blueColor];
//        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textField.borderStyle = UITextBorderStyleRoundedRect;
//        textField.keyboardType=UIKeyboardTypeDefault;
//    }];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Search" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtMeterNo = textfields[0];
        UITextField * txtNamee = textfields[1];
       // UITextField * txtEmail = textfields[2];
        NSLog(@"Meter No--%@------Name----%@",txtMeterNo.text,txtNamee.text);
        
        [self filterThrough:txtMeterNo.text :txtNamee.text :@""];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        yesFiltered=NO;
        [_tblViewHistory reloadData];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];

}

-(void)filterThrough :(NSString*)strMeterNo :(NSString*)strName :(NSString*)strEmail{
    
    yesFiltered=YES;
    
    filteredArray=[[NSMutableArray alloc]init];
    
    for(int i=0;i<[arrDashBoardResponse count];i++){
        
        NSString *str11;
        NSString *str22;
       // NSString *str33;
        
        NSDictionary *dictdata=arrDashBoardResponse[i];
        str11=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterSerialNumber"]];
        str22=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"MeterNickName"]];
       // str33=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"LastMeterReading"]];
        
        if (strMeterNo.length==0) {
            strMeterNo=@"#`%&*";
        }
        if (strName.length==0) {
            strName=@"#`%&*";
        }
        if (strEmail.length==0) {
            strEmail=@"#`%&*";
        }

        NSRange strRange1;
        if ([strMeterNo isEqualToString:@"#`%&*"]) {
            
            strRange1=[@"#`%&*" rangeOfString:strMeterNo options:NSCaseInsensitiveSearch];
            
        } else {
            
            strRange1=[str11 rangeOfString:strMeterNo options:NSCaseInsensitiveSearch];
            
        }
        
        NSRange strRange2;
        if ([strName isEqualToString:@"#`%&*"]) {
            
            strRange2=[@"#`%&*" rangeOfString:strName options:NSCaseInsensitiveSearch];
            
        } else {
            
            strRange2=[str22 rangeOfString:strName options:NSCaseInsensitiveSearch];
            
        }
        
//        NSRange strRange3;
//        if ([strEmail isEqualToString:@"#`%&*"]) {
//            
//            strRange3=[@"#`%&*" rangeOfString:[NSString stringWithFormat:@"%@",strEmail] options:NSCaseInsensitiveSearch];
//            
//        } else {
//            
//            strRange3=[str33 rangeOfString:[NSString stringWithFormat:@"%@",strEmail] options:NSCaseInsensitiveSearch];
//            
//        }
 
        if(strRange1.location!=NSNotFound&&strRange2.location!=NSNotFound)
        {
            [filteredArray addObject:dictdata];
        }
    }
    
    [_tblViewHistory setHidden:NO];
    [_tblViewHistory reloadData];
    
}

//============================================================================
//============================================================================
#pragma mark- SEARCH BAR DELEGATE METHODS
//============================================================================
//============================================================================

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length==0){
        yesFiltered=NO;
        [_tblViewHistory reloadData];
    }else
    {
        yesFiltered=YES;
        filteredArray=[[NSMutableArray alloc]init];
        
        for(int i=0;i<[arrDashBoardResponse count];i++){
            
            
            NSDictionary *dict=[arrDashBoardResponse objectAtIndex:i];
            NSString *str11=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterSerialNumber"]];
            NSString *str111=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterNickName"]];
            NSString *str22=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CustomerName"]];
            NSString *str33=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MeterSerialNumber"]];
            NSRange strRange1=[str11 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange strRange2=[str22 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange strRange3=[str33 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange strRange4=[str111 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(strRange1.location!=NSNotFound||strRange2.location!=NSNotFound||strRange3.location!=NSNotFound||strRange4.location!=NSNotFound)
            {
                [filteredArray addObject:dict];
            }
        }
    }
    [_tblViewHistory reloadData];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *) bar
{
    UITextField *searchBarTextField = nil;
    NSArray *views = ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f) ? bar.subviews : [[bar.subviews objectAtIndex:0] subviews];
    for (UIView *subview in views)
    {
        if ([subview isKindOfClass:[UITextField class]])
        {
            searchBarTextField = (UITextField *)subview;
            break;
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
}
-(void)saveToCoreDataDashBoardData :(NSArray*)arrOfDashBoardList{
    
    //==================================================================================
    //==================================================================================
    
    [self deleteDashBoardListFromDB];
    
    //==================================================================================
    //==================================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDashBoardList = [appDelegate managedObjectContext];
    entityDashBoardList=[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList];
    
    //==================================================================================
    //==  ================================================================================
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    
    //==================================================================================
    //==================================================================================
    
    DashBoardListt *objDashBoardList = [[DashBoardListt alloc]initWithEntity:entityDashBoardList insertIntoManagedObjectContext:contextDashBoardList];
    objDashBoardList.userName=[dictLoginDetail valueForKey:@"UserName"];
    objDashBoardList.userType=@"";
    objDashBoardList.arrOfDashBoardList=arrOfDashBoardList;
    NSError *error1;
    [contextDashBoardList save:&error1];
    
    [self fetchFromCoreDataDashBoardList];
    //==================================================================================
    //==================================================================================
}

-(void)deleteDashBoardListFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDashBoardList = [appDelegate managedObjectContext];
    entityDashBoardList=[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [contextDashBoardList executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [contextDashBoardList deleteObject:data];
        }
        NSError *saveError = nil;
        [contextDashBoardList save:&saveError];
    }
    
}

-(void)fetchFromCoreDataDashBoardList{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextDashBoardList = [appDelegate managedObjectContext];
    entityDashBoardList=[NSEntityDescription entityForName:@"DashBoardListt" inManagedObjectContext:contextDashBoardList];
    requestDashBoardList = [[NSFetchRequest alloc] init];
    [requestDashBoardList setEntity:entityDashBoardList];
    
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDetail=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUsername =[NSString stringWithFormat:@"%@",[dictLoginDetail valueForKey:@"UserName"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    [requestDashBoardList setPredicate:predicate];
    
    sortDescriptorDashBoardList = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsDashBoardList = [NSArray arrayWithObject:sortDescriptorDashBoardList];
    
    [requestDashBoardList setSortDescriptors:sortDescriptorsDashBoardList];
    
    self.fetchedResultsControllerDashBoardList = [[NSFetchedResultsController alloc] initWithFetchRequest:requestDashBoardList managedObjectContext:contextDashBoardList sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerDashBoardList setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerDashBoardList performFetch:&error];
    arrAllObjDashBoardList = [self.fetchedResultsControllerDashBoardList fetchedObjects];
    if ([arrAllObjDashBoardList count] == 0)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = NoDataAvailable;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        matchesDashBoardList = arrAllObjDashBoardList[0];
        arrDashBoardResponse=[matchesDashBoardList valueForKey:@"arrOfDashBoardList"];
        [_tblViewHistory reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}

@end
