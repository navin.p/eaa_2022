//
//  TermsConditionViewController.m
//  Eaa
//
//  Created by Saavan Patidar on 07/11/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "TermsConditionViewController.h"
#import "DashBoardViewController.h"
#import "TechDashBoardViewController.h"

@interface TermsConditionViewController ()

@end

@implementation TermsConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_Back:(id)sender {
    
    NSUserDefaults *defsLoginData=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictResponse=[defsLoginData objectForKey:@"LoginData"];
    NSString *strUserType=[dictResponse valueForKey:@"LoginType"];
    
    if ([strUserType isEqualToString:@"Customer"]) {
        
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k=0; k<arrstack.count; k++) {
                if ([[arrstack objectAtIndex:k] isKindOfClass:[DashBoardViewController class]]) {
                    index=k;
                   // break;
                }
            }
            DashBoardViewController *myController = (DashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            [self.navigationController popToViewController:myController animated:NO];
        
    }else{
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k=0; k<arrstack.count; k++) {
            if ([[arrstack objectAtIndex:k] isKindOfClass:[TechDashBoardViewController class]]) {
                index=k;
                // break;
            }
        }
        TechDashBoardViewController *myController = (TechDashBoardViewController *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];

    }
}
@end
